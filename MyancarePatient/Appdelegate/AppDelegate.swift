//
//  AppDelegate.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import LocalAuthentication
import IQKeyboardManagerSwift
import UserNotifications

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import FirebaseCrash

import Fabric
import Crashlytics

import AWSCognito
import AWSCore
import PushKit
import CallKit

import AVKit
import AVFoundation

let scaleFactorX = UIScreen.main.bounds.size.width/375
let scaleFactorY = UIScreen.main.bounds.size.height/667

var appDelegate = UIApplication.shared.delegate

var callDuration: String = ""

// dev = prod
let ApplicationName = "MyanCare"
let AmazonS3PoolID = "ap-southeast-1:d928357c-d38f-4702-87e8-d8277fc1f0d0"
let AmazonS3BucketName = AWSBucket + "/my-records/" //"myancare-prod/my-records/"
let AmazonS3BucketNameProfileImage = AWSBucket + "/user/" //"myancare-prod/user/"

let NotificationInternetConnection = "internetConnection"

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate, SINCallDelegate
{
    var window: UIWindow?
    var internetReachability : Reachability? = Reachability .networkReachabilityForInternetConnection()
    var client: SINClient?
    var push: SINManagedPush?
    var callKitProvider : SINCallKitProvider?
    var callSin : SINCall?
    
    let cognitoAccountId = "xxxxxxxxxxxxxxxxx"
    let cognitoIdentityPoolId = "ap-southeast-1:428e0a5b-05a5-4c2b-976a-219bc306e181"
    let cognitoUnauthRoleArn = "xxxxxxxxxxxxxxxxx"
    let cognitoAuthRoleArn = "xxxxxxxxxxxxxxxxx"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        
        print(UIFont.familyNames)
        
        Fabric.with([Crashlytics.self, AWSCognito.self])

        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityDidChange(_:)), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        
        _ = internetReachability?.startNotifier()
        
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 50
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().enable = true
        
        // sinch work
        
        if let spush = Sinch.managedPush(with: SINAPSEnvironment.production)
        {
            self.push = spush
        }
        
        //register for voip notifications
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        voipRegistry.delegate = self;
        
        self.push?.delegate = self
        self.push?.setDesiredPushTypeAutomatically()
        
        // firebase configuration
        if #available(iOS 10.0, *)
        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
                
                if granted
                {
                    application.registerForRemoteNotifications()
                }
            }
        }
        else
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // sinch work
        print("get user id:- \(UtilityClass.getUserIdData() ?? "0")")
        self.push?.registerUserNotificationSettings()
        
        if(UtilityClass.getUserIdData() != nil)
        {
            self.initSinchClient(withUserId:UtilityClass.getUserIdData() ?? "")
        }
        
        let onUserDidLogin: ((_: String) -> Void)? = {(_ userId: String) -> Void in
            self.initSinchClient(withUserId: userId)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UserDidLoginNotification"), object: nil, queue: nil, using: {(_ note: Notification) -> Void in
            
            let userId = note.userInfo!["userId"] as? String
            onUserDidLogin!(userId!)
        })
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        Fabric.sharedSDK().debug = true
        
        Messaging.messaging().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton), name: .InstanceIDTokenRefresh, object: nil)
        
        application.applicationIconBadgeNumber = 0
        
        //add amazon s3 to app
    AmazonServiceManager.defaultServiceManager().setDefaultServiceConfiguration(forRegion: .APSoutheast1, poolID: AmazonS3PoolID)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.setLanuchVideoViewController()
        //checkController()
        
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func checkController()
    {
        if UtilityClass.getPasscodeOnOff()
        {
            if UtilityClass.getPasscode() != ""
            {
                if UtilityClass.getEnableTouchIDOrNot() == true
                {
                    self.authenticateUser()
                }
                else
                {
                    self.setRootViewController()
                }
            }
            else
            {
                self.redirectToNextScreen()
            }
        }
        else
        {
            if UtilityClass.getPasscode() != ""
            {
                if UtilityClass.getEnableTouchIDOrNot() == true
                {
                    self.authenticateUser()
                }
                else
                {
                    self.redirectToNextScreen()
                }
            }
            else
            {
                self.redirectToNextScreen()
            }
        }
    }
    
    func setLanuchVideoViewController ()
    {
        let PasscodeScreen = UIStoryboard.getWelcomeStoryBoard().instantiateViewController(withIdentifier: "splashVideo") as! SplashVideoVC
        
        let navController = UINavigationController.init(rootViewController: PasscodeScreen)
        navController.isNavigationBarHidden = true
        
        self.window?.rootViewController = navController
    }
    
    func setRootViewController ()
    {
        let PasscodeScreen = UIStoryboard.getPasscodeStoryBoard().instantiateViewController(withIdentifier: "PasscodeViewController") as! PasscodeViewController
        
        let navController = UINavigationController.init(rootViewController: PasscodeScreen)
        navController.isNavigationBarHidden = true
        
        self.window?.rootViewController = navController
    }
    
    func redirectToNextScreen()
    {
        var rootController : UIViewController?
        
        if (UtilityClass.getUserSidData()) != nil && (UtilityClass.getUserSidData()) != ""
        {
            rootController = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
        }
        else
        {
            rootController = UIStoryboard.getLanguageStoryBoard().instantiateViewController(withIdentifier: "languageVC") as! LanguageViewController
        }
        
        let navController = UINavigationController.init(rootViewController: rootController!)
        navController.isNavigationBarHidden = true
        
        self.window?.rootViewController = navController
    }
    
    func authenticateUser()
    {
        let context = LAContext()
        
        let reasonString = "Authentication is needed to access \(ApplicationName)".localized()
        
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success, evalPolicyError) in
            
            if success
            {
                print("success")
                
                DispatchQueue.main.async
                {
                    self.redirectToNextScreen()
                }
            }
            else
            {
                print("evalPolicyError = \(String(describing: evalPolicyError?.localizedDescription))")
                
                switch evalPolicyError?._code {
                    
                case LAError.systemCancel.rawValue?:
                    print("Authentication was cancelled by the system")
                    DispatchQueue.main.async {
                        self.authenticateUser()
                    }
                    
                case LAError.userCancel.rawValue?:
                    print("Authentication was cancelled by the user")
                    DispatchQueue.main.async {
                        self.authenticateUser()
                    }
                    
                default:
                    print("Authentication failed")
                    DispatchQueue.main.async {
                        self.authenticateUser()
                    }
                }
            }
        })
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        SocketManagerHandler.sharedInstance().disconnectSocket()
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
      
        SocketManagerHandler.sharedInstance().connectSocket
            {[weak self] (data, ack) in
                
                guard self != nil else { return }
                
                print(data)
                print(ack)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? CallHandlingVC
        {
            chatVwCntroler.callDismissed()
        }
        else if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? VideoCallHandlingVC
        {
            chatVwCntroler.callDismissed()
        }
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        var refreshToken = deviceTokenString
        
        if let refreshedToken = InstanceID.instanceID().token()
        {
            debugPrint("fcm token: \(refreshedToken)")
            
            refreshToken = refreshedToken
            
            UtilityClass.saveDeviceFCMToken(userDict: refreshToken)
        }
        
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
    }
    
    @objc func tokenRefreshNotificaiton(_ notification: Foundation.Notification)
    {
        if let refreshedToken = InstanceID.instanceID().token()
        {
            debugPrint("fcm refresh token: \(refreshedToken)")
            
            if (UtilityClass.getUserSidData()) != nil
            {
                updateDeviceTokenAPI()
            }
        }
        
        connectToFcm()
    }
    
    func connectToFcm()
    {
        if InstanceID.instanceID().token() != nil
        {
            return
        }
        
        Messaging.messaging().disconnect()
        
        Messaging.messaging().connect { (error) in
            
            if (error != nil)
            {
                print("Unable to connect with FCM. \(String(describing: error))")
            }
            else
            {
                print("Connected to FCM.")
            }
        }
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any])
    {
        print("Push notification received: \(data)")
        self.push?.application(application, didReceiveRemoteNotification: data)
    }
    
    func application(_ application : UIApplication, didReceiveRemoteNotification userInfo : [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print("notification received =========>   \(userInfo)")
        self.push?.application(application, didReceiveRemoteNotification: userInfo)
        
        let aps = userInfo[AnyHashable("notification")] as? String
        
        var dictonary:NSDictionary?
        
        if let data = aps?.data(using: String.Encoding.utf8)
        {
            do
            {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as! NSDictionary
                
                if let myDictionary = dictonary
                {
                    let notification_type = myDictionary["title"] as! String
                    if notification_type == "Chat message"
                    {
                        print("chat notification received")
                        
                        let chatListVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatListViewController") as! ChatListViewController
                        
                        guard let appD = appDelegate as? AppDelegate else {
                            return
                        }
                        
                        let topController = appD.window?.visibleViewController()
                    topController?.navigationController?.pushViewController(chatListVC, animated: true)
                    }
                }
            }
            catch let error as NSError
            {
                print(error)
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        print("Firebase registration token for messaging : \(fcmToken)")
    }
    
    // This method will be called when app received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("userNotificationCenter data=== ")
        
        let identifier = notification.request.identifier as NSString
        if (identifier .isEqual(to: "UYLLocalNotification"))
        {
            
        }
        else
        {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    /* ====================== Sinch Work =============================== */
    // Sinch SDK 3.12.4
    //konstantinfo02@gmail.com/konstant123
    // MARK: -  Sinch Init
    func initSinchClient(withUserId userId: String)
    {
        if !(self.client != nil)
        {
            if userId.isEmptyString()
            {
                
            }
            else
            {
                self.client = Sinch.client(withApplicationKey: "d68d658e-b5d6-4617-b12f-968fba671bb6", applicationSecret: "IYAVEU82qUabQ7U/ynqbsQ==", environmentHost: "clientapi.sinch.com", userId: userId)
                
                client?.delegate = self
                client?.call().delegate = self
            
                client?.setSupportCalling(true)
                client?.setSupportPushNotifications(true)
                client?.enableManagedPushNotifications()
            
                client?.start()
            
                client?.startListeningOnActiveConnection()
            
                callKitProvider = SINCallKitProvider(client: client)
            }
        }
    }
    
    func stopSinchClient()
    {
        client?.stop()
    }
    
    // MARK: -  SINManagedPushDelegate
    func managedPush(_ unused: SINManagedPush, didReceiveIncomingPushWithPayload payload: [AnyHashable: Any], forType pushType: String)
    {
        print("SINManagedPush Incoming call")
        self.handleRemoteNotification(payload)
    }
    
    func handleRemoteNotification(_ userInfo: [AnyHashable: Any])
    {
        print("Handle Remote notificaiton")
        
        if !(client != nil)
        {
            let userId = UtilityClass.getUserIdData()
            
            if userId != nil
            {
                self.initSinchClient(withUserId: userId!)
            }
        }
        
        print("userinfo Managed Push===== \(userInfo)")
        
        let dic = userInfo
        let sinchinfo = dic["sin"] as? String
        
        if sinchinfo == nil
        {
            return
        }
        
        let aps = dic[AnyHashable("sin")] as? String
        
        var dictonary:NSDictionary?
        
        if let data = aps?.data(using: String.Encoding.utf8)
        {
            do
            {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as! NSDictionary
                
                if let myDictionary = dictonary
                {
                    let dict = myDictionary["public_headers"] as! NSDictionary
                    if let val = dict["CALLER_NAME"]
                    {
                        print(val)
                        //userDefaults.set(userInfo, forKey: "sinchUserInfo")
                        userDefaults.set(myDictionary, forKey: "pushUserInfo")
                        userDefaults.synchronize()
                    }
                }
            }
            catch let error as NSError
            {
                print(error)
            }
        }

        _ = SINNotificationResult.self
        
        if let result = client?.relayRemotePushNotification(userInfo)
        {
            if (result.isCall()) && (result.call()?.isCallCanceled)!
            {
                setMissedNotification()
            }
            else if (result.isCall()) && (result.call()?.isTimedOut)!
            {
                setMissedNotification()
            }
            else
            {
                
            }
        }
    }
    
    func setNotification()
    {
        let now = Date(timeIntervalSinceNow: 0)
        print("NSDate:\(now)")
        
        let pushData = userDefaults.object(forKey: "pushUserInfo") as! NSDictionary
        let pushDict = pushData["public_headers"] as! NSDictionary
        print("push dict===\(pushDict)")
        
        let objNotificationContent = UNMutableNotificationContent()
        objNotificationContent.title = NSString.localizedUserNotificationString(forKey: "", arguments: nil)

        if (pushDict != nil)
        {
            objNotificationContent.body = NSString.localizedUserNotificationString(forKey: "Incoming call from \(pushDict["CALLER_NAME"] as! String)", arguments: nil)
        }
        else
        {
            objNotificationContent.body = NSString.localizedUserNotificationString(forKey: "Incoming call", arguments: nil)
        }
        
        objNotificationContent.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval:0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: "UYLLocalNotification", content: objNotificationContent, trigger: trigger)
        
        /// 3. schedule localNotification
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: {(_ error: Error?) -> Void in
            
            if error == nil
            {
                print("Local Notification succeeded")
            }
            else
            {
                print("Local Notification failed")
            }
        })
        
        print("notification ios 10 \(request.content.userInfo)")
    }
    
    func setMissedNotification()
    {
        let now = Date(timeIntervalSinceNow: 0)
        print("NSDate:\(now)")
        
        let pushData = userDefaults.object(forKey: "pushUserInfo") as! NSDictionary
        let pushDict = pushData["public_headers"] as! NSDictionary
        print("push dict===\(pushDict)")
        
        let appointmentID = pushDict["APPOINTMENT_ID"] as! String
        
        sinchSocketCallEvent (SocketManageCallEventKeyword.callEventPatientMiss.rawValue,appointmentID: appointmentID)
        
        initSinchClient(withUserId: SocketManageCallEventKeyword.callEventPatientMiss.rawValue)
        
        let objNotificationContent = UNMutableNotificationContent()
        
        objNotificationContent.title = NSString.localizedUserNotificationString(forKey: "", arguments: nil)

        if (pushDict != nil)
        {
            objNotificationContent.body = NSString.localizedUserNotificationString(forKey: "Missed call from \(pushDict["CALLER_NAME"] as! String)", arguments: nil)
        }
        else
        {
            objNotificationContent.body = NSString.localizedUserNotificationString(forKey: "Missed call", arguments: nil)
        }
        
        objNotificationContent.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval:0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: "UYLLocalNotification1", content: objNotificationContent, trigger: trigger)
        
        /// 3. schedule localNotification
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: {(_ error: Error?) -> Void in
            
            if error == nil
            {
                print("Local Notification succeeded")
            }
            else
            {
                print("Local Notification failed")
            }
        })
        
        print("notification ios 10 \(request.content.userInfo)")
    }
    
    //MARK: - Sinch Call Fire Socket Event
    func sinchSocketCallEvent(_ eventType:String, appointmentID: String)
    {
        if !SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            return
        }
        
        SocketManagerHandler.sharedInstance().manageCallSocketEvent (callDuration, eventType: eventType, appointmentID: appointmentID) { (dataDict, statusCode) in
            
        }
    }
    
    // MARK: -  SINCallClientDelegate
    func client(_ client: SINCallClient, didReceiveIncomingCall call: SINCall)
    {
        //Find MainViewController and present CallViewController from it.
       
        print(" didReceiveIncomingCall Incoming call")
        
        if !(callKitProvider?._calls.isEmpty)!
        {
            
        }
        else
        {
            let state: UIApplicationState = UIApplication.shared.applicationState
            print("call description==== \(call)")
         
            if state == .active
            {
                callKitProvider?.reportNewIncomingCall(call)
            }
            
            callSin = call
        }
    }
    
    func client(_ client: SINCallClient, willReceiveIncomingCall call: SINCall)
    {
        print("willReceiveIncomingCall")
        callKitProvider?.reportNewIncomingCall(call)
    }
    
    // MARK: - SINClientDelegate
    func clientDidStart(_ client: SINClient)
    {
        print("Sinch client started successfully (version: \(Sinch.version()))")
        //voipRegistration()
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!)
    {
        print("Sinch client error: \(error.localizedDescription)")
    }
    
    func client(_ client: SINClient, logMessage message: String, area: String, severity: SINLogSeverity, timestamp: Date)
    {
        if severity == SINLogSeverity.critical
        {
            print("\(message)")
        }
    }
    
    //MARK: - Call Logout API
    func updateDeviceTokenAPI()
    {
        //  let
        let params:[String:Any] = ["device_token":UtilityClass.getDeviceFCMToken()!]
        print(params)
        
        let urlToHit =  EndPoints.logoutUser.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            //HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    @objc func reachabilityDidChange(_ notification: Notification)
    {
        checkReachability()
    }
    
    //MARK:-
    func checkReachability()
    {
        guard let r = internetReachability else { return }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationInternetConnection), object: r.isReachable)
    }
    
    //MARK:-
    func isInternetAvailable() -> Bool
    {
        guard let r = internetReachability else { return false}
        return r.isReachable
    }
}

extension AppDelegate : PKPushRegistryDelegate
{
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType)
    {
        //print out the VoIP token. We will use this to test the notification.
        NSLog("voip token: \(pushCredentials.token)")
        
        _ = pushCredentials.token
        
        let deviceTokenString = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        UtilityClass.saveDeviceAPNSToken(userDict: deviceTokenString)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType)
    {
        let payloadDict = payload.dictionaryPayload["aps"] as? Dictionary<String, String>
        let message = payloadDict?["alert"]
        
        //present a local notifcation to visually see when we are recieving a VoIP Notification
        if UIApplication.shared.applicationState == UIApplicationState.background
        {
            let localNotification = UILocalNotification();
            localNotification.alertBody = message
            localNotification.applicationIconBadgeNumber = 1;
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            
            UIApplication.shared.presentLocalNotificationNow (localNotification);
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "VoIP Notification", message: message, onViewController: self.window?.currentViewController(), withButtonArray: nil, dismissHandler: nil)
        }
        
        NSLog("incoming voip notfication: \(payload.dictionaryPayload)")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType)
    {
        NSLog("token invalidated")
    }
}

//MARK:-
extension UIWindow
{
    //MARK:-
    func visibleViewController() -> UIViewController?
    {
        if let rootViewController: UIViewController  = self.rootViewController
        {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        
        return nil
    }
    
    //MARK:-
    class func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController
    {
        if vc.isKind(of: UINavigationController.self)
        {
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
        }
        else if vc.isKind(of: UITabBarController.self)
        {
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
        }
        else
        {
            if let presentedViewController = vc.presentedViewController
            {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            }
            else
            {
                return vc
            }
        }
    }
}
