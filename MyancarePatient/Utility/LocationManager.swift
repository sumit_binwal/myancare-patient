//
//  LocationManager.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 27/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager : NSObject, CLLocationManagerDelegate {
    
    typealias CompletionHandler = (_ data:Data?, _ dictionary:Dictionary<String, Any>?, _ statusCode:Int, _ error:Error?) -> ()
    
    var newLatitude : Double = 0
    var prevLatitude : Double = 0
    var newLongitude : Double = 0
    var prevLongitude : Double = 0
    
    var locationUpdateCount : Int = 0
    
    let locationManager = CLLocationManager()
    
    private static var instance : LocationManager =
    {
        let newInstance = LocationManager.init()
        
        return newInstance
    }()
    
    private override init() {
        
        super.init()
        
        locationUpdateCount = 0
        prevLatitude = 0
        prevLongitude = 0
        
        userDefaults.setValue(prevLatitude, forKey: "latitude")
        userDefaults.setValue(prevLongitude, forKey: "longitude")
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
    }
    
    class func sharedInstance() -> LocationManager {
        return instance
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        prevLongitude = newLongitude
        prevLatitude = newLatitude
        
        let location = locations.last
        
        guard let newLocation = location else
        {
            return
        }
        
        newLatitude = newLocation.coordinate.latitude
        newLongitude = newLocation.coordinate.longitude
        
        userDefaults.setValue(newLatitude, forKey: "latitude")
        userDefaults.setValue(newLongitude, forKey: "longitude")
        
        locationUpdateCount += 1
        
        if locationUpdateCount==1
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationUpdate"), object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if let latitude = userDefaults.value(forKey: "latitude") as? Double {
            prevLatitude = latitude
        }
        
        if let longitude = userDefaults.value(forKey: "longitude") as? Double {
            prevLongitude = longitude
        }
        
        newLatitude = prevLatitude
        newLongitude = prevLongitude
    }
    
    func isLocationAccessAllowed() -> Bool
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .restricted:
            return false
        case .denied:
            return false
        default:
            return true
        }
    }
    
    func askForLocation()
    {
        locationManager.requestWhenInUseAuthorization()
    }
}
