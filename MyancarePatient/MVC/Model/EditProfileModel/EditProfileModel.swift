//
//  EditProfileModel.swift
//  MyanCareDoctor
//
//  Created by iOS on 05/02/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import Foundation
import UIKit

class EditProfileModel
{
    var gender: String?
    var age: String?
    var dob: String?
    var blood_type: String?
    var height: String?
    var weight: String?
    var avatar : String = ""
    var name : String = ""
    var email : String = ""
    
    init() {
        
    }
}
