//
//  MyRecordAllTableViewCell.swift
//  MyanCareDoctor
//
//  Created by iOS on 01/03/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

//MARK: -
protocol MyRecordAllTableViewCellDelegate: class
{
    func myRecordCellViewButtonAction(_ cell: MyRecordAllTableViewCell)
}

class MyRecordAllTableViewCell: UITableViewCell {
    
    weak var delegate: MyRecordAllTableViewCellDelegate?
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var diseaseNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var viewButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.layer.cornerRadius = 5.0
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor.clear.cgColor
        containerView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- on view button action
    @IBAction func viewRecordButtonClicked(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellViewButtonAction(self)
    }
}
