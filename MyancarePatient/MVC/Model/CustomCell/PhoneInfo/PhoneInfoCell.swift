//
//  PhoneInfoCell.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 04/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

//MARK: - Cell Identifiers
let kCellIdentifier_PhoneInfo_PhoneNo = "phoneInfoCellPhone"
let kCellIdentifier_PhoneInfo_Email = "phoneInfoCellEmail"

//MARK: -
protocol PhoneDataCellDelegate: class
{
    func phoneDataCell(_ cell: PhoneInfoCell, updatedInputFieldText inputValue: String)
}

//MARK: - CellTypes Enum
enum PhoneInfoCellType {
    case phoneNumber
    case email
    case password
    case confirmPassword
    case none
}

class PhoneInfoCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    weak var delegate: PhoneDataCellDelegate?

    var cellType = PhoneInfoCellType.none

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.setBorderColor(UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 5, borderWidth: 0.5)
        
        if inputTextField != nil
        {
            inputTextField.delegate = self
            inputTextField.text = ""
            
            inputTextField.setPlaceholderColor (UIColor.MyanCarePatient.textfieldPlaceholderColor)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        switch cellType {
        case .phoneNumber:
            inputTextField.placeholder = "9xxxxxxxxx"
            inputTextField.keyboardType = .numberPad

            
        case .email:
            inputTextField.placeholder = "mailaddress@domain.com".localized()
            inputTextField.keyboardType = .emailAddress
            
        case .password:
            inputTextField.placeholder = "Type your password here".localized()
            inputTextField.keyboardType = .default
            
        case .confirmPassword:
            inputTextField.placeholder = "Retype your password".localized()
            inputTextField.keyboardType = .default

        default:
            inputTextField.placeholder = ""
            //bindThePickerview()
            break
        }
        
        inputTextField.tag = self.tag
    }
}

extension PhoneInfoCell: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let phoneDelegate = delegate else {
            return true
        }
        
        phoneDelegate.phoneDataCell(self, updatedInputFieldText: newString!)
        
        return true
    }
}
