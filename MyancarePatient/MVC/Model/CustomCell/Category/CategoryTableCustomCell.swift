//
//  CategoryTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 15/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_CategoryCell = "cellCategoryTable"

class CategoryTableCustomCell: UITableViewCell {
    
    
    @IBOutlet var imageViewIcon: UIButton!
    @IBOutlet var buttonCategoryListName: UIButton!
    @IBOutlet var buttonSelection: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
