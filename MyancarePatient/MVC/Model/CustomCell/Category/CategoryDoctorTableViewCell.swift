//
//  CategoryDoctorTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class CategoryDoctorTableViewCell: UITableViewCell {

    @IBOutlet var labelCategoryListName: UILabel!
    @IBOutlet var buttonSelection: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
