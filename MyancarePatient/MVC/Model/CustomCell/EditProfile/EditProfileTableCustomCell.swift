//
//  EditProfileTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 11/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_EditProfileDOBTableCell = "EditProfileDOBCell"
let cellIdentifier_EditProfileBloodTypeTableCell = "EditProfileBloodTypeCell"
let cellIdentifier_EditHeightTableCell = "EditProfileHeightCell"

//MARK: -
enum editProfileDataCellType {
    case bloodGroup
    case height
    case weight
    case dob
    case none
}

//MARK: -
protocol EditProfileTableViewCellDelegate: class
{
    func editProfileDataCell(_ cell: EditProfileTableCustomCell, updatedInputFieldText inputValue: String)
}

class EditProfileTableCustomCell: UITableViewCell {
    
    private var arrayBloodGroupTypes : NSMutableArray = []
    
    @IBOutlet weak var downImage: UIImageView!
    @IBOutlet weak var inputTextField: UITextField!
    
    var pickerBinder: PickerViewBinder?
    var cellType = editProfileDataCellType.none
    
    weak var delegate: EditProfileTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.disableSelection()
        
        arrayBloodGroupTypes.add("O-")
        arrayBloodGroupTypes.add("O+")
        arrayBloodGroupTypes.add("A-")
        arrayBloodGroupTypes.add("A+")
        arrayBloodGroupTypes.add("B-")
        arrayBloodGroupTypes.add("B+")
        arrayBloodGroupTypes.add("AB-")
        arrayBloodGroupTypes.add("AB+")
        
        if inputTextField != nil
        {
            inputTextField.delegate = self
            inputTextField.text = ""
            
            inputTextField.setPlaceholderColor (UIColor.MyanCarePatient.textfieldPlaceholderColor)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: -
    override func layoutSubviews() {
        switch cellType {
            
        case .height:
            inputTextField.placeholder = "Height"
            inputTextField.keyboardType = .numberPad
            
        case .weight:
            inputTextField.placeholder = "Weight"
            inputTextField.keyboardType = .numberPad
            
        case .dob:
            inputTextField.placeholder = "Select your DOB"
            downImage.isHidden = false
            bindThePickerview1()
            
        default:
            inputTextField.placeholder = "Select blood group"
            downImage.isHidden = true
            bindThePickerview(_arrValue: arrayBloodGroupTypes)
        }
        
        inputTextField.tag = self.tag
    }
    
    func bindThePickerview1()
    {
        if cellType == .dob, pickerBinder == nil
        {
            let pickerVw = UIDatePicker()
            pickerBinder = PickerViewBinder ()
            
            pickerBinder?.handleDatePickerView(_pickerView: pickerVw, addDelegate: self)
            
            inputTextField.inputView = pickerVw
        }
    }
    
    //MARK: -
    func bindThePickerview(_arrValue : NSMutableArray)
    {
        let picker = UIPickerView ()
        pickerBinder = PickerViewBinder ()
        
        pickerBinder?.handlePickerView(_pickerView: picker, values: _arrValue as! [String], addDelegate: self)
        
        inputTextField.inputView = picker
    }
    
    //MARK: -
    func updateCell()
    {
        switch cellType {
            
        case .bloodGroup:
            guard modelEditProfileProcess.blood_type != nil else
            {
                return
            }
            
            inputTextField.text = modelEditProfileProcess.blood_type
            
        case .height:
            guard modelEditProfileProcess.height != nil else
            {
                return
            }
            
            inputTextField.text = modelEditProfileProcess.height
            
        case .weight:
            guard modelEditProfileProcess.weight != nil else
            {
                return
            }
            
            inputTextField.text = modelEditProfileProcess.weight
            
        case .dob:
            guard modelEditProfileProcess.dob != nil else
            {
                return
            }
            
            inputTextField.text = modelEditProfileProcess.dob
            
        default:
            break
        }
    }
    
    func updateHeightValue(usingValue value: String)
    {
        var numberString = UtilityClass.extractNumber(fromString: value)
        //print("number string = \(numberString)")
        
        if numberString.count == 1
        {
            numberString = numberString + "'"
        }
        else
        {
            numberString.insert("'", at: numberString.index(after: numberString.startIndex))
        }
        
        inputTextField.text = numberString
    }
}

//MARK: -
//MARK: - Extension -> PickerViewBinderDelegate
extension EditProfileTableCustomCell: PickerViewBinderDelegate, DatePickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String) {
        
        inputTextField.text = value
        
        guard let tempDelegate = delegate else {
            return
        }
        
        tempDelegate.editProfileDataCell(self, updatedInputFieldText: value)
    }
    
    func pickerBinder(_ pickerBinder: PickerViewBinder, didSelectRow row: Int, withUpdatedValue value: String) {
        inputTextField.text = value
        
        switch cellType {
            
        case .bloodGroup:
            inputTextField.text = value
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.editProfileDataCell(self, updatedInputFieldText: value)
            
        default:
            break
        }
    }
}

//MARK: -
//MARK: - Extension -> UITextFieldDelegate
extension EditProfileTableCustomCell: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch cellType {
            
        case .bloodGroup :
            textField.clearsOnBeginEditing = true
            
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if cellType == .bloodGroup || cellType == .dob {
            return false
        }
        
        var newString : String?
        //var finalCount = 0
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                if textField.text?.last == "'" {
                    newString = ""
                } else {
                    var newStr = textField.text! as NSString
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    newString = newStr as String
                }
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            let length = (textField.text?.count)!
            
            if cellType == .height {
                if length >= 4 {
                    newString = textField.text
                } else {
                    var newStr = textField.text! as NSString
                    
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    
                    newString = newStr as String
                }
            } else {
                if length >= 3 {
                    newString = textField.text
                } else {
                    var newStr = textField.text! as NSString
                    
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    
                    newString = newStr as String
                }
            }
        }
        
        if cellType == .height {
            if string.count > 0 {
                updateHeightValue(usingValue: newString!)
            } else {
                inputTextField.text = newString
            }
        } else {
            inputTextField.text = newString
        }
        
        guard let tempDelegate = delegate else {
            return true
        }
        
        tempDelegate.editProfileDataCell(self, updatedInputFieldText: newString!)
        
        return false
    }
}
