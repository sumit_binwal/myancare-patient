//
//  AboutUsTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 11/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_AboutUsCell = "cellAboutUsTable"
let cellIdentifier_AboutUsSecondCell = "cellAboutUsTableSecond"
class AboutUsTableCustomCell: UITableViewCell {

    //MARK: - Properties (cellMoreTable)
    @IBOutlet weak var buttonListIcons: UIButton!
    @IBOutlet weak var buttonListNames: UIButton!
    @IBOutlet weak var buttonNextScreen: UIButton!

    
    //cellSettingMoreTable
    
    @IBOutlet weak var legalLabel: UILabel!
    @IBOutlet var labelAboutUsOrVist: UILabel!
    @IBOutlet weak var buttonVistOrAboutIcon: UIButton!
    @IBOutlet weak var buttonVisitOrAboutUsListName: UIButton!
    @IBOutlet weak var buttonVisitOrAboutUsNextScreen: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disableSelection()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
