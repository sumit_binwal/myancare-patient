//
//  DoctorProfileTableViewCell.swift
//  MyancarePatient
//
//  Created by Ratina on 3/16/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class DoctorProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLAbel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
