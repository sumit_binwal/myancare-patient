//
//  SignupCustomeCell.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 18/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit

//MARK: -
enum PersonalDataCellType {
    case name
    case dob
    case none
}

let cellIdentifier_SignupCell = "signupCell"

//MARK: -
protocol PersonalDataCellDelegate: class
{
    func personalDataCell(_ cell: SignupCustomeCell, dateOfBirthValue: String)
    func personalDataCell(_ cell: SignupCustomeCell, updatedInputFieldText inputValue: String)
}

class SignupCustomeCell: UITableViewCell {

    @IBOutlet weak var inputTextField: UITextField!
    
    var activeTextField: UITextField!
    
    var pickerBinder: PickerViewBinder?
    
    weak var delegate: PersonalDataCellDelegate?
    
    var cellType = PersonalDataCellType.none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disableSelection()

        // Initialization code
    }

    override func layoutSubviews() {
        
        switch cellType {
        case .name:
            inputTextField.placeholder = "Type your name here".localized()
            inputTextField.keyboardType = .asciiCapable
            inputTextField.delegate = self
            inputTextField.autocorrectionType = .no
            
        case .dob:
            inputTextField.placeholder = "Select your DOB".localized()
            inputTextField.delegate = self
            inputTextField.tintColor = UIColor.clear
            
        default:
            break
        }
        
        inputTextField.tag = self.tag
    }
    
    func bindThePickerview()
    {
        if cellType == .dob, pickerBinder == nil
        {
            let pickerVw = UIDatePicker()
            pickerBinder = PickerViewBinder ()
            
            pickerBinder?.handleDatePickerView(_pickerView: pickerVw, addDelegate: self)
            
            inputTextField.inputView = pickerVw
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension SignupCustomeCell: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch cellType {
        case .dob:
            bindThePickerview()
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let signUpDelegate = delegate else {
            return true
        }
        
        signUpDelegate.personalDataCell(self, updatedInputFieldText: newString!)
        
        return true
    }
}

extension SignupCustomeCell: DatePickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String) {
        
        inputTextField.text = value
        
        guard let tempDelegate = delegate else {
            return
        }
        
        tempDelegate.personalDataCell(self, dateOfBirthValue: value)
    }
}
