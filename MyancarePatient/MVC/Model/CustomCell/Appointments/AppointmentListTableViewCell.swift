//
//  AppointmentListTableViewCell.swift
//  MyanCareDoctor
//
//  Created by iOS on 01/02/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit

let cellIdentifier_AppointmentListTableViewCell = "appointmentListTableViewCell"

//MARK: -
protocol AppointmentListTableViewCellDelegate: class
{
    func AppointmentListTableViewCellImageViewClick(_ cell: AppointmentListTableViewCell)
}

class AppointmentListTableViewCell: UITableViewCell {

    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var NameLabel: UILabel!
    
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    weak var delegate: AppointmentListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        patientImageView.layer.cornerRadius = patientImageView.frame.size.width/2*scaleFactorX
        patientImageView.layer.borderWidth = 1.0
        patientImageView.layer.borderColor = UIColor.clear.cgColor
        patientImageView.layer.masksToBounds = true
        
        startDateView.layer.cornerRadius = 1.0
        startDateView.layer.borderWidth = 1.0
        startDateView.layer.borderColor = UIColor.clear.cgColor
        startDateView.layer.masksToBounds = true
        
        patientImageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(patientImageViewClicked))
        patientImageView.addGestureRecognizer(gesture)
    }

    @objc func patientImageViewClicked() {
        
        guard let phoneDelegate = delegate else {
            return
        }
        
        phoneDelegate.AppointmentListTableViewCellImageViewClick(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
