//
//  MedicineReminderTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//MARK: -
protocol MedicineReminderTableViewCellDelegate: class
{
    func myRecordCellDeleteButtonAction(_ cell: MedicineReminderTableViewCell)
    func myRecordCellEditButtonAction(_ cell: MedicineReminderTableViewCell)
}

class MedicineReminderTableViewCell: UITableViewCell {

    weak var delegate: MedicineReminderTableViewCellDelegate?
    
    @IBOutlet weak var medicineDescription: UILabel!
    @IBOutlet weak var medicineTime: UILabel!
    @IBOutlet weak var medicineName: UILabel!
    @IBOutlet weak var medicineImage: UIImageView!
    @IBOutlet weak var medicineTimeImage: UIImageView!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellDeleteButtonAction(self)
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellEditButtonAction(self)
    }
}
