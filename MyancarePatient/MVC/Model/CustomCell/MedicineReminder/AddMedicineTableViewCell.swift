//
//  AddMedicineTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 12/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let kCellIdentifier_Add_Medicine_single_table_view_cell = "AddMedicineTableViewCellSingle"
let kCellIdentifier_Add_Medicine_double_table_view_cell = "AddMedicineTableViewCellDouble"

//MARK: -
enum AddMedicineCellType {
    case drugName
    case medicineFor
    case medicineType
    case repeatation
    case frequencyUnit
    case fromTo
    case startTime
    case none
}

//MARK: -
protocol AddMedicineTableViewCellDelegate: class
{
    // update from and freguency
    func addMedicineRecordUpdateFirstData(_ cell: AddMedicineTableViewCell, inputValue: String)
    
    // update to and unit
    func addMedicineRecordUpdateSecondData(_ cell: AddMedicineTableViewCell, inputValue: String)
    
    // update start time
    func addMedicineRecordUpdateStartTimeData(_ cell: AddMedicineTableViewCell, inputValue: String)
    
    // update medicine type and repeatation
    func addMedicineRecordUpdateMedicineTypeData(_ cell: AddMedicineTableViewCell, inputValue: String)
    
    // update drug name and medicine for
    func addMedicineRecordUpdateData(_ cell: AddMedicineTableViewCell, updatedInputFieldText inputValue: String)
}

class AddMedicineTableViewCell: UITableViewCell {

    @IBOutlet weak var singleImage: UIImageView!
    @IBOutlet weak var singleNameLabel: UILabel!
    @IBOutlet weak var singleTextFiled: UITextField!
    
    @IBOutlet weak var doubleImage1: UIImageView!
    @IBOutlet weak var doubleNameLabel1: UILabel!
    @IBOutlet weak var doubleTextFiled1: UITextField!
    
    @IBOutlet weak var doubleImage2: UIImageView!
    @IBOutlet weak var doubleNameLabel12: UILabel!
    @IBOutlet weak var doubleTextFiled2: UITextField!
    
    var pickerBinder: PickerViewBinder?
    
    weak var delegate: AddMedicineTableViewCellDelegate?
    
    var cellType = AddMedicineCellType.none
    
    var activeTextField : UITextField?
    
    private var arrayFrequencyTypes : NSMutableArray = []
    private var arrayUnitTypes : NSMutableArray = []
    private var arrayMedicineTypeTypes : NSMutableArray = []
    private var arrayRepeatationTypes : NSMutableArray = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //arrayRepeatationTypes.add("1 Hr")
        arrayRepeatationTypes.add("2 Hr")
        //arrayRepeatationTypes.add("3 Hr")
        arrayRepeatationTypes.add("4 Hr")
        //arrayRepeatationTypes.add("5 Hr")
        arrayRepeatationTypes.add("6 Hr")
        //arrayRepeatationTypes.add("7 Hr")
        arrayRepeatationTypes.add("8 Hr")
        //arrayRepeatationTypes.add("9 Hr")
        arrayRepeatationTypes.add("10 Hr")
        //arrayRepeatationTypes.add("11 Hr")
        arrayRepeatationTypes.add("12 Hr")
        
        arrayMedicineTypeTypes.add("Tablet to disolve".localized())
        arrayMedicineTypeTypes.add("Capsule".localized())
        arrayMedicineTypeTypes.add("Tablet".localized())
        arrayMedicineTypeTypes.add("Liquid by drops".localized())
        arrayMedicineTypeTypes.add("Liquid by spon".localized())
    }
    
    override func layoutSubviews() {
        
        switch self.tag {
            
        case 0:
            singleTextFiled.placeholder = "Type drug name here".localized()
            singleTextFiled.keyboardType = .asciiCapable
            singleTextFiled.delegate = self
            singleTextFiled.autocorrectionType = .no
            
            singleTextFiled.tag = self.tag
            
            cellType = .drugName
            
            singleImage.isHidden = true
            
            singleNameLabel.text = "Drug Name".localized()
            
        case 1:
            singleTextFiled.placeholder = "Type here the reason".localized()
            singleTextFiled.keyboardType = .asciiCapable
            singleTextFiled.delegate = self
            singleTextFiled.autocorrectionType = .no
            
            singleTextFiled.tag = self.tag
            
            cellType = .medicineFor
            
            singleImage.isHidden = true
            
            singleNameLabel.text = "What’s this medication for?".localized()
            
        case 2:
            singleTextFiled.placeholder = "Select the type here".localized()
            singleTextFiled.delegate = self
            singleTextFiled.tintColor = UIColor.clear
            
            singleTextFiled.tag = self.tag
            
            cellType = .medicineType
            
            singleImage.isHidden = false
            singleImage.image = #imageLiteral(resourceName: "downArrow")
            
            singleNameLabel.text = "Medication type?".localized()
            
        case 3:
            singleTextFiled.placeholder = "Select repeatation".localized()
            singleTextFiled.delegate = self
            
            singleTextFiled.tag = self.tag
            singleTextFiled.tintColor = UIColor.clear
            
            cellType = .repeatation
            
            singleImage.isHidden = false
            singleImage.image = #imageLiteral(resourceName: "downArrow")
            
            singleNameLabel.text = "Repeatation?".localized()
            
        case 4:
            doubleTextFiled1.placeholder = "Select ...".localized()
            doubleTextFiled1.delegate = self
            doubleTextFiled1.tintColor = UIColor.clear
            
            doubleTextFiled1.tag = self.tag
            
            cellType = .fromTo
            
            doubleImage1.isHidden = false
            doubleImage1.image = #imageLiteral(resourceName: "add-medicine-date")
            
            doubleNameLabel1.text = "From".localized()
            
            doubleTextFiled2.placeholder = "Select ...".localized()
            doubleTextFiled2.delegate = self
            doubleTextFiled2.tintColor = UIColor.clear
            
            doubleTextFiled2.tag = self.tag
            
            doubleImage2.isHidden = false
            doubleImage2.image = #imageLiteral(resourceName: "add-medicine-date")
            
            doubleNameLabel12.text = "To".localized()
            
        default:
            singleTextFiled.placeholder = "Start reminder time".localized()
            singleTextFiled.delegate = self
            singleTextFiled.tintColor = UIColor.clear
            
            singleTextFiled.tag = self.tag
            
            cellType = .startTime
            
            singleImage.isHidden = false
            singleImage.image = #imageLiteral(resourceName: "add-medicine-time")
            
            singleNameLabel.text = "Start time".localized()
        }
    }
    
    //MARK: -
    func updateCell()
    {
        switch self.tag {
         
        case 0:
            guard modelAddMedicineProcess.drugName != "" else
            {
                return
            }
            
            singleTextFiled.text = modelAddMedicineProcess.drugName
            
        case 1:
            guard modelAddMedicineProcess.medicationFor != "" else
            {
                return
            }
            
            singleTextFiled.text = modelAddMedicineProcess.medicationFor
            
        case 2:
            guard modelAddMedicineProcess.medicationType != "" else
            {
                return
            }
            
            singleTextFiled.text = modelAddMedicineProcess.medicationType
            
        case 3:
            guard modelAddMedicineProcess.repeatation != "" else
            {
                return
            }
            
            singleTextFiled.text = modelAddMedicineProcess.repeatation
            let inputValue1 = modelAddMedicineProcess.repeatation.replacingOccurrences(of: " Hr", with: "")
            modelAddMedicineProcess.frequency = Int(inputValue1)!
            
        case 4:
            guard modelAddMedicineProcess.from != "" else
            {
                return
            }
            
            doubleTextFiled1.text = modelAddMedicineProcess.from
            
            guard modelAddMedicineProcess.to != "" else
            {
                return
            }
            
            doubleTextFiled2.text = modelAddMedicineProcess.to
            
        default:
            guard modelAddMedicineProcess.startTime != "" else
            {
                return
            }
            
            singleTextFiled.text = modelAddMedicineProcess.startTime
        }
    }
    
    //MARK: -
    func bindThePickerviewMedicineType(_arrValue : NSMutableArray)
    {
        let picker = UIPickerView ()
        pickerBinder = PickerViewBinder ()
        
        picker.selectRow(0, inComponent: 0, animated: false)
        
        pickerBinder?.handlePickerView(_pickerView: picker, values: _arrValue as! [String], addDelegate: self)
        
        activeTextField?.inputView = picker
    }

    func bindThePickerview()
    {
        let pickerVw = UIDatePicker()
        
        pickerBinder = PickerViewBinder ()
        
        pickerBinder?.format = "dd MMM, yyyy"
        
        pickerVw.setDate(Date(), animated: false)

        pickerBinder?.handleDatePickerViewMedicine(_pickerView: pickerVw, addDelegate: self)

        activeTextField?.inputView = pickerVw
    }
    
    func bindThePickerviewStartTime()
    {
        let pickerVw = UIDatePicker()
            
        pickerBinder = PickerViewBinder ()
        pickerBinder?.format = "hh:mm a"
            
        pickerBinder?.handleTimePickerViewMedication(_pickerView: pickerVw, addDelegate: self)
            
        activeTextField?.inputView = pickerVw
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension AddMedicineTableViewCell: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //print("self.tag == \(self.tag)")
        
        switch self.tag {
            
        case 0:
            activeTextField = singleTextFiled
            
        case 1:
            activeTextField = singleTextFiled
            
        case 2:
            activeTextField = singleTextFiled
            bindThePickerviewMedicineType(_arrValue: arrayMedicineTypeTypes)
            
        case 3:
            activeTextField = singleTextFiled
            bindThePickerviewMedicineType(_arrValue: arrayRepeatationTypes)
            
        case 4:
            if textField == doubleTextFiled1 {
                activeTextField = doubleTextFiled1
                bindThePickerview()
            } else {
                activeTextField = doubleTextFiled2
                bindThePickerview()
            }
            
        default:
            
            activeTextField = singleTextFiled
            bindThePickerviewStartTime()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if cellType == .fromTo || cellType == .frequencyUnit || cellType == .startTime || cellType == .medicineType || cellType == .repeatation
        {
            return true
        }
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                newString = textField.text
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let signUpDelegate = delegate else {
            return true
        }
        
        signUpDelegate.addMedicineRecordUpdateData(self, updatedInputFieldText: newString!)
        
        return true
    }
}

extension AddMedicineTableViewCell: DatePickerViewBinderDelegate, PickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String) {
        
        activeTextField?.text = value
        
        if activeTextField == singleTextFiled {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.addMedicineRecordUpdateStartTimeData(self, inputValue: value)
            
        } else if activeTextField == doubleTextFiled1 {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.addMedicineRecordUpdateFirstData(self, inputValue: value)
            
        } else {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.addMedicineRecordUpdateSecondData(self, inputValue: value)
        }
    }
    
    func pickerBinder(_ pickerBinder: PickerViewBinder, didSelectRow row: Int, withUpdatedValue value: String) {
        
        activeTextField?.text = value
        
        switch cellType {
            
        case .frequencyUnit:
            
            if activeTextField == doubleTextFiled1 {
                
                guard let tempDelegate = delegate else {
                    return
                }
                
                tempDelegate.addMedicineRecordUpdateFirstData(self, inputValue: value)
                
            } else {
                
                guard let tempDelegate = delegate else {
                    return
                }
                
                tempDelegate.addMedicineRecordUpdateSecondData(self, inputValue: value)
            }
            
        case .repeatation:
            guard let tempDelegate = delegate else {
                return
            }
                
            tempDelegate.addMedicineRecordUpdateMedicineTypeData(self, inputValue: value)
            
        default:
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.addMedicineRecordUpdateMedicineTypeData(self, inputValue: value)
        }
    }
}
