//
//  AddRecordFileModel.swift
//  MyancarePatient
//
//  Created by iOS on 15/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class AddRecordFileModel
{
    var recordID : String?
    var recordName : String?
    var recordPathName : String?
    
    init()
    {
        recordID = ""
        recordName = ""
        recordPathName = ""
    }
    
    deinit
    {
        print("AddRecordFileModel Model deinit")
    }
}
