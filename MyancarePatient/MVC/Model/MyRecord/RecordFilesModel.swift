//
//  RecordFilesModel.swift
//  MyancarePatient
//
//  Created by iOS on 15/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class RecordFilesModel
{
    var recordID : String?
    var recordName : String?
    var recordPathName : String?
    var file_url : String?
    
    init()
    {
        recordID = ""
        recordName = ""
        recordPathName = ""
        file_url = ""
    }
    
    deinit
    {
        print("RecordFilesModel Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["id"] as? String
        {
            recordID = id
        }
        
        if let recordName1 = dictionary["name"] as? String
        {
            recordName = recordName1
        }
        
        if let recordPathName1 = dictionary["path"] as? String
        {
            recordPathName = recordPathName1
        }
        
        if let file_url1 = dictionary["file_url"] as? String
        {
            let AWSBucketur = AWSBucket + "/" + AWSBucket + "/"
            let aws1 = AWSBucket + "/"
            file_url = file_url1
            file_url = file_url?.replacingOccurrences(of: AWSBucketur, with: aws1)
        }
    }
}

