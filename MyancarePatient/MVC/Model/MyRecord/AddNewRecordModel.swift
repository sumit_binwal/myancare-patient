//
//  AddNewRecordModel.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class AddNewRecordModel
{
    var doctorName : String?
    var diseaseName : String?
    var hospitalName : String?
    var date : String?
    var recordID : String?
    
    init() {
        
    }
}
