//
//  ReviewModel.swift
//  MyancarePatient
//
//  Created by iOS on 07/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class ReviewModel
{
    var reviewUserName : String?
    var reviewUserImage : String?
    var reviewComment : String?
    var rating : Int?
    var dateString : String?
    
    init()
    {
        reviewComment = ""
        reviewUserName = ""
        reviewUserImage = ""
        dateString = ""
        
        rating = 0
    }
    
    deinit
    {
        print("ReviewModel Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let fromUser = dictionary["fromUser"] as? [String : Any]
        {
            reviewUserName = fromUser["name"] as? String
            reviewUserImage = fromUser["avatar_url"] as? String
        }
        
        if let comment1 = dictionary["comment"] as? String
        {
            reviewComment = comment1
        }
        
        if let rating1 = dictionary["rating"] as? Int
        {
            rating = rating1
        }
        
        if let date = dictionary["created"] as? Int
        {
            dateString = String(date)
            
            let date1 = UtilityClass.getDateStringFromTimeStamp1(timeStamp: dateString!)
            
            dateString = date1 as String
        }
    }
}
