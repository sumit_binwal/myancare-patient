//
//  ArticleSearchViewController.swift
//  MyancarePatient
//
//  Created by iOS on 24/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet
///Aricle Search ViewController
class ArticleSearchViewController: UIViewController {
    ///UILabel Refrence Variable - NOData Label
    @IBOutlet weak var noDataLabel: UILabel!
    ///UITableView Refrence Variable - ArticleSearch TableView
    @IBOutlet weak var articleSearchtableView: UITableView!
    ///Article Model Data Refrence Variable - arrayArticle SearchData

    var arrayArticleSearchData = [ArticleModel]()
    
    ///UIButton Refrence Variable - Search Button
    @IBOutlet weak var searchBtn: UIButton!
    ///UILabel Refrence Variable - title Label
    @IBOutlet weak var titleLabel: UILabel!
    ///UITextField Refrence Variable - search Text Field
    @IBOutlet weak var searchTextField: UITextField!
    ///String Refrence Variable - Select Category String
    var selectedCategoryString = ""
    ///NSMutableArray Refrence Variable - selectedCategoryArr
    var selectedCategoryArr : NSMutableArray = []
    ///Bool Refrence Variable - is Paging
    var isPaging = false
    ///String Refrence Variable - Search String

    var searchString = ""
    
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    //MARK: - viewDidLoad
    ///UIVIewController LIfe Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        noDataLabel.isHidden = true
        
        self.navigationController?.isNavigationBarHidden = true
        
        titleLabel.text = "Search".localized()
        
        UtilityClass.disableIQKeyboard()
        
        //if searchTextField.text == "" {
        //    searchTextField.isHidden = true
        //} else {
        //    searchTextField.isHidden = false
        //}
        //
        //self.view.endEditing(true)
        
        searchTextField.isHidden = false
        
        searchBtn.isHidden = true
        titleLabel.isHidden = true
        
        searchTextField.placeholder = "Find an Article".localized()
        searchTextField.becomeFirstResponder()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        UtilityClass.enableIQKeyboard()
    }
    
    //MARK: - Deinit
    deinit {
        print("ArticleSearchViewController deinit")
    }
    
    //MARK: - setUpView
    ///setUpView
    func setUpView() {
        
        searchTextField.delegate = self
        noDataLabel.isHidden = true
        
        articleSearchtableView.isHidden = true
        
        articleSearchtableView.emptyDataSetSource = self
        articleSearchtableView.emptyDataSetDelegate = self
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - on Filter Button Action
    ///on Filter Button Action
    @IBAction func filterButtonAction(_ sender: Any) {
        
        let filterVC = UIStoryboard.getFilterStoryBoard().instantiateInitialViewController() as! CategoryViewController
        
        filterVC.delegate = self
        if selectedCategoryString != "" {
            filterVC.selectedCategoryIdArr = selectedCategoryArr
        }
        
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    //MARK: - On Search Button Action
    ///On Search Button Action
    @IBAction func searchButtonAction(_ sender: Any) {
        
        searchTextField.isHidden = false
        
        searchBtn.isHidden = true
        titleLabel.isHidden = true
        
        searchTextField.placeholder = "Find an Article".localized()
        searchTextField.becomeFirstResponder()
    }
    
    //MARK; - on Back Button Action
    ///on Back Button Action
    @IBAction func backButtonAction(_ sender: Any) {
        
        if searchTextField.isHidden {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.view.endEditing(true)
            searchTextField.isHidden = true
            
            searchBtn.isHidden = false
            titleLabel.isHidden = false
            
            searchTextField.text = ""
        }
    }
    
    //MARK: - get search data according to search string and filter
    ///get search data according to search string and filter
    func getSearchDataFromAPI()
    {
        //self.view.endEditing(true)
        
        var searchStr = searchString
        searchStr = searchStr.replacingOccurrences(of: " ", with: "%20")
        
        let urlToHit = EndPoints.search(searchStr, selectedCategoryString, arrayArticleSearchData.count).path
        print("search url = \(urlToHit)")
        
        if arrayArticleSearchData.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if self.searchString != ""
                {
                    //if error?.localizedDescription == "The network connection was lost."
                    //{
                    //    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    //}
                    //else
                    //{
                    //    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    //}
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let articleData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard articleData != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: articleData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    ///Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayArticleSearchData.append(model) // Adding model to array
        }
        
        articleSearchtableView.isHidden = false
        
        articleSearchtableView.reloadData()
    }
}

///Extension -> UITextField -> UITextFieldDelegate
extension ArticleSearchViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.text?.isEmptyString())! {
            
        } else {
            
            arrayArticleSearchData.removeAll()
            isPaging = true
            
            // Cancel the currently pending item
            pendingRequestWorkItem?.cancel()
            
            // Wrap our request in a work item
            let requestWorkItem = DispatchWorkItem { [weak self] in
                
                self?.searchString = ""
                
                AppWebHandler.sharedInstance().cancelTask(forEndpoint: "article/search?")
                self?.getSearchDataFromAPI()
            }
            
            // Save the new work item and execute it after 250 ms
            pendingRequestWorkItem = requestWorkItem
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        textField.text = ""
        
        arrayArticleSearchData.removeAll()
        
        articleSearchtableView.reloadData()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            
            if textField.text?.count == 1 {
                
                arrayArticleSearchData.removeAll()
                isPaging = true
                
                // Cancel the currently pending item
                pendingRequestWorkItem?.cancel()
                
                // Wrap our request in a work item
                let requestWorkItem = DispatchWorkItem { [weak self] in
                    
                    self?.searchString = ""
                    self?.articleSearchtableView.reloadData()
                }
                
                // Save the new work item and execute it after 250 ms
                pendingRequestWorkItem = requestWorkItem
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
            }
        }
        
        var newString : String?
        
        if string.count == 0 {
            if textField.text?.count == 0 {
                newString = textField.text
            } else {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        } else {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if (newString?.count)! >= 3 {
            
            //searchTextField.text = newString
            
            arrayArticleSearchData.removeAll()
            isPaging = true
            
            // Cancel the currently pending item
            pendingRequestWorkItem?.cancel()
            
            // Wrap our request in a work item
            let requestWorkItem = DispatchWorkItem { [weak self] in
                
                self?.searchString = newString!
                
                AppWebHandler.sharedInstance().cancelTask(forEndpoint: "article/search?")
                self?.getSearchDataFromAPI()
            }
            
            // Save the new work item and execute it after 250 ms
            pendingRequestWorkItem = requestWorkItem
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        }
        
        return true
    }
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension ArticleSearchViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayArticleSearchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ArticlesTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_ArticlesTableCell) as! ArticlesTableCustomCell
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            cell.labelArticleDate.font = UIFont(name: cell.labelArticleDate.font.fontName, size: (12.0 + CGFloat(customFontSize)))
            
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticleSearchData[indexPath.row].title!, location: 0, length: arrayArticleSearchData[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: (15.0 + CGFloat(customFontSize))))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticleSearchData[indexPath.row].description_short!, location: 0, length: arrayArticleSearchData[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: (14.0 + CGFloat(customFontSize))))
            
            cell.labelArticleTitle.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleTitle.attributedText, lineHeightMultiple: 0.8)
            
            cell.labelArticleDescription.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleDescription.attributedText, lineHeightMultiple: 0.8)
        }
        else
        {
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticleSearchData[indexPath.row].title!, location: 0, length: arrayArticleSearchData[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: 15))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticleSearchData[indexPath.row].description_short!, location: 0, length: arrayArticleSearchData[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: 14))
        }
        
        cell.imageViewArticleImage.setShowActivityIndicator(true)
        cell.imageViewArticleImage.setIndicatorStyle(.gray)
        
        cell.imageViewArticleImage.sd_setImage(with: URL.init(string: arrayArticleSearchData[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
    cell.buttonTotalComments.setTitle(arrayArticleSearchData[indexPath.row].commentsCount!, for: .normal)
        
        cell.labelArticleDate.text = UtilityClass.getDateStringFromTimeStamp(timeStamp: arrayArticleSearchData[indexPath.row].postedTime!) as String
        
        if isPaging && indexPath.row == arrayArticleSearchData.count - 1 {
            getSearchDataFromAPI()
        }
        
        return cell
    }
    
    //3 Simple Ways to Embrace Change Now
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleDetailVC = UIStoryboard.getArticlDetailStoryBoard().instantiateInitialViewController() as! ArticleDetailViewController
        articleDetailVC.articleModelData = arrayArticleSearchData[indexPath.row]
        navigationController?.pushViewController(articleDetailVC, animated: true)
    }
}

//MARK: - Extension -> CategoryArrayDelegate
///Extension -> CategoryArrayDelegate
extension ArticleSearchViewController : CategoryArrayDelegate
{
    func CategoryData(_selectedCategory dataValue: NSMutableArray) {
        
        selectedCategoryArr = dataValue
        selectedCategoryString = UtilityClass.getCommaSepratedStringFromArray(completeArray: dataValue)
        
        arrayArticleSearchData.removeAll()
        isPaging = true
        
        self.getSearchDataFromAPI()
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension ArticleSearchViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
}
