//
//  WalletViewController.swift
//  MyanCareDoctor
//
//  Created by iOS on 02/02/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD

/// This is the class of UIViewController used to show wallet balance and payment history
class WalletViewController: UIViewController
{
    /// UILabel for available balance string
    @IBOutlet weak var availableBalanceLabel1: UILabel!
    
    /// UILabel for total available balance of patient in wallet
    @IBOutlet weak var availableBalanceLabel: UILabel!
    
    /// UITableView for showing payment history of a patient
    @IBOutlet weak var paymentHistoryTableView: UITableView!
    
    /// UIButton for top up wallet
    @IBOutlet weak var topUpButton: UIButton!
    
    /// UIbutton for Payment History Data
    @IBOutlet weak var paymentHistoryButton: UIButton!
    
    /// variable that makes difference for a UIViewController that it comes from
    var isFromConfirmBooking = false
    
    /// array for paymentHistory data showing in a UITableView
    var paymentHistoryData = [PaymentHistoryModel]()
    
    /// variable using for pagination
    var isPaging = false
    
    //MARK: - viewDidLoad
    ///UIViewController LIfeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        self.tabBarController?.tabBar.isHidden = true
        
        let wallet_balance = UtilityClass.getUserInfoData()["wallet_balance"] as! Double
        let wallet_balance1 = NSString(format : "%.2f", wallet_balance)
        //print("wallet_balance = \(wallet_balance)")
        
        availableBalanceLabel.text = "\(wallet_balance1)"
        
        isPaging = true
        paymentHistoryData.removeAll()
        
        getMyWalletDataFromAPI()
        getMyTransactionsListFromAPI()
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("WalletViewController deinit")
    }
    
    //MARK: - setTableViewDelegate
    
    /// method using to set delegate and datasource for a UITableView and other UIlabel to Localize their title
    func setTableViewDelegate()  {
        
        topUpButton.setTitle("Top up".localized(), for: .normal)
        paymentHistoryButton.setTitle("Payment History".localized(), for: .normal)
        
        availableBalanceLabel1.text = "Available Balance".localized()
        
        paymentHistoryTableView.delegate = self
        paymentHistoryTableView.dataSource = self
        
        paymentHistoryTableView.emptyDataSetSource = self
        paymentHistoryTableView.emptyDataSetDelegate = self
    }
    
    // MARK: - setUpNavigationBar
    
    /// method to set UINavigationbar and title for UIViewController
    func setUpNavigationBar() {
        
        self.title = "My Wallet".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }

    // MARK: - Navigation bar Back Button Action
    
    /// UIbutton action method for UINavigation bar back button
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Mark :- get transactions listing from api
    /// method to get wallet balance from API and show it in UILabel
    func getMyWalletDataFromAPI()
    {
        let urlToHit = EndPoints.myWallet.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let walletDict = responseDictionary["data"] as? [String : Any]
                    
                    guard walletDict != nil else
                    {
                        return
                    }
                    
                    let wallet_balance = walletDict!["wallet_balance"] as! Double
                    let wallet_balance1 = NSString(format : "%.2f", wallet_balance)
                    
                    self.availableBalanceLabel.text = "\(wallet_balance1)"
                        
                    UtilityClass.setWalletBalanceInUserInfo (walletBalance: wallet_balance)
                }
            }
        }
    }
    
    //Mark :- get transactions listing from api
    
    /// method to get payment history data from API
    func getMyTransactionsListFromAPI()
    {
        let urlToHit = EndPoints.myTransactions(paymentHistoryData.count).path
        
        if paymentHistoryData.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    
    /// method used to populate payment history data
    ///
    /// - Parameter array: it contains array of dictionary"[String : Any]"
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = PaymentHistoryModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            paymentHistoryData.append(model) // Adding model to array
        }
        
        paymentHistoryTableView.reloadData()
    }
    
    //MARK:- on add to wallet button action
    
    /// IBAction action method perform on tap of "Add to Wallet" UIButton
    ///
    /// - Parameter sender: sender is UIButton itself
    @IBAction func addToWallletButtonAction(_ sender: Any)
    {
        let paymentVC = UIStoryboard.getWalletStoryBoard().instantiateViewController(withIdentifier: "PaymentVC") as! PaymentViewController
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// MARK: - UITableViewDelegate, UITableViewDataSource methods for Payment History UItableView
extension WalletViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return paymentHistoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell") as! WalletTableViewCell
        
        var amountText = ""
        
        if paymentHistoryData[indexPath.row].type == "credit"
        {
            cell.circleImageView.image = #imageLiteral(resourceName: "wallet-green")
            cell.amountLabel.textColor = UIColor(RED: 2, GREEN: 210, BLUE: 46, ALPHA: 1.0)
            
            amountText = "+ " + paymentHistoryData[indexPath.row].amount
        }
        else
        {
            cell.circleImageView.image = #imageLiteral(resourceName: "wallet-red")
            cell.amountLabel.textColor = UIColor(RED: 255, GREEN: 0, BLUE: 0, ALPHA: 1.0)
            
            amountText = "- " + paymentHistoryData[indexPath.row].amount
        }
        
        cell.dateLabel.text = paymentHistoryData[indexPath.row].dateString
        
        cell.descriptionLabel.text = paymentHistoryData[indexPath.row].description
        
        cell.amountLabel.text = amountText
        
        if paymentHistoryData[indexPath.row].transaction_status == ""
        {
            cell.statusLabel.text = ""
        }
        else
        {
            cell.statusLabel.text = "Set Status".localized() + " : " + paymentHistoryData[indexPath.row].transaction_status
        }
        
        if isPaging && indexPath.row == paymentHistoryData.count - 1
        {
            getMyTransactionsListFromAPI()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (110 * scaleFactorX)
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension WalletViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No Payment History Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16 * scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
}
