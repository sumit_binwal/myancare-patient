//
//  HomeViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import BBBadgeBarButtonItem

class HomeViewController: UIViewController {
    
    var arrayArticle = [ArticleModel]()
    var arrayCategory = [ArticleModel]()
    
    //Mark:- Properties
    @IBOutlet var imageViewProfile: UIImageView!
    @IBOutlet var labelNavtitle: UILabel!
    @IBOutlet var collectionViewGallery: UICollectionView!
    @IBOutlet var tableViewArticleList: UITableView!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        setUpNavigationBar()

        self.getArticleListFromAPI()
        getCategoryListFromAPI()
    }
    
    //MARK: - Deinit
    deinit {
        print("HomeViewController deinit")
    }
    
    // MARK: - setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Home".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if (UtilityClass.getUserSidData()) != nil
        {
            //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(profileButtonPressed))
            
            let profilePictureURL = URL.init(string: UtilityClass.getUserImageData()!)
            
            DispatchQueue.global().async
                {
                    let data = try? Data.init(contentsOf: profilePictureURL!)
                    
                    guard let appDeleg = appDelegate as? AppDelegate else {
                        return
                    }
                    
                    if appDeleg.isInternetAvailable()
                    {
                        DispatchQueue.main.async
                        {
                            let button = UIButton.init(type: .custom)
                            
                            button.setImage(UIImage(data: data!), for: UIControlState.normal)
                            
                            button.addTarget(self, action: #selector(self.profileButtonPressed), for: UIControlEvents.touchUpInside)
                            
                            button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                            
                            button.clipsToBounds = true
                            
                            let barButton = UIBarButtonItem(customView: button)
                            
                            NSLayoutConstraint.activate ([(barButton.customView!.widthAnchor.constraint(equalToConstant: 30)), (barButton.customView!.heightAnchor.constraint(equalToConstant: 30))])
                            
                            barButton.customView?.layer.cornerRadius = 15
                            barButton.customView?.layer.borderColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0).cgColor
                            barButton.customView?.layer.borderWidth = 1.0
                            
                            self.navigationItem.leftBarButtonItem = barButton
                        }
                    }
            }
            
            //var _ : MJBadgeBarButton!
            
            let customButton = UIButton(type: UIButtonType.custom)
            customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
            customButton.addTarget(self, action: #selector(self.messagesButtonPressed), for: .touchUpInside)
            customButton.setImage(#imageLiteral(resourceName: "messages"), for: .normal)
            
            let btnBarBadge = MJBadgeBarButton()
            btnBarBadge.setup(customButton: customButton)
            
            if (UtilityClass.getChatUnReadCount())! > 0 {
                btnBarBadge.badgeValue = "\(UtilityClass.getChatUnReadCount() ?? 0)"
            } else {
                btnBarBadge.badgeValue = "0"
            }
            
            btnBarBadge.badgeOriginX = 20.0
            btnBarBadge.badgeOriginY = -4
            
            self.navigationItem.rightBarButtonItem = btnBarBadge
        }
    }
    
    // MARK: - Navigation Profile Button Action
    @objc func profileButtonPressed() {
        
        let ProfileVC = UIStoryboard.getProfileStoryBoard().instantiateInitialViewController() as! ProfileViewController
        navigationController?.pushViewController(ProfileVC, animated: true)
    }
    
    // MARK: - Navigation Message Button Action
    @objc func messagesButtonPressed () {
        
        let chatListVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    //MARK: - setUpView
    func setUpView() {
        
        imageViewProfile.roundedImageView()
        
        tableViewArticleList.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 12 * scaleFactorX, right: 0)
    }
    
    //MARK: - Get Article APi Calling
    func getArticleListFromAPI()
    {
        let urlToHit = EndPoints.articles.path
        
        //HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
    
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]

                    guard dataDict != nil else
                    {
                        return
                    }

                    self.updateModelArray(usingArray: dataDict!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Get Category APi 
    func getCategoryListFromAPI()
    {
        let urlToHit = EndPoints.categoryList.path
        
        //HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    self.updateCateModelArray(usingArray: dataDict!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //Mark :- Article Like Unlike API
    func articleLikeUnlikeAPI(articleID : String, articleIndex : Int)
    {
        let urlToHit = EndPoints.articleLike.path
        
        let params:[String:Any] = ["article_id":articleID]
        
        //HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.post, parameters: params) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    let isLiked = dataDict!["liked"] as! Bool
                    
                    if isLiked {
                        self.arrayArticle[articleIndex].isLikeByMe = true
                    } else {
                        self.arrayArticle[articleIndex].isLikeByMe = false
                    }
                }
                else
                {
                    self.arrayArticle[articleIndex].isLikeByMe = false
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayArticle.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayArticle.append(model) // Adding model to array
        }
        
        tableViewArticleList.reloadData()
    }
    
    //MARK:- Updating Category Model Array
    func updateCateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayCategory.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateCateModel(usingDictionary: dict) // Updating model
            arrayCategory.append(model) // Adding model to array
        }
        
        collectionViewGallery.reloadData()
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension HomeViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayArticle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: HomeCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_HomeCell) as! HomeCustomCell
        
        if  arrayArticle[indexPath.row].isLikeByMe! {
            cell.buttonLikes.setImage(#imageLiteral(resourceName: "likes-1"), for: .normal)
        } else {
            cell.buttonLikes.setImage(#imageLiteral(resourceName: "unlike"), for: .normal)
        }
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            cell.labelArcTitle.font = UIFont(name: cell.labelArcTitle.font.fontName, size: (15.0 + CGFloat(customFontSize)))
            cell.labelArcDate.font = UIFont(name: cell.labelArcDate.font.fontName, size: (12.0 + CGFloat(customFontSize)))
            cell.labelArcDescription.font = UIFont(name: cell.labelArcDescription.font.fontName, size: (15.0 + CGFloat(customFontSize)))
        }
        
        cell.labelArcTitle.text = arrayArticle[indexPath.row].title
        cell.labelArcDescription.text = arrayArticle[indexPath.row].description_short
        
        cell.ImageViewArcImage.setShowActivityIndicator(true)
        cell.ImageViewArcImage.setIndicatorStyle(.gray)
        
        cell.ImageViewArcImage.sd_setImage(with: URL(string: arrayArticle[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
        
        cell.buttonLikes.setTitle(arrayArticle[indexPath.row].likesCount! + " " + "Likes".localized(), for: .normal)
        cell.buttonViews.setTitle(arrayArticle[indexPath.row].viewCount! + " " + "Views".localized(), for: .normal)
        cell.buttonComments.setTitle(arrayArticle[indexPath.row].commentsCount! + " " + "Comments".localized(), for: .normal)
        
        cell.labelArcDate.text = UtilityClass.getDateStringFromTimeStamp(timeStamp: arrayArticle[indexPath.row].postedTime!) as String
        
        cell.delegate = self
        
        cell.buttonLikes.tag = indexPath.row
        cell.buttonComments.tag = indexPath.row

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        return 386 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleDetailVC = UIStoryboard.getArticlDetailStoryBoard().instantiateInitialViewController() as! ArticleDetailViewController
        
        articleDetailVC.articleModelData = arrayArticle[indexPath.row]
        //articleDetailVC.hidesBottomBarWhenPushed = true
        
        navigationController?.pushViewController(articleDetailVC, animated: true)
    }
}

//Mark:- Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrayCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ArticlesCollectionCustomCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier_ArticlesCollectionCell, for: indexPath) as! ArticlesCollectionCustomCell
    
        cell.imageViewArticleImage.setShowActivityIndicator(true)
        cell.imageViewArticleImage.setIndicatorStyle(.gray)
        
        cell.imageViewArticleImage.sd_setImage(with: URL.init(string: arrayCategory[indexPath.row].categoryImage!), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // your code here
    
        return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let articleVC = UIStoryboard.getArticlesStoryBoard().instantiateInitialViewController() as! ArticlesViewController
        articleVC.cateGoryID = arrayCategory[indexPath.row].categoryId!
        //articleVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(articleVC, animated: true)
    }
}

//MARK: - Extension -> HomeDataCellDelegate
extension HomeViewController : HomeDataCellDelegate
{
    func homeDataCell(_ cell: HomeCustomCell, withButtonTag: Int) {
        
        if (UtilityClass.getUserSidData()) == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0 {
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
            
            return
        }
        
        if cell.buttonLikes.currentImage == #imageLiteral(resourceName: "unlike") {
            cell.buttonLikes.setImage(#imageLiteral(resourceName: "likes-1"), for: .normal)
            
            let likeCount = NSInteger(arrayArticle[withButtonTag].likesCount!)
            
            arrayArticle[withButtonTag].likesCount = String(likeCount!+1)
            
            cell.buttonLikes.setTitle(arrayArticle[withButtonTag].likesCount! + " " + "Likes".localized(), for: .normal)
            
        }
        else
            
        {
            let likeCount = NSInteger(arrayArticle[withButtonTag].likesCount!)
            
            arrayArticle[withButtonTag].likesCount = String(likeCount!-1)
            
            cell.buttonLikes.setTitle(arrayArticle[withButtonTag].likesCount! + " " + "Likes".localized(), for: .normal)
            cell.buttonLikes.setImage(#imageLiteral(resourceName: "unlike"), for: .normal)
        }
        
        articleLikeUnlikeAPI(articleID: arrayArticle[withButtonTag].articleID!, articleIndex: withButtonTag)
    }
    
    func homeDataCellComment(_ cell: HomeCustomCell, withButtonTag: Int) {
        
        if (UtilityClass.getUserSidData()) != nil
        {
            let commentVC = UIStoryboard.getCommentsStoryBoard().instantiateViewController(withIdentifier: "CommentsVC") as! CommentsViewController
            commentVC.articleID = arrayArticle[withButtonTag].articleID!
            //commentVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(commentVC, animated: true)
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0 {
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
        }
    }
}
