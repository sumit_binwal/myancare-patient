//
//  MoreViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import Localize_Swift
import IQKeyboardManagerSwift
import BBBadgeBarButtonItem
import Crashlytics

class MoreViewController: UIViewController {
    
    // MARK: - Variables
    var arrayListItems = NSArray()
    
    //MARK: - Properties
    @IBOutlet weak var tableViewMoreList: UITableView!
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        if (UtilityClass.getUserSidData()) != nil
        {
            if let path = Bundle.main.path(forResource: "MoreList", ofType: "plist")
            {
                arrayListItems = NSArray(contentsOfFile: path)!
                
                if arrayListItems.count > 0
                {
                    tableViewMoreList.reloadData()
                }
            }
        }
        else
        {
            if let path = Bundle.main.path(forResource: "GuestMoreList", ofType: "plist")
            {
                arrayListItems = NSArray(contentsOfFile: path)!
                
                if arrayListItems.count > 0
                {
                    tableViewMoreList.reloadData()
                }
            }
        }
        
        self.setUpNavigationBar()
        
        tableViewMoreList.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 12 * scaleFactorX, right: 0)
    }
    
    //MARK: - Deinit
    deinit {
        print("MoreViewController deinit")
    }
    
    //MARK: - setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "More".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if (UtilityClass.getUserSidData()) != nil
        {
            //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(profileButtonPressed))
            
            let profilePictureURL = URL.init(string: UtilityClass.getUserImageData()!)
            
            DispatchQueue.global().async
                {
                    let data = try? Data.init(contentsOf: profilePictureURL!)
                    
                    guard let appDeleg = appDelegate as? AppDelegate else {
                        return
                    }
                    
                    if appDeleg.isInternetAvailable()
                    {
                        DispatchQueue.main.async
                            {
                                let button = UIButton.init(type: .custom)
                                
                                button.setImage(UIImage(data: data!), for: UIControlState.normal)
                                
                                button.addTarget(self, action: #selector(self.profileButtonPressed), for: UIControlEvents.touchUpInside)
                                
                                button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                                
                                button.clipsToBounds = true
                                
                                let barButton = UIBarButtonItem(customView: button)
                                
                                NSLayoutConstraint.activate ([(barButton.customView!.widthAnchor.constraint(equalToConstant: 30)), (barButton.customView!.heightAnchor.constraint(equalToConstant: 30))])
                                
                                barButton.customView?.layer.cornerRadius = 15
                                barButton.customView?.layer.borderColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0).cgColor
                                barButton.customView?.layer.borderWidth = 1.0
                                
                                self.navigationItem.leftBarButtonItem = barButton
                        }
                    }
            }
            
            let customButton = UIButton(type: UIButtonType.custom)
            customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
            customButton.addTarget(self, action: #selector(self.messagesButtonPressed), for: .touchUpInside)
            customButton.setImage(#imageLiteral(resourceName: "messages"), for: .normal)
            
            let btnBarBadge = MJBadgeBarButton()
            btnBarBadge.setup(customButton: customButton)
            
            if (UtilityClass.getChatUnReadCount())! > 0 {
                btnBarBadge.badgeValue = "\(UtilityClass.getChatUnReadCount() ?? 0)"
            } else {
                btnBarBadge.badgeValue = "0"
            }
            
            btnBarBadge.badgeOriginX = 20.0
            btnBarBadge.badgeOriginY = -4
            
            self.navigationItem.rightBarButtonItem = btnBarBadge
        }
    }
    
    // MARK: - Navigation Profile Button Action
    @objc func profileButtonPressed() {
        
        let ProfileVC = UIStoryboard.getProfileStoryBoard().instantiateInitialViewController() as! ProfileViewController
        navigationController?.pushViewController(ProfileVC, animated: true)
    }
    
    // MARK: - Navigation Message Button Action
    @objc func messagesButtonPressed () {
        
        let chatListVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension MoreViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayListItems.count > 0 {
            return arrayListItems.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //CellForListItems
        let cellObj : MoreCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_MoreCell) as! MoreCustomCell
        
        //CellForSetting
        let cellObjSetting : MoreCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_SettingCell) as! MoreCustomCell
        
        let dict = arrayListItems.object(at: indexPath.row) as! NSDictionary
        if indexPath.row != 8 {
            
            cellObj.buttonListIcons.setImage(UIImage.init(named: dict["Image"] as! String), for: UIControlState.normal)
            
            cellObj.buttonListNames.setTitle((dict["Name"] as? String)?.localized(), for: UIControlState.normal)
            
            if indexPath.row == 0 {
                cellObj.topConstraint.constant = 11
            } else {
                cellObj.topConstraint.constant = 0
            }
            
            return cellObj
            
        } else {
            cellObjSetting.buttonSettingIcon.setImage(UIImage.init(named:dict["Image"] as! String), for: .normal)
    
            cellObjSetting.buttonSettingListName.setTitle((dict["Name"] as? String)?.localized(), for: UIControlState.normal)
            
            cellObjSetting.settingLabel.text = "Settings".localized()
            cellObjSetting.buttonSettingNextScreen.isHidden = true
            cellObjSetting.switchImage.isHidden = false
            
            if (UtilityClass.getNotificationStatus())! {
                cellObjSetting.switchImage.isOn =  true
            } else {
                cellObjSetting.switchImage.isOn =  false
            }
            
            cellObjSetting.delegate = self
            
            return cellObjSetting
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (UtilityClass.getUserSidData()) != nil
        {
            if indexPath.row == 0 {
                //My Profile
                
                let ProfileVC = UIStoryboard.getProfileStoryBoard().instantiateInitialViewController() as! ProfileViewController
                //ProfileVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(ProfileVC, animated: true)
                
            } else if indexPath.row == 1 {
                // Articles
                
                let articleVC = UIStoryboard.getArticlesStoryBoard().instantiateInitialViewController() as! ArticlesViewController
                //articleVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(articleVC, animated: true)
                
            } else if indexPath.row == 2 {
                // Medicine Reminder
                
                let medicineReminderSegmentVC = UIStoryboard.getMedicineReminderStoryBoard().instantiateInitialViewController() as! MedicineReminderSegmentViewController
                //medicineReminderSegmentVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(medicineReminderSegmentVC, animated: true)
                
            } else if indexPath.row == 3 {
                // My Wallet
                
                let myWalletVC = UIStoryboard.getWalletStoryBoard().instantiateInitialViewController() as! WalletViewController
                //myWalletVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(myWalletVC, animated: true)
                
            } else if indexPath.row == 4 {
                // Appointment
                
                let myAppointmentsVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateInitialViewController() as! AppointmentSegmentViewController
                //myAppointmentsVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(myAppointmentsVC, animated: true)
                
            } else if indexPath.row == 5 {
                // Liked
                
                let bookmarkLikeVC = UIStoryboard.getBookmarkLikeStoryBoard().instantiateInitialViewController() as! BookmarkLikeViewController
                
                //bookmarkLikeVC.hidesBottomBarWhenPushed = true
                bookmarkLikeVC.isBookmarkOrLike = false
                
                navigationController?.pushViewController(bookmarkLikeVC, animated: true)
                
            } else if indexPath.row == 6 {
                // Bookmark
                
                let bookmarkLikeVC = UIStoryboard.getBookmarkLikeStoryBoard().instantiateInitialViewController() as! BookmarkLikeViewController
                
                //bookmarkLikeVC.hidesBottomBarWhenPushed = true
                bookmarkLikeVC.isBookmarkOrLike = true
                
                navigationController?.pushViewController(bookmarkLikeVC, animated: true)
                
            } else if indexPath.row == 7 {
                // Sign Out
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Are you sure you want to sign out ?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()]) { (buttonindex) in
                    
                    if buttonindex == 0
                    {
                        self.callLogoutAPI()
                        // delete user data and model data
                        UtilityClass.deleteUserData()
                        
                        let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                        
                        let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                        
                        UtilityClass.changeRootViewController(with: navigationController)
                    }
                }
            } else if indexPath.row == 8 {
                // Notification On
                
                //Crashlytics.sharedInstance().crash()
                
            } else if indexPath.row == 9 {
                // Passcode
                
                let passcodeSettingVC = UIStoryboard.getPasscodeStoryBoard().instantiateViewController(withIdentifier: "PasscodeSettingVC") as! PasscodeSettingViewController
                //passcodeSettingVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(passcodeSettingVC, animated: true)
                
            } else if indexPath.row == 10 {
                // Change password
                
                let changePasswordVC = UIStoryboard.getChangePhoneNumberStoryBoard().instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordViewController
                //changePasswordVC.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(changePasswordVC, animated: true)
                
            } else if indexPath.row == 11 {
                // Change Text Size
                
                // Change Text Size
                let changeTextSizeVC = UIStoryboard.getChangeTextSizeStoryBoard().instantiateViewController(withIdentifier: "ChangeTextSizeVC") as! ChangeTextSizeViewController
                //changeTextSizeVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(changeTextSizeVC, animated: true)
                
            } else if indexPath.row == 12 {
                // Change Language
                
                let optionMenu = UIAlertController(title: nil, message: "Change Language".localized(), preferredStyle: .actionSheet)
                
                let cameraAction = UIAlertAction(title: "English".localized(), style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                    Localize.setCurrentLanguage("en")
                    
                    IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
                    
                    let loginVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController()
                    
                    let navigationController = UINavigationController.init(rootViewController: loginVC!)
                    
                    navigationController.isNavigationBarHidden = true
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                })
                
                let galleryAction = UIAlertAction(title: "Burmese".localized(), style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                    Localize.setCurrentLanguage("my")
                    
                    IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
                    
                    let loginVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController()
                    
                    let navigationController = UINavigationController.init(rootViewController: loginVC!)
                    
                    navigationController.isNavigationBarHidden = true
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                })
                
                let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                })
                
                optionMenu.addAction(cameraAction)
                optionMenu.addAction(galleryAction)
                optionMenu.addAction(cancelAction)
                
                self.present(optionMenu, animated: true, completion: nil)
                
            } else if indexPath.row == 13 {
                // Invite Your Friends
                
                let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: ["shareItem"], applicationActivities:nil)
                activityViewController.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
                
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: nil);
                }
                
            } else if indexPath.row == 14 {
                // Feedback
            } else {
                // About Us
                
                let aboutUsVC = UIStoryboard.getAboutUsStoryBoard().instantiateInitialViewController() as! AboutUsViewController
                //aboutUsVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(aboutUsVC, animated: true)
            }
        }
        else
        {
            if indexPath.row == 0
            {
                // Sign In
                
                let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                
                let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                
                UtilityClass.changeRootViewController(with: navigationController)
                
            } else if indexPath.row == 1 {
                // Invite Your Friends
                
                let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: ["shareItem"], applicationActivities:nil)
                activityViewController.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
                
                DispatchQueue.main.async {
                    self.present(activityViewController, animated: true, completion: nil);
                }
                
            } else if indexPath.row == 2 {
                // Feedback
            } else {
                // About Us
                
                let aboutUsVC = UIStoryboard.getAboutUsStoryBoard().instantiateInitialViewController() as! AboutUsViewController
                //aboutUsVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(aboutUsVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row != 8 {
            if indexPath.row == 0 {
                return 64*scaleFactorX
            } else {
                return 53*scaleFactorX
            }
        } else {
            return 96*scaleFactorX
        }
    }
    
    //MARK: - Call update profile API
    func callUpdateNotificationSettingAPI()
    {
        let params:[String:Any] = [
            "notification_setting" : UtilityClass.getNotificationStatus()!
        ]
        
        print(params)
        
        let urlToHit =  EndPoints.update_profile.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let responseData = responseDictionary["data"] as! [String:Any]
                    print(responseData)
                    
                    UtilityClass.saveNotificationStatus(userDict: responseData["notification_setting"]! as! Bool)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Call Logout API
    func callLogoutAPI()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let params:[String:Any] = ["device_token":UtilityClass.getDeviceFCMToken()!]
        
        print(params)
        
        let urlToHit =  EndPoints.logoutUser.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    // delete user data and model data
                    UtilityClass.deleteUserData()
                    SocketManagerHandler.sharedInstance().disconnectSocket()

                    let loginVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController()
                    
                    let navigationController = UINavigationController.init(rootViewController: loginVC!)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

extension MoreViewController:MoreNotificationSwitchDelegate
{
    func moreNotificationStatusCell(_ cell: MoreCustomCell, notificationStatus: Bool)
    {
        callUpdateNotificationSettingAPI()
    }
}
