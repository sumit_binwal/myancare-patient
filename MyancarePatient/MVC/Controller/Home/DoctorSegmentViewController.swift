//
//  DoctorSegmentViewController.swift
//  MyancarePatient
//
//  Created by iOS on 29/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import ScrollableSegmentedControl

var modelDoctorFilterProcess = ModelDoctorFilter()

class DoctorSegmentViewController: UIViewController
{
    @IBOutlet fileprivate weak var segmentedControl: ScrollableSegmentedControl!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet fileprivate weak var containerView: UIView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    private var pendingRequestWorkItem: DispatchWorkItem?
    
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    var selectedCategoryString = ""
    var selectedCategoryArr : NSMutableArray = []
    
    var isFromMoreScreen = false
    
    fileprivate lazy var viewControllers: [UIViewController] = {
        return self.preparedViewControllers()
    }()
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
        
        setupScrollView()
        
        segmentedControl.segmentStyle = .imageOnTop
        
        segmentedControl.selectedSegmentContentColor = UIColor.MyanCarePatient.colorForDoctorSegmentSelected
        
        segmentedControl.underlineSelected = true
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.insertSegment(withTitle: "All Doctors".localized(), image: #imageLiteral(resourceName: "recent"), at: 0)
        segmentedControl.insertSegment(withTitle: "Recent".localized(), image: #imageLiteral(resourceName: "recent"), at: 1)
        segmentedControl.insertSegment(withTitle: "Favourites".localized(), image: #imageLiteral(resourceName: "favourite"), at: 2)
        
        segmentedControl.addTarget(self, action: #selector(DoctorSegmentViewController.segmentSelected(sender:)), for: .valueChanged)
    }

    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
        
        let contentOffsetX = scrollView.frame.width * CGFloat(sender.selectedSegmentIndex)
        scrollView.setContentOffset(CGPoint(x: contentOffsetX, y: 0), animated: true)
        
        updateValues()
        
        //print(sender.selectedSegmentIndex)
        //print(sender.inputViewController)
        
        for controller in (sender.viewController()?.childViewControllers)!
        {
            if LocationManager.sharedInstance().isLocationAccessAllowed()
            {
                if sender.selectedSegmentIndex == 0
                {
                    if controller.isKind(of: DoctorsViewController.self)
                    {
                        let controller1 = controller as! DoctorsViewController
                        controller1.getAllDoctorsApiNotification()
                        
                        return
                    }
                }
                
                if sender.selectedSegmentIndex == 1
                {
                    if controller.isKind(of: RecentViewController.self)
                    {
                        let controller1 = controller as! RecentViewController
                        controller1.getRecentDoctorsApiNotification()
                        
                        return
                    }
                }
                
                if sender.selectedSegmentIndex == 2
                {
                    if controller.isKind(of: FavouritesViewController.self)
                    {
                        let controller1 = controller as! FavouritesViewController
                        controller1.getFavouritesDoctorsApiNotification()
                        
                        return
                    }
                }
            }
            else
            {
                UtilityClass.showAlertWithTitle(title: "", message: "To Get Doctor Listing, Please Enable Your Location First".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            }
        }
    }
    
    //MARK: - Deinit
    deinit {
        print("DoctorSegmentViewController deinit")
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        
        UtilityClass.disableIQKeyboard()
        
        if searchTextField.text == "" {
            searchTextField.isHidden = true
        } else {
            searchTextField.isHidden = false
            backButton.isHidden = false
        }
        
        self.view.endEditing(true)
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UtilityClass.enableIQKeyboard()
    }
    
    //MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - setUpView
    func setUpView () {
        
        if isFromMoreScreen {
            self.backButton.isHidden = false
        } else {
            self.backButton.isHidden = true
        }
        
        searchTextField.delegate = self
    }
    
    // Example viewControllers
    
    fileprivate func preparedViewControllers() -> [UIViewController] {
        
        let storyboard = self.storyboard
        
        let firstViewController = storyboard?
            .instantiateViewController(withIdentifier: "DoctorsVC") as! DoctorsViewController
        
        let secondViewController = storyboard?
            .instantiateViewController(withIdentifier: "RecentVC") as! RecentViewController
        
        let thirdViewController = storyboard?
            .instantiateViewController(withIdentifier: "FavouritesVC") as! FavouritesViewController
        
        return [
            firstViewController,
            secondViewController,
            thirdViewController
        ]
    }
    
    // MARK: - Setup container view
    
    fileprivate func setupScrollView() {
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated() {
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            addChildViewController(viewController)
        
            scrollView.addSubview(viewController.view)
            //scrollView.addSubview(viewController.view, options: .useAutoresize) // module's extension
            
            viewController.didMove(toParentViewController: self)
        }
        
        self.titleLabel.text = "Doctors".localized()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - on Filter Button Action
    @IBAction func filterButtonAction(_ sender: Any) {
        
        let filterVC = UIStoryboard.getDoctorFilterStoryBoard().instantiateInitialViewController() as! DoctorFilterViewController
        
        filterVC.selectedIndex = segmentedControl.selectedSegmentIndex
        
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    //MARK: - On Search Button Action
    @IBAction func searchButtonAction(_ sender: Any) {
        
        searchTextField.isHidden = false
        searchTextField.autocorrectionType = .no
        
        searchBtn.isHidden = true
        titleLabel.isHidden = true
        
        searchTextField.placeholder = "Find a Doctor".localized()
        searchTextField.becomeFirstResponder()
        
        backButton.isHidden = false
    }
    
    //MARK: - on Back Button action
    @IBAction func backButtonAction(_ sender: Any) {
        
        if searchTextField.isHidden {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.view.endEditing(true)
            searchTextField.isHidden = true
            
            searchBtn.isHidden = false
            titleLabel.isHidden = false
            
            searchTextField.text = ""
            
            modelDoctorFilterProcess.name = ""
            
            callSearchNotification()
            
            if isFromMoreScreen {
                backButton.isHidden = false
            } else {
                backButton.isHidden = true
            }
        }
    }
    
    func updateValues() {
        
        if searchTextField.isHidden {
            
        } else {
            self.view.endEditing(true)
            searchTextField.isHidden = true
            
            searchBtn.isHidden = false
            titleLabel.isHidden = false
            
            searchTextField.text = ""
            
            modelDoctorFilterProcess.name = ""
            
            if isFromMoreScreen {
                backButton.isHidden = false
            } else {
                backButton.isHidden = true
            }
        }
        
        modelDoctorFilterProcess.d_pcode = ""
        modelDoctorFilterProcess.district = ""
        modelDoctorFilterProcess.locationArry = []
        modelDoctorFilterProcess.selectedCategoryId = ""
        modelDoctorFilterProcess.sr_pcode = ""
        modelDoctorFilterProcess.state = ""
        modelDoctorFilterProcess.town = ""
        modelDoctorFilterProcess.town_pcode = ""
        modelDoctorFilterProcess.townID = ""
    }
    
    func callSearchNotification() {
        
        if segmentedControl.selectedSegmentIndex == 0 {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorAll"), object: nil, userInfo: nil)
        } else if segmentedControl.selectedSegmentIndex == 1 {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorRecent"), object: nil, userInfo: nil)
        } else {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorFavourites"), object: nil, userInfo: nil)
        }
    }
}

extension DoctorSegmentViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        segmentedControl.selectedSegmentIndex = Int(currentPage)
        
        updateValues()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
}

//Extension -> UITextField -> UITextFieldDelegate
extension DoctorSegmentViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        var name = textField.text! as NSString
        if name.contains(" ") {
            name = name.replacingOccurrences(of: " ", with: "%20") as NSString
        }
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            modelDoctorFilterProcess.name = name as String
            
            self?.callSearchNotification()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            modelDoctorFilterProcess.name = ""
            
            self?.callSearchNotification()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if textField.text?.count == 1
            {
                // Cancel the currently pending item
                pendingRequestWorkItem?.cancel()
                
                // Wrap our request in a work item
                let requestWorkItem = DispatchWorkItem { [weak self] in
                    
                    modelDoctorFilterProcess.name = ""
                    
                    self?.callSearchNotification()
                }
                
                // Save the new work item and execute it after 250 ms
                pendingRequestWorkItem = requestWorkItem
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
            }
        }
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                newString = textField.text
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if (newString?.count)! >= 3
        {
            var name = newString! as NSString
            if name.contains(" ")
            {
                name = name.replacingOccurrences(of: " ", with: "%20") as NSString
            }
            
            print("name = \(name)")
            
            // Cancel the currently pending item
            pendingRequestWorkItem?.cancel()
            
            // Wrap our request in a work item
            let requestWorkItem = DispatchWorkItem { [weak self] in
                
                modelDoctorFilterProcess.name = name as String
                
                self?.callSearchNotification()
            }
            
            // Save the new work item and execute it after 250 ms
            pendingRequestWorkItem = requestWorkItem
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        }
        
        return true
    }
}
