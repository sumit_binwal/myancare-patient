//
//  FavouritesViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class FavouritesViewController: UIViewController {

    //Mark: - Properties
    @IBOutlet weak var tableViewFavouriteDoctorList: UITableView!
    
    var doctorArr = [DoctorModel]()
    
    var isPaging = false
    
    var refreshControl1 = UIRefreshControl()
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *)
        {
            tableViewFavouriteDoctorList?.refreshControl = refreshControl1
        }
        else
        {
            tableViewFavouriteDoctorList?.addSubview(refreshControl1)
        }
        
        // Configure Refresh Control
        refreshControl1.addTarget(self, action: #selector(refreshDoctorData(_:)), for: .valueChanged)
        
        if LocationManager.sharedInstance().isLocationAccessAllowed()
        {
            doctorArr.removeAll()
            isPaging = true
            
            //AppWebHandler.sharedInstance().cancelTask(forEndpoint: "doctor?")
            getFavouritesDoctorsApi()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "", message: "To Get Doctor Listing, Please Enable Your Location First".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector (getFavouritesDoctorsApiNotification), name: NSNotification.Name(rawValue: "applyFilterDoctorFavourites"), object: nil)
    }
    
    //MARK: - Deinit
    deinit {
        print("FavouritesViewController deinit")
    }
    
    //MARK: - setUpTableView
    func setUpTableView() {
        
        tableViewFavouriteDoctorList.delegate = self
        tableViewFavouriteDoctorList.dataSource = self
        
        tableViewFavouriteDoctorList.emptyDataSetSource = self
        tableViewFavouriteDoctorList.emptyDataSetDelegate = self
        
        tableViewFavouriteDoctorList.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 12 * scaleFactorX, right: 0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func refreshDoctorData(_ sender: Any) {
        
        doctorArr.removeAll()
        isPaging = true
        
       // AppWebHandler.sharedInstance().cancelTask(forEndpoint: "doctor?")
        getFavouritesDoctorsApi()
    }
    
    @objc func getFavouritesDoctorsApiNotification() {
        
        doctorArr.removeAll()
        isPaging = true
        
        //AppWebHandler.sharedInstance().cancelTask(forEndpoint: "doctor?")
        getFavouritesDoctorsApi()
    }
    
    //MARK: - get all doctors list from API
    @objc func getFavouritesDoctorsApi() {
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.doctor(String(LocationManager.sharedInstance().newLongitude), String(LocationManager.sharedInstance().newLatitude), "5000000", modelDoctorFilterProcess.sr_pcode!, modelDoctorFilterProcess.d_pcode!, modelDoctorFilterProcess.town_pcode!, modelDoctorFilterProcess.name!, modelDoctorFilterProcess.selectedCategoryId!, "1", "", doctorArr.count).path
        
        print("favourites doctor list url = \(urlToHit)")
        
        if doctorArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().cancelTask(forEndpoint: "doctor?")
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                //if error?.localizedDescription == "The network connection was lost."
                //{
                //    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                //}
                //else
                //{
                //    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                //}
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let doctorData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard doctorData != nil else
                    {
                        return
                    }
                    
                    self.updateDoctorModelArray(usingArray: doctorData!)
                }
                else
                {
                    HUD.hide()
                    self.refreshControl1.endRefreshing()
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Comment Model Array
    func updateDoctorModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 || dataArray.count < 10 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = DoctorModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            doctorArr.append(model) // Adding model to array
        }
        
        tableViewFavouriteDoctorList.reloadData()
        
        HUD.hide()
        self.refreshControl1.endRefreshing()
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension FavouritesViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctorArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: DoctorsCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_DoctorsCell) as! DoctorsCustomCell
        
        if doctorArr.count > 0
        {
            cell.labelDoctorName.text = doctorArr[indexPath.row].doctorName
            
            let arrSpec : NSArray = doctorArr[indexPath.row].specializations! as NSArray
            var specilaization = ""
            
            for i in 0 ..< arrSpec.count {
                let dict = arrSpec.object(at: i) as! NSDictionary
                specilaization = specilaization.appending(dict.object(forKey: "name") as! String)
                
                if i != arrSpec.count - 1 {
                    specilaization = specilaization.appending(", ")
                }
            }
            
            cell.labelDoctorType.text = specilaization
            
            cell.labelDoctorLocation.text = doctorArr[indexPath.row].districttown?["town"] as? String
            
            cell.imageViewProfilePic.setShowActivityIndicator(true)
            cell.imageViewProfilePic.setIndicatorStyle(.gray)
            
            cell.imageViewProfilePic.sd_setImage(with: URL(string: doctorArr[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
            
            if doctorArr[indexPath.row].online_status == "Online" {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "greenDot")
            } else if doctorArr[indexPath.row].online_status == "Busy" {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "redDot")
            } else {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "greyDot")
            }
            
            cell.labelDoctorRating.text = doctorArr[indexPath.row].avg_review
            
            cell.delegate = self
            cell.tag = indexPath.row
            
            if !doctorArr[indexPath.row].isStatusUpdated!
            {
                cell.getDoctorStatusApi(doctor_id: doctorArr[indexPath.row].doctorID!, index: indexPath.row)
            }
            
            if isPaging && indexPath.row == doctorArr.count - 1 {
                getFavouritesDoctorsApi()
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 106 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if doctorArr.count > 0
        {
            let doctorDetailVC = UIStoryboard.getDoctorDetailStoryBoard().instantiateInitialViewController() as! DoctorDetailViewController
            
            doctorDetailVC.doctor_id = doctorArr[indexPath.row].doctorID!
    
            self.navigationController?.pushViewController (doctorDetailVC, animated: true)
        }
    }
}

extension FavouritesViewController : DoctorsCustomCellDelegate
{
    func DoctorsCustomCellDoctorStatus(_ cell: DoctorsCustomCell, dataDict: [String : Any])
    {
        let status = (dataDict["current_status"] as! String).capitalized
        
        if doctorArr.count > 0  && cell.tag < self.doctorArr.count
        {
            self.doctorArr[cell.tag].online_status = status
            self.doctorArr[cell.tag].isStatusUpdated = true
            
            if status == "Online"
            {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "greenDot")
            }
            else if status == "Busy"
            {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "redDot")
            }
            else
            {
                cell.imageViewStatus.image = #imageLiteral(resourceName: "greyDot")
            }
        }
    }
    
    func DoctorsCustomCellImageViewClick(_ cell: DoctorsCustomCell)
    {
        if doctorArr.count > 0
        {
            let tag = cell.tag
            
            let doctorDetailVC = UIStoryboard.getDoctorDetailStoryBoard().instantiateInitialViewController() as! DoctorDetailViewController
            
            doctorDetailVC.doctor_id = doctorArr[tag].doctorID!
            
            self.navigationController?.pushViewController (doctorDetailVC, animated: true)
        }
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension FavouritesViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
}
