//
//  BookmarkLikeViewController.swift
//  MyancarePatient
//
//  Created by iOS on 31/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class BookmarkLikeViewController: UIViewController {
///UITableView Refrence - BookmarkLike Table View
    @IBOutlet weak var bookmarkLikeTableView: UITableView!
    ///Bool Variable - is Bookmark Liked or Not
    var isBookmarkOrLike = true
    ///Article Model Array - bookmark Like Data Array
    var bookmarkLikeDataArr = [ArticleModel]()
    
    // MARK: - viewDidLoad
    ///UIViewController life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }

    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("BookmarkLikeViewController deinit")
    }
    
    //MARK: - setUpTableView
    ///SetupTable View method
    func setUpTableView() {
        
        bookmarkLikeTableView.delegate = self
        bookmarkLikeTableView.dataSource = self
        
        bookmarkLikeTableView.emptyDataSetSource = self
        bookmarkLikeTableView.emptyDataSetDelegate = self
    }
    
    // MARK: - setUpNavigationBar
    ///setupNavigation bar Method
    func setUpNavigationBar() {
        
        if isBookmarkOrLike {
            self.title = "Bookmarks".localized()
            getMyBookmarksAPI()
        } else {
            self.title = "Likes".localized()
            getMyLikesAPI()
        }
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(searchButtonPressed))
    }
    
    // MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation bar Search Button Action
    ///Navigation bar Search Button Action
    @objc func searchButtonPressed () {
        
        let articleSearchVC = UIStoryboard.getArticlesStoryBoard().instantiateViewController(withIdentifier: "ArticleSearchVC") as! ArticleSearchViewController
        self.navigationController?.pushViewController(articleSearchVC, animated: true)
    }

    
    //Mark :- get Bookmarks listing API Calling
    ///get Bookmarks listing API Calling
    func getMyBookmarksAPI()
    {
        let urlToHit = EndPoints.myBookmark.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let bookmarkLikeData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard bookmarkLikeData != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: bookmarkLikeData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //Mark :- get Likes listing API Calling
    ///get Likes listing API Calling
    func getMyLikesAPI() {
        
        let urlToHit = EndPoints.myLikes.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let bookmarkLikeData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard bookmarkLikeData != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: bookmarkLikeData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    ///Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        bookmarkLikeDataArr.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            bookmarkLikeDataArr.append(model) // Adding model to array
        }
        
        bookmarkLikeTableView.reloadData()
    }
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension BookmarkLikeViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bookmarkLikeDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ArticlesTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_ArticlesTableCell) as! ArticlesTableCustomCell
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            cell.labelArticleDate.font = UIFont(name: cell.labelArticleDate.font.fontName, size: (12.0 + CGFloat(customFontSize)))
            
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: bookmarkLikeDataArr[indexPath.row].title!, location: 0, length: bookmarkLikeDataArr[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: (15.0 + CGFloat(customFontSize))))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: bookmarkLikeDataArr[indexPath.row].description_short!, location: 0, length: bookmarkLikeDataArr[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: (14.0 + CGFloat(customFontSize))))
            
            cell.labelArticleTitle.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleTitle.attributedText, lineHeightMultiple: 0.8)
            
            cell.labelArticleDescription.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleDescription.attributedText, lineHeightMultiple: 0.8)
        }
        else
        {
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: bookmarkLikeDataArr[indexPath.row].title!, location: 0, length: bookmarkLikeDataArr[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: 15))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: bookmarkLikeDataArr[indexPath.row].description_short!, location: 0, length: bookmarkLikeDataArr[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: 14))
        }
        
        cell.imageViewArticleImage.setShowActivityIndicator(true)
        cell.imageViewArticleImage.setIndicatorStyle(.gray)
        
        cell.imageViewArticleImage.sd_setImage(with: URL.init(string: bookmarkLikeDataArr[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        
        cell.buttonTotalComments.setTitle(bookmarkLikeDataArr[indexPath.row].commentsCount!, for: .normal)
        
        cell.labelArticleDate.text = UtilityClass.getDateStringFromTimeStamp(timeStamp: bookmarkLikeDataArr[indexPath.row].postedTime!) as String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleDetailVC = UIStoryboard.getArticlDetailStoryBoard().instantiateInitialViewController() as! ArticleDetailViewController
        
        articleDetailVC.articleModelData = bookmarkLikeDataArr[indexPath.row]
        articleDetailVC.hidesBottomBarWhenPushed = true
        
        navigationController?.pushViewController(articleDetailVC, animated: true)
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension BookmarkLikeViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
