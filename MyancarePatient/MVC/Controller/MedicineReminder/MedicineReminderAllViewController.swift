//
//  MedicineReminderAllViewController.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD
///MedicineReminderAll View Controller - UiViewController Class
class MedicineReminderAllViewController: UIViewController
{
    ///UITableView Refrence - Medicine TableView
    @IBOutlet weak var medicineTableView: UITableView!
    
    ///MedicationReminder Model Array Refrence Variable
    var medicationReminderDataArr = [MedicationReminderModel]()
    
    ///Variable - delete MedicationReminder ID
    var delete_medicationreminder_id = ""
    
    ///Bool Variable - is Paging Applied or Not
    var isPaging = false
    
    //MARK:- viewDidLoad
    ///UiViewController Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector (someActionAllReminder), name: NSNotification.Name(rawValue: "someActionAllReminder"), object: nil)
        
        medicationReminderDataArr.removeAll()
        isPaging = true
        
        getMedicationReminderListFromAPI()
    }
    
    //MARK: - Deinit
    deinit
    {
        print("MedicineReminderAllViewController deinit")
    }
    
    //MARK:- setTableViewDelegate
    ///Set TableView Delegate Method
    func setTableViewDelegate()
    {
        medicineTableView.delegate = self
        medicineTableView.dataSource = self
        
        medicineTableView.emptyDataSetSource = self
        medicineTableView.emptyDataSetDelegate = self
    }
    
    /// Fetch All Meditaion Data API Call Method
    func callAllMedicationApi()
    {
        medicationReminderDataArr.removeAll()
        isPaging = true
        
        getMedicationReminderListFromAPI()
    }
    
    ///Some Action All Reminder
    @objc func someActionAllReminder()
    {
        medicationReminderDataArr.removeAll()
        isPaging = true
        
        getMedicationReminderListFromAPI()
    }
    
    //Mark :- get medication reminder listing from api
    ///Get medication reminder listing from api
    func getMedicationReminderListFromAPI()
    {
        let urlToHit = EndPoints.getMedicationReminder (medicationReminderDataArr.count).path
        
        if medicationReminderDataArr.count == 0
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    /// Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = MedicationReminderModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            medicationReminderDataArr.append(model) // Adding model to array
        }
        
        medicineTableView.reloadData()
    }
    
    //MARK:- get delete medication reminder params
    /// get delete medication reminder params
    func getDeleteMedicineReminderParametersdata () -> [String : String]
    {
        let dictDeleteMedicuneReminder = [
            "medicationreminder_id" : delete_medicationreminder_id
            ] as [String : String]
        
        return dictDeleteMedicuneReminder
    }
    
    //MARK: - Call delete medication reminder API
    ///Call delete medication reminder API
    func callDeleteMedicationReminderAPi()
    {
        self.view.endEditing(true)
        
        let params = getDeleteMedicineReminderParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.medicationReminder.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "someActionTodayReminder"), object: nil)
                        
                        self.medicationReminderDataArr.removeAll()
                        self.isPaging = true
                        
                        self.getMedicationReminderListFromAPI()
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension MedicineReminderAllViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return medicationReminderDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicineReminderTableViewCell") as! MedicineReminderTableViewCell
        
        cell.medicineName.text = medicationReminderDataArr[indexPath.row].drug_name
        
        cell.medicineDescription.text = medicationReminderDataArr[indexPath.row].type
        
        if medicationReminderDataArr[indexPath.row].type == "Tablet to disolve".localized()
        {
            cell.medicineImage.image = #imageLiteral(resourceName: "Tablet-to-disolve-icon")
        }
        else if medicationReminderDataArr[indexPath.row].type == "Capsule".localized()
        {
            cell.medicineImage.image = #imageLiteral(resourceName: "Capsule-icon")
        }
        else if medicationReminderDataArr[indexPath.row].type == "Tablet".localized()
        {
            cell.medicineImage.image = #imageLiteral(resourceName: "Tablet-icon")
        }
        else if medicationReminderDataArr[indexPath.row].type == "Liquid by drops".localized()
        {
            cell.medicineImage.image = #imageLiteral(resourceName: "Liquid-by-drops-icon")
        }
        else
        {
            cell.medicineImage.image = #imageLiteral(resourceName: "Liquid-by-spon-icon")
        }
        
        cell.delegate = self
        
        cell.deleteButton.tag = indexPath.row
        cell.editButton.tag = indexPath.row
        
        if medicationReminderDataArr[indexPath.row].frequency == 24
        {
            cell.medicineTime.text = UtilityClass.getTimeFromDateString((medicationReminderDataArr[indexPath.row].frequency_time_long?[0])!)
        }
        else
        {
            cell.medicineTime.text = ""
            
            for (_, dateString) in (medicationReminderDataArr[indexPath.row].frequency_time_long?.enumerated())!
            {
                if cell.medicineTime.text == ""
                {
                    let date = UtilityClass.getDateFromDateString(dateString)
                    let nowDate = Date()
                    
                    let cal = Calendar (identifier: .gregorian)
                    
                    let order = cal.compare(nowDate, to: date, toGranularity: .minute)
                    
                    switch order
                    {
                    
                    case .orderedAscending:
                        print("orderedAscending")
                        cell.medicineTime.text = UtilityClass.getTimeFromDateString(dateString)
                        break
                        
                    default:
                        break
                    }
                }
                else
                {
                    break
                }
            }
        }
        
        let stringDay = UtilityClass.getDisplayDayFromTime(timeString: cell.medicineTime.text!)
        
        if stringDay == "Morning".localized()
        {
            cell.medicineTimeImage.image = #imageLiteral(resourceName: "medicine-time-morning")
        }
        else if stringDay == "Evening".localized()
        {
            cell.medicineTimeImage.image = #imageLiteral(resourceName: "medicine-time-evening")
        }
        else if stringDay == "Night".localized()
        {
            cell.medicineTimeImage.image = #imageLiteral(resourceName: "medicine-time-night")
        }
        else
        {
            cell.medicineTimeImage.image = #imageLiteral(resourceName: "medicine-time-afternoon")
        }
        
        if isPaging && indexPath.row == medicationReminderDataArr.count - 1
        {
            getMedicationReminderListFromAPI()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (84 * scaleFactorX)
    }
}

///Medicine Reminder TableView Cell Delegate Extension
extension MedicineReminderAllViewController : MedicineReminderTableViewCellDelegate
{
    
    /// WhenTap On Edit Button Action to Redirect EditMedicine Reminder Page
    ///
    /// - Parameter cell: MedicineReminderTableView Cell Refrence
    func myRecordCellEditButtonAction(_ cell: MedicineReminderTableViewCell)
    {
        let tag = cell.editButton.tag
        
        let addMedicineRecordVC = UIStoryboard.getMedicineReminderStoryBoard().instantiateViewController(withIdentifier: "AddMedicineRecordVC") as! AddMedicineRecordViewController
        
        addMedicineRecordVC.isEditMode = true
        addMedicineRecordVC.medicationReminderDataArr = medicationReminderDataArr[tag]
        
        self.navigationController?.pushViewController(addMedicineRecordVC, animated: true)
    }
    
    
    /// MyRecord Delete Button Clicked Event- To Delete MyMedical Record From Listing
    ///
    /// - Parameter cell: MedicineReminderTableViewCell Refrence
    func myRecordCellDeleteButtonAction(_ cell: MedicineReminderTableViewCell)
    {
        let tag = cell.deleteButton.tag
        
        UtilityClass.showAlertWithTitle(title: "", message: "Do you really want to delete this record?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()], dismissHandler: { (buttonIndex) in
            
            if buttonIndex == 0
            {
                self.delete_medicationreminder_id = self.medicationReminderDataArr[tag].meditationId
                self.callDeleteMedicationReminderAPi()
            }
        })
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension MedicineReminderAllViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
}
