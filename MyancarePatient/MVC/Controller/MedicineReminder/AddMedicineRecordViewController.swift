//
//  AddMedicineRecordViewController.swift
//  MyancarePatient
//
//  Created by iOS on 12/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

//MARK: -
///AddMedicineRecordModel Refrence
var modelAddMedicineProcess = AddMedicneRecordModel ()

class AddMedicineRecordViewController: UIViewController {

    ///UIView Outlet Refrence Variable
    @IBOutlet weak var editView: UIView!
    
    ///UITableView Outlet Refrence Variable
    @IBOutlet weak var addMedicineTableView: UITableView!
    
    ///MedicalRemiderModel Refrence Variable
    var medicationReminderDataArr = MedicationReminderModel()
    
    ///Bool Variable Refrence - is Edit Mode On/Off
    var isEditMode = false
    
    ///UIButton Outlet Refrence - Update Button
    @IBOutlet weak var updateButton: UIButton!
    
    ///UIButton Outlet Refrence - Save Button
    @IBOutlet weak var saveButton: UIButton!
    
    //MARK:- viewDidLoad
    ///UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }

    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        modelAddMedicineProcess = AddMedicneRecordModel.init()
        
        setUpNavigationBar()
        
        setUpView()
        
        updateButton.setTitle("Update".localized(), for: .normal)
        
        saveButton.setTitle("Save".localized(), for: .normal)
    }
    
    //MARK: - Deinit
    deinit {
        print("AddMedicineRecordViewController deinit")
    }
    
    //MARK:- setUpView
    ///Setup View Method
    func setUpView() {
        
        if isEditMode {
            
            editView.isHidden = false
            
            modelAddMedicineProcess.medicationID = medicationReminderDataArr.meditationId
            
            modelAddMedicineProcess.drugName = medicationReminderDataArr.drug_name!
            
            modelAddMedicineProcess.medicationFor = medicationReminderDataArr.medication_for!
            
            modelAddMedicineProcess.medicationType = medicationReminderDataArr.type!
            
            modelAddMedicineProcess.repeatation = "\(medicationReminderDataArr.frequency ?? 0) Hr"
            modelAddMedicineProcess.frequency = medicationReminderDataArr.frequency!
            
            modelAddMedicineProcess.from = medicationReminderDataArr.from_date!
            modelAddMedicineProcess.from_date_long = UtilityClass.getTimeStampFromDate(dateString: modelAddMedicineProcess.from)
            
            modelAddMedicineProcess.to = medicationReminderDataArr.to_date!
            modelAddMedicineProcess.to_date_long = UtilityClass.getTimeStampFromDate(dateString: modelAddMedicineProcess.to)
            
            modelAddMedicineProcess.startTime = medicationReminderDataArr.start_time!
            
            //print("\(modelAddMedicineProcess.from ) \(modelAddMedicineProcess.startTime )")
            
            modelAddMedicineProcess.start_time_long = UtilityClass.getTimeStampFromTime(dateString: "\(modelAddMedicineProcess.from ) \(modelAddMedicineProcess.startTime )")
            
        } else {
       
            editView.isHidden = true
            
            modelAddMedicineProcess.repeatation = "24 Hr"
            let inputValue1 = modelAddMedicineProcess.repeatation.replacingOccurrences(of: " Hr", with: "")
            modelAddMedicineProcess.frequency = Int(inputValue1)!
        }
    }
    
    //MARK:- setUpTableView
    ///Setup TableView Method

    func setUpTableView () {
        
        addMedicineTableView.delegate = self
        addMedicineTableView.dataSource = self
    }
    
    //MARK:- setUpNavigationBar
    
    /// Setup Navigation Bar Method
    func setUpNavigationBar() {
        
        self.title = "Medication Reminder".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    /// Back Button Clicked Action Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- save button action
    /// Save Button Clicked Action Method
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if validateTheInputField() {
            
            if isEditMode {
                callUpdateMedicationReminderAPi()
            } else {
                callAddMedicationReminderAPi()
            }
        }
    }
    
    //MARK:- get add medication reminder params
    /// get add medication reminder params
    func getAddMedicineReminderParametersdata () -> [String : Any]
    {
        var dictAddMedicuneReminder : [String : Any]
        
        if modelAddMedicineProcess.from == modelAddMedicineProcess.to
        {
            dictAddMedicuneReminder = [
                "drug_name" : modelAddMedicineProcess.drugName ,
                "for" : modelAddMedicineProcess.medicationFor ,
                "type" : modelAddMedicineProcess.medicationType ,
                "frequency" : 0 ,
                "from_date_long" : modelAddMedicineProcess.from_date_long ,
                "to_date_long" : modelAddMedicineProcess.to_date_long ,
                "start_time_long" : modelAddMedicineProcess.start_time_long
                ] as [String : Any]
        }
        else
        {
            dictAddMedicuneReminder = [
                "drug_name" : modelAddMedicineProcess.drugName ,
                "for" : modelAddMedicineProcess.medicationFor ,
                "type" : modelAddMedicineProcess.medicationType ,
                "frequency" : modelAddMedicineProcess.frequency ,
                "from_date_long" : modelAddMedicineProcess.from_date_long ,
                "to_date_long" : modelAddMedicineProcess.to_date_long ,
                "start_time_long" : modelAddMedicineProcess.start_time_long
                ] as [String : Any]
        }

        return dictAddMedicuneReminder
    }
    
    //MARK: - Call add medication reminder API
///Call add medication reminder API
    func callAddMedicationReminderAPi()
    {
        self.view.endEditing(true)
        
        let params = getAddMedicineReminderParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.medicationReminder.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Call update medication reminder API
    /// Call update medication reminder API
    func callUpdateMedicationReminderAPi()
    {
        self.view.endEditing(true)
        
        let params : [String : Any]
        if modelAddMedicineProcess.from == modelAddMedicineProcess.to
        {
            params = [
                "drug_name" : modelAddMedicineProcess.drugName ,
                "for" : modelAddMedicineProcess.medicationFor ,
                "type" : modelAddMedicineProcess.medicationType ,
                "medicationreminder_id" : modelAddMedicineProcess.medicationID ,
                "frequency" : 0 ,
                "from_date_long" : modelAddMedicineProcess.from_date_long ,
                "to_date_long" : modelAddMedicineProcess.to_date_long ,
                "start_time_long" : modelAddMedicineProcess.start_time_long
                ] as [String : Any]
        }
        else
        {
            params = [
                "drug_name" : modelAddMedicineProcess.drugName ,
                "for" : modelAddMedicineProcess.medicationFor ,
                "type" : modelAddMedicineProcess.medicationType ,
                "medicationreminder_id" : modelAddMedicineProcess.medicationID ,
                "frequency" : modelAddMedicineProcess.frequency ,
                "from_date_long" : modelAddMedicineProcess.from_date_long ,
                "to_date_long" : modelAddMedicineProcess.to_date_long ,
                "start_time_long" : modelAddMedicineProcess.start_time_long
                ] as [String : Any]
        }
        
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.medicationReminder.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .put, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Validate Input TextField
    ///Validate Input TextField Method
    func validateTheInputField() -> Bool
    {
        if modelAddMedicineProcess.drugName.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter Drug Name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddMedicineProcess.medicationFor.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter medicine reason.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddMedicineProcess.medicationType.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter medicine type.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddMedicineProcess.from.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select from date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddMedicineProcess.to.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select to date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddMedicineProcess.startTime.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select start time.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    ///Delete Button Clicked Action Method
    @IBAction func deletButtonAction(_ sender: Any) {
        
        UtilityClass.showAlertWithTitle(title: "", message: "Do you really want to delete this record?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()], dismissHandler: { (buttonIndex) in
            
            if buttonIndex == 0 {
                self.callDeleteMedicationReminderAPi()
            }
        })
    }
    
    //MARK:- get delete meditation reminder params
    ///get delete meditation reminder params
    func getDeleteMedicineReminderParametersdata () -> [String : String]
    {
        let dictDeleteMedicuneReminder = [
            "medicationreminder_id" : modelAddMedicineProcess.medicationID
            ] as [String : String]
        
        return dictDeleteMedicuneReminder
    }
    
    //MARK: - Call delete medication reminder API
    ///Call delete medication reminder API
    func callDeleteMedicationReminderAPi()
    {
        self.view.endEditing(true)
        
        let params = getDeleteMedicineReminderParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.medicationReminder.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension AddMedicineRecordViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : AddMedicineTableViewCell!
        
        var cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
        
        switch indexPath.row {
            
        case 0:
            cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
            
        case 1:
            cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
            
        case 2:
            cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
            
        case 3:
            cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
            
        case 4:
            cellIdentifier = kCellIdentifier_Add_Medicine_double_table_view_cell
            
        default:
            cellIdentifier = kCellIdentifier_Add_Medicine_single_table_view_cell
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! AddMedicineTableViewCell
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        cell.updateCell()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (80 * scaleFactorX)
    }
}

///AddMedicineTableViewCellDelegate Method Extension
extension AddMedicineRecordViewController : AddMedicineTableViewCellDelegate
{
    
    /// Initially Update Cell Data On Input Value
    ///
    /// - Parameters:
    ///   - cell: AddMedicineTableViewCell Cell Value
    ///   - inputValue: Input TextField Values
    func addMedicineRecordUpdateFirstData(_ cell: AddMedicineTableViewCell, inputValue: String) {
        
        switch cell.cellType {
            
        case .fromTo:
            if !modelAddMedicineProcess.to.isEmptyString() {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM, yyyy"
                dateFormatter.timeZone = NSTimeZone.system
                
                let toDate = dateFormatter.date(from: modelAddMedicineProcess.to)
                print("todate = \(String(describing: toDate))")
                
                let fromDate = dateFormatter.date(from: inputValue)
                
                let cal = Calendar (identifier: .gregorian)
                
                let comparisonResult = cal.compare(fromDate!, to: toDate!, toGranularity: .day)
                var datesame = 0
                
                switch comparisonResult {
                case .orderedAscending:
                    //print("orderedAscending")
                    datesame = 0
                case .orderedSame:
                    //print("orderedSame")
                    datesame = 0
                case .orderedDescending:
                    //print("orderedDescending")
                    datesame = 1
                }

                if datesame == 1 {
                    
                    UtilityClass.showAlertWithTitle(title:App_Name, message: "From date can't be greater than To date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    cell.activeTextField?.text = ""
                    
                } else {
                    
                    modelAddMedicineProcess.from = inputValue
                    modelAddMedicineProcess.from_date_long = UtilityClass.getTimeStampFromDate(dateString: inputValue)
                }
            } else {
                
                modelAddMedicineProcess.from = inputValue
                modelAddMedicineProcess.from_date_long = UtilityClass.getTimeStampFromDate(dateString: inputValue)
            }
            
        default:
            modelAddMedicineProcess.frequency = 0
        }
    }
    
    
    /// AddMedicine Record Update Second Data Value
    ///
    /// - Parameters:
    ///   - cell: AddMedicineTableViewCell
    ///   - inputValue: input field value
    func addMedicineRecordUpdateSecondData(_ cell: AddMedicineTableViewCell, inputValue: String) {
        
        switch cell.cellType {
            
        case .fromTo:
            if !modelAddMedicineProcess.from.isEmptyString() {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM, yyyy"
                
                dateFormatter.timeZone = NSTimeZone.system
                
                let toDate = dateFormatter.date(from: modelAddMedicineProcess.from)
                print("todate = \(String(describing: toDate))")
                
                let fromDate = dateFormatter.date(from: inputValue)
                
                let cal = Calendar (identifier: .gregorian)
                
                let comparisonResult = cal.compare(fromDate!, to: toDate!, toGranularity: .day)
                var datesame = 0
                
                switch comparisonResult {
                case .orderedAscending:
                    //print("orderedAscending")
                    datesame = 1
                case .orderedSame:
                    //print("orderedSame")
                    datesame = 0
                case .orderedDescending:
                    //print("orderedDescending")
                    datesame = 0
                }
                
                if datesame == 1 {
                    
                    UtilityClass.showAlertWithTitle(title:App_Name, message: "To date can't be less than From date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    cell.activeTextField?.text = ""
                    
                } else {
                    
                    modelAddMedicineProcess.to = inputValue
                    modelAddMedicineProcess.to_date_long = UtilityClass.getTimeStampFromDate(dateString: inputValue)
                }
            } else {
                
                modelAddMedicineProcess.to = inputValue
                modelAddMedicineProcess.to_date_long = UtilityClass.getTimeStampFromDate(dateString: inputValue)
            }
            
        default:
            modelAddMedicineProcess.unit = inputValue
        }
    }
    
    ///AddMedicine Record Update Start Time Fill Data
    func addMedicineRecordUpdateStartTimeData(_ cell: AddMedicineTableViewCell, inputValue: String) {
        
        if !modelAddMedicineProcess.from.isEmptyString() {
            
            modelAddMedicineProcess.startTime = inputValue
            modelAddMedicineProcess.start_time_long = UtilityClass.getTimeStampFromTime(dateString: "\(modelAddMedicineProcess.from) \(inputValue)")
            
        } else
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select From date first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            cell.activeTextField?.text = ""
        }
    }
    
    
    /// Add Medicine Record Update Medicine Type Data
    ///
    /// - Parameters:
    ///   - cell: AddMedicinTableView Cell or Record Which We Need to Update
    ///   - inputValue: InputValue or Updated Value
    func addMedicineRecordUpdateMedicineTypeData(_ cell: AddMedicineTableViewCell, inputValue: String) {
        
        switch cell.cellType {
            
        case .repeatation:
            modelAddMedicineProcess.repeatation = inputValue
            let inputValue1 = inputValue.replacingOccurrences(of: " Hr", with: "")
            modelAddMedicineProcess.frequency = Int(inputValue1)!
            
        default:
            modelAddMedicineProcess.medicationType = inputValue
        }
    }
    

    /// Add Medicine Record Update Medicine Type Data
    ///
    /// - Parameters:
    ///   - cell: AddMedicinTableView Cell or Record Which We Need to Update
    ///   - inputValue: InputValue or Updated Value
    func addMedicineRecordUpdateData(_ cell: AddMedicineTableViewCell, updatedInputFieldText inputValue: String) {
        
        switch cell.cellType {
        
        case .drugName:
            modelAddMedicineProcess.drugName = inputValue
            
        case .medicineFor:
            modelAddMedicineProcess.medicationFor = inputValue
            
        default:
             modelAddMedicineProcess.medicationFor = inputValue
        }
    }
}

