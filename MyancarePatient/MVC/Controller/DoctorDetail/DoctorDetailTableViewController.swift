//
//  DoctorDetailTableViewController.swift
//  MyancarePatient
//
//  Created by iOS on 30/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
///Doctor Detail TableView Controller
class DoctorDetailTableViewController: UIViewController {

    ///Working Address Model Data Array
    var workingAddressData = [WorkingAddressModel]()
    
    ///UITableView Refrence - Doctor Detail Table
    @IBOutlet weak var doctorDetailTableView: UITableView!
    ///Variable - Selected Segement Index
    var selectedSegmentIndex = 0
    
    //Mark:- Properties
    ///Doctor Model Data Refrence
    var doctorModelData = DoctorModel()
    
    ///Review Model Data Array Variable
    var reviewModelData = [ReviewModel]()
    
    ///Bool Variable - is Pagiang
    var isPaging = true
    ///String Variable - Doctor ID
    var doctor_id = ""
    
    //MARK: - viewDidLoad
    ///UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        if selectedSegmentIndex == 2 {
            isPaging = true
            reviewModelData.removeAll()
            getDoctorReviewerListFromAPI()
        } else if selectedSegmentIndex == 1 {
            isPaging = true
            workingAddressData.removeAll()
            getWorkingAddressListFromAPI()
        } else {
            getDoctorProfileDataFromAPI()
        }
    }

    //MARK: - Deinit
    deinit {
        print("DoctorDetailTableViewController deinit")
    }
    
    //MARK: - setUpTableView
    ///SetupTable View Method
    func setUpTableView() {
        
        doctorDetailTableView.delegate = self
        doctorDetailTableView.dataSource = self
    }
    
    //Mark :- get doctor profile data from api
    ///get doctor profile data from api
    func getDoctorProfileDataFromAPI()
    {
        let urlToHit = EndPoints.get_profile(doctor_id).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let appointmentDtaArr1 = responseDictionary["data"] as? [String : Any]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateDoctorModel(dict: appointmentDtaArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    ///Update Doctor Model Method
    func updateDoctorModel (dict : [String : Any]) {
        
        let model = DoctorModel () // Model creation
        model.updateModel(usingDictionary: dict) // Updating model
        
        doctorModelData = model
        
        doctorDetailTableView.reloadData()
    }
    
    //Mark :- get medical record listing from api
    ///get medical record listing from api
    func getWorkingAddressListFromAPI()
    {
        let urlToHit = EndPoints.getDoctorWorkingAddress(doctor_id, workingAddressData.count).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let workingAddressArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard workingAddressArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: workingAddressArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    ///Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = WorkingAddressModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            workingAddressData.append(model) // Adding model to array
        }
        
        doctorDetailTableView.reloadData()
    }
    
    //Mark :- get doctor reviewer listing from api
    ///get doctor reviewer listing from api
    func getDoctorReviewerListFromAPI()
    {
        let urlToHit = EndPoints.doctorReviewGet(doctor_id, reviewModelData.count).path
        print("review list url = ", urlToHit)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let appointmentDtaArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateReviewModelArray(usingArray: appointmentDtaArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating review Model Array
    ///Updating review Model Array
    func updateReviewModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ReviewModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            reviewModelData.append(model) // Adding model to array
        }
        
        doctorDetailTableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension DoctorDetailTableViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedSegmentIndex == 0 {
            return 3
        } else if selectedSegmentIndex == 1 {
            return workingAddressData.count
        } else {
            return reviewModelData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedSegmentIndex == 0 || selectedSegmentIndex == 1
        {
            let cell: DoctorDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_DoctorDetail_table_view_cell) as! DoctorDetailTableViewCell
            
            if selectedSegmentIndex == 0
            {
                if indexPath.row == 0
                {
                    cell.address1label.text = "About".localized()
                    cell.address2Label.text = "\(doctorModelData.biography ?? "")"
                }
                else if indexPath.row == 1
                {
                    let arrSpec : NSArray = doctorModelData.specializations! as NSArray
                    var specilaization = ""
                    
                    for i in 0 ..< arrSpec.count
                    {
                        let dict = arrSpec.object(at: i) as! NSDictionary
                        specilaization = specilaization.appending(dict.object(forKey: "name") as! String)
                        
                        if i != arrSpec.count - 1
                        {
                            specilaization = specilaization.appending(", ")
                        }
                    }
                    
                    var spec = "Specialization".localized() as NSString
                    spec = spec.appending(": \(specilaization)") as NSString
                    cell.address1label.text = spec as String
                    
                    var address = "\(doctorModelData.experience ?? "")" as NSString
                    address = address.appending(" ") as NSString
                    address = address.appending("Years of experience".localized()) as NSString
                    cell.address2Label.text = address as String
                }
                else
                {
                    cell.address1label.text = "Qualification".localized()
                    cell.address2Label.text = "\(doctorModelData.degrees ?? "")"
                }
            }
            else
            {
                var spec = "Working Address".localized() as NSString
                spec = spec.appending(" #") as NSString
                spec = spec.appending("\(indexPath.row + 1)") as NSString
                
                cell.address1label.text = spec as String
                
                cell.address2Label.text = workingAddressData[indexPath.row].name
                
                if isPaging && indexPath.row == workingAddressData.count - 1
                {
                    getWorkingAddressListFromAPI()
                }
            }
            
            return cell
        }
        else
        {
            let cell: ReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell") as! ReviewTableViewCell
            
            cell.userNameLabel.text = reviewModelData[indexPath.row].reviewUserName
            
            cell.userCommentLabel.text = reviewModelData[indexPath.row].reviewComment
            
            cell.userImage.setShowActivityIndicator(true)
            cell.userImage.setIndicatorStyle(.gray)
            
            cell.userImage.sd_setImage(with: URL.init(string: reviewModelData[indexPath.row].reviewUserImage!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
            
            cell.dateLabel.text = reviewModelData[indexPath.row].dateString
            
            if reviewModelData[indexPath.row].rating == 1 {
                
                cell.star1Image.image = #imageLiteral(resourceName: "star")
                cell.star2Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star3Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star4Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star5Image.image = #imageLiteral(resourceName: "empty-star")
                
            } else if reviewModelData[indexPath.row].rating == 2 {
                
                cell.star1Image.image = #imageLiteral(resourceName: "star")
                cell.star2Image.image = #imageLiteral(resourceName: "star")
                cell.star3Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star4Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star5Image.image = #imageLiteral(resourceName: "empty-star")
                
            } else if reviewModelData[indexPath.row].rating == 3 {
                
                cell.star1Image.image = #imageLiteral(resourceName: "star")
                cell.star2Image.image = #imageLiteral(resourceName: "star")
                cell.star3Image.image = #imageLiteral(resourceName: "star")
                cell.star4Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star5Image.image = #imageLiteral(resourceName: "empty-star")
                
            } else if reviewModelData[indexPath.row].rating == 4 {
                
                cell.star1Image.image = #imageLiteral(resourceName: "star")
                cell.star2Image.image = #imageLiteral(resourceName: "star")
                cell.star3Image.image = #imageLiteral(resourceName: "star")
                cell.star4Image.image = #imageLiteral(resourceName: "star")
                cell.star5Image.image = #imageLiteral(resourceName: "empty-star")
                
            } else if reviewModelData[indexPath.row].rating == 5 {
                
                cell.star1Image.image = #imageLiteral(resourceName: "star")
                cell.star2Image.image = #imageLiteral(resourceName: "star")
                cell.star3Image.image = #imageLiteral(resourceName: "star")
                cell.star4Image.image = #imageLiteral(resourceName: "star")
                cell.star5Image.image = #imageLiteral(resourceName: "star")
                
            } else {
                
                cell.star1Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star2Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star3Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star4Image.image = #imageLiteral(resourceName: "empty-star")
                cell.star5Image.image = #imageLiteral(resourceName: "empty-star")
            }
            
            if isPaging && indexPath.row == reviewModelData.count - 1 {
                getDoctorReviewerListFromAPI()
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedSegmentIndex == 2 {
            return 110 * scaleFactorX
        } else if selectedSegmentIndex == 1 {
            return 94 * scaleFactorX
        } else {
            return 94 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedSegmentIndex == 2 {
            return UITableViewAutomaticDimension
        } else if selectedSegmentIndex == 1 {
            return UITableViewAutomaticDimension
        } else {
            return UITableViewAutomaticDimension
        }
    }
}
