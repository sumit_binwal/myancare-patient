//
//  ChangeTextSizeViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 15/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class ChangeTextSizeViewController: UIViewController {

    //Mark :- IBOutlets
    
    /// UILabel Refrence - Static Infos
    @IBOutlet var labelStaticInfo: UILabel!
    /// UILabel Refrence - Change Label Value When Drag Slider
    @IBOutlet var labelDragText: UILabel!
    /// UISlider Refrence - Slider For Text Size Change
    @IBOutlet var sliderAdustSize: UISlider!
    /// UILabel Refrence - Label Small A
    @IBOutlet var labelSmallA: UILabel!
    /// UILabel Refrence - Label Large B
    @IBOutlet var labelLargeA: UILabel!
    
    /// UILabel Refrence - Label 1
    @IBOutlet weak var label1: UILabel!
    /// UILabel Refrence - Label 2
    @IBOutlet weak var label2: UILabel!
    
    //MARK: - viewDidLoad
    
    /// UIView Controller LIfe Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let intChangedValue :Int = (userDefaults.value(forKey: "customFontSize") as! Int)
            
            if (intChangedValue == -1)
            {
                sliderAdustSize.value = 0.33
            }
            else if(intChangedValue == 1)
            {
                sliderAdustSize.value = 0.67
            }
            else if(intChangedValue == -2)
            {
                sliderAdustSize.value = 0.174
            }
            else if(intChangedValue == 2)
            {
                sliderAdustSize.value = 0.839
            }
            else if(intChangedValue == -3)
            {
                sliderAdustSize.value = 0.0
            }
            else if(intChangedValue == 3)
            {
                sliderAdustSize.value = 1.0
            }
            else
            {
                sliderAdustSize.value = 0.5
            }
        }
        else
        {
            sliderAdustSize.value = 0.5
        }
        
        let headerSTring1 = "Apps that support Dynamic Type \n will adjust to your preferred \n reading size below.".localized()
        labelStaticInfo.text = headerSTring1
        
        let headerSTring2 = "Drag the slider below".localized()
        labelDragText.text = headerSTring2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }

    //MARK:- setUpNavigationBar
    
    /// SetupNavigation bar Method
    func setUpNavigationBar() {
        
        self.title = "Change Text Size".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:-
    /// Back Button Clicked Event Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Deinit
    /// ViewContoller Deinit Method
    deinit {
        print("ChangeTextSizeViewController deinit")
    }
    
    @IBAction func sliderValueChangedMethod(_ sender: UISlider) {
        
    }
    
    /// Slider View End Editing Dragging Method
    /// Slider Value Change
    ///
    /// - Parameter sender: UiSlider Refrence
    @IBAction func sliderValueEndEditingMethod(_ sender: UISlider)
    {
        var intChangedValue:NSInteger =  NSInteger(roundf((sender.value-0.5)*10))
        let _:Float = Float(labelStaticInfo.font.pointSize)
        print("slider value is===== \(intChangedValue) and font size")
    
        if (intChangedValue == -1 || intChangedValue == -2)
        {
            intChangedValue = -1
            sliderAdustSize.value = 0.33
        }
        else if(intChangedValue == 1 || intChangedValue == 2)
        {
            intChangedValue = 1
            sliderAdustSize.value = 0.67
        }
        else if(intChangedValue == -3)
        {
            intChangedValue = -2
            sliderAdustSize.value = 0.174
        }
        else if(intChangedValue == 3)
        {
            intChangedValue = 2
            sliderAdustSize.value = 0.839
        }
        else if(intChangedValue == -4 || intChangedValue == -5)
        {
            intChangedValue = -3
            sliderAdustSize.value = 0.0
        }
        else if(intChangedValue == 4 || intChangedValue == 5)
        {
            intChangedValue = 3
            sliderAdustSize.value = 1.0
        }
        else
        {
            intChangedValue = 0
            sliderAdustSize.value = 0.5
        }
        
        print("slider value is===== \(intChangedValue) and font size")
        
        labelStaticInfo.font = UIFont(name: labelStaticInfo.font.fontName, size: (17.0 + CGFloat(intChangedValue)))
        
        userDefaults.set(intChangedValue, forKey: "customFontSize")
        userDefaults.synchronize()
    }
}
