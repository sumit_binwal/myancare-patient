//
//  DoctorFilterViewController.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

//MARK: -
/// Doctor Filter Delegate Methods
protocol DoctorFilterDelegate: class
{
    func DoctorFilterData(_selectedCategory dataValue: NSMutableArray)
}

/// DoctorFilter ViewController
class DoctorFilterViewController: UIViewController {

    
    /// Custome Varibale - ExpandadSection Header Number
    var expandedSectionHeaderNumber: Int = -1
    /// Custome Varibale - Expanded Section Header Footer View
    var expandedSectionHeader: UITableViewHeaderFooterView!
    
    
    /// Variable - Header Section Tag
    let kHeaderSectionTag: Int = 6900;
    /// Variable - Header Content View Tag
    let kHeaderContentViewTag: Int = 7900;
    
    
    /// ArrayCategory Article Model Data
    var arrayCategory = [ArticleModel]()
    
    /// Doctor Filter Delegate
    weak var delegate: DoctorFilterDelegate?
    /// UIButton Refrence - Apply Button
    @IBOutlet weak var applyButton: UIButton!
    
    /// NSMutableArray Selected Cateogry
    var selectedCategoryIdArr : NSMutableArray = []
    
    /// Variable - Selected Index Value
    var selectedIndex = 0
    
    
    /// UITableView Category Lists
    @IBOutlet weak var tableViewCategoryList: UITableView!
    
    //MARK: - viewDidLoad
    /// View Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        applyButton.setTitle("Apply".localized(), for: .normal)
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.setUpNavigationBar()
        
        if modelDoctorFilterProcess.selectedCategoryId != ""
        {
            if (modelDoctorFilterProcess.selectedCategoryId?.contains(","))!
            {
                selectedCategoryIdArr = modelDoctorFilterProcess.selectedCategoryId?.components (separatedBy: ",") as! NSMutableArray
            }
            else
            {
                selectedCategoryIdArr.add (modelDoctorFilterProcess.selectedCategoryId!)
            }
        }
        
        getCategoryListFromAPI()
        getTownListFromServer()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("DoctorFilterViewController deinit")
    }
    
    //MARK: - setUpNavigationBAr
    
    /// Setup Navigation Bar Method
    func setUpNavigationBar() {
        
        self.title = "Filter".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK: - Navigation bar Back Button Action
    /// Back Button Clicked Event Methods
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - on Apply Button Action
    /// UIButton Apply Clicked Event Action
    @IBAction func btnApplyPressed(_ sender: Any) {
        
        modelDoctorFilterProcess.selectedCategoryId = UtilityClass.getCommaSepratedStringFromArrayDoctorFilter(completeArray: selectedCategoryIdArr)
        
        if selectedIndex == 0
        {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorAll"), object: nil, userInfo: nil)
        }
        else if selectedIndex == 1
        {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorRecent"), object: nil, userInfo: nil)
        }
        else
        {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applyFilterDoctorFavourites"), object: nil, userInfo: nil)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - get Town List From API
    /// getTown List Data From Server
    func getTownListFromServer()
    {
        let urlToHit =  EndPoints.districtTown.path
        //print("getTownListFromServer url = \(urlToHit)")
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler: {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                let isEqual = (replyType == "success")
                
                if isEqual
                {
                    // login...
                    guard responseDictionary["data"] != nil else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    let userDataDict = (responseDictionary["data"] as! [[String : Any]] )
                    
                    locationGlobleArr = userDataDict
                }
            }
        })
    }
    
    //Mark :- get category list API Calling
    /// getTown List Data From WebServer
    func getCategoryListFromAPI()
    {
        let urlToHit = EndPoints.specialization.path
        print("getCategoryListFromAPI url = \(urlToHit)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    self.updateCateModelArray(usingArray: dataDict!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - update Category Model Arr
    
    /// Update Category Model Array
    ///
    /// - Parameter array: ArrayCategory Data
    func updateCateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayCategory.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateCateModel(usingDictionary: dict) // Updating model
            arrayCategory.append(model) // Adding model to array
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension DoctorFilterViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if self.expandedSectionHeaderNumber == section
        {
            if section == 0
            {
                return  arrayCategory.count
            }
            else
            {
                return 3
            }
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            
        header.textLabel?.textColor = UIColor.white
            
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section)
        {
            viewWithTag.removeFromSuperview()
        }
            
        if let viewWithTag = self.view.viewWithTag(kHeaderContentViewTag + section)
        {
            viewWithTag.removeFromSuperview()
        }
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryHeaderTableViewCell") as! CategoryHeaderTableViewCell?
        
        if section == 0
        {
            cell?.headerImage.image = #imageLiteral(resourceName: "category-icon")
            cell?.headerLabel.text = "Category".localized()
        }
        else
        {
            cell?.headerImage.image = #imageLiteral(resourceName: "location-icon")
            cell?.headerLabel.text = "Location".localized()
        }
            
        cell?.contentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: (cell?.contentView.frame.size.height)!)
            
        cell?.contentView.tag = kHeaderContentViewTag + section
            
        header.addSubview((cell?.contentView)!)
            
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell: CategoryDoctorTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoryDoctorTableViewCell") as! CategoryDoctorTableViewCell
            
            cell.labelCategoryListName.text = arrayCategory[indexPath.row].categoryName
            
            let strSelected = arrayCategory[indexPath.row].categoryId
            
            if (selectedCategoryIdArr.contains(strSelected!))
            {
                cell.buttonSelection.isSelected = true
                cell.buttonSelection.setImage(UIImage.init(named: "checked"), for: .normal)
            }
            else
            {
                cell.buttonSelection.isSelected = false
                cell.buttonSelection.setImage(UIImage.init(named: "uncheck"), for: .normal)
            }
            
            cell.buttonSelection.tag = indexPath.row
            
            cell.buttonSelection.isUserInteractionEnabled = false
            
            return cell
        }
        else
        {
            var cell : CategoryLocationTableViewCell!
            
            let cellIdentifier = kCellIdentifier_CategoryLocationTableViewCell
            
            var cellType = CategoryLocationDataCellType.state
            
            switch indexPath.row {
                
            case 0:
                cellType = .state
                
            case 1:
                cellType = .district
                
            case 2:
                cellType = .town
                
            default:
                break
            }
            
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CategoryLocationTableViewCell
            
            cell.tag = indexPath.row
            
            cell.cellType = cellType
            
            cell.delegate = self
            
            cell.updateCell()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 60 * scaleFactorX
        }
        else
        {
            return 64 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            let cell: CategoryDoctorTableViewCell = tableView.cellForRow(at: indexPath) as! CategoryDoctorTableViewCell
            
            if cell.buttonSelection.isSelected == true
            {
                cell.buttonSelection.isSelected = false
                cell.buttonSelection.setImage(UIImage.init(named: "uncheck"), for: .normal)
                
                let strSelected = arrayCategory[indexPath.row].categoryId
                
                if (selectedCategoryIdArr.contains(strSelected!))
                {
                    selectedCategoryIdArr.remove(strSelected!)
                }
            }
            else
            {
                let strSelectedID = arrayCategory[indexPath.row].categoryId
                
                selectedCategoryIdArr.add(strSelectedID!)
                
                cell.buttonSelection.isSelected = true
                cell.buttonSelection.setImage(UIImage.init(named: "checked"), for: .normal)
            }
        }
    }
    
    // MARK: - Expand / Collapse Methods
    
    /// Expand / Collapse Menus Methods
    ///
    /// - Parameter sender: UITapGestureRecognizer
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer)
    {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section = headerView.tag
        
        if (self.expandedSectionHeaderNumber == -1)
        {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section)
        }
        else
        {
            if (self.expandedSectionHeaderNumber == section)
            {
                tableViewCollapeSection(section)
            }
            else
            {
                tableViewCollapeSection(self.expandedSectionHeaderNumber)
                tableViewExpandSection(section)
            }
        }
    }
    
    
    /// Table View Collape Section
    ///
    /// - Parameter section: Section for Title
    func tableViewCollapeSection(_ section: Int)
    {
        self.expandedSectionHeaderNumber = -1;
        tableViewCategoryList.reloadData()
    }
    
    
    /// Table View Expand Section
    ///
    /// - Parameter section:
    func tableViewExpandSection(_ section: Int)
    {
        if section == 0
        {
            if arrayCategory.count != 0
            {
                self.expandedSectionHeaderNumber = section
                tableViewCategoryList.reloadData()
            }
            else
            {
                self.expandedSectionHeaderNumber = -1;
                tableViewCategoryList.reloadData()
            }
        }
        else
        {
            self.expandedSectionHeaderNumber = section
            tableViewCategoryList.reloadData()
        }
    }
}

//MARK: - Extension -> HealthDataCellDelegate
///Category Location Data Cell Delegate Methods
extension DoctorFilterViewController: CategoryLocationDataCellDelegate
{
    
    /// Categorry Location Data Cell - Update Input Text Values
    ///
    /// - Parameters:
    ///   - cell: CategoryLocationTableViewCell
    ///   - inputValue: Inpute Values According To Cell Type
    func categoryLocataionDataCell(_ cell: CategoryLocationTableViewCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
        
        case .state:
            print("state : "+inputValue)
            
            modelDoctorFilterProcess.state = inputValue
            
            modelDoctorFilterProcess.district = ""
            modelDoctorFilterProcess.d_pcode = ""
            
            modelDoctorFilterProcess.town = ""
            modelDoctorFilterProcess.town_pcode = ""
            
            modelDoctorFilterProcess.sr_pcode = UtilityClass.getStatePCode(inputValue)
            
            let indexPath0 = IndexPath(item: 0, section: 1)
            let indexPath1 = IndexPath(item: 1, section: 1)
            let indexPath2 = IndexPath(item: 2, section: 1)
            
            tableViewCategoryList.reloadRows(at: [indexPath0, indexPath1, indexPath2], with: .automatic)
            
        case .district:
            print("district : "+inputValue)
            
            modelDoctorFilterProcess.district = inputValue
            modelDoctorFilterProcess.town = ""
            modelDoctorFilterProcess.town_pcode = ""
            
            modelDoctorFilterProcess.d_pcode = UtilityClass.getDistrictPCode(inputValue)
            
            let indexPath1 = IndexPath(item: 1, section: 1)
            let indexPath2 = IndexPath(item: 2, section: 1)
            tableViewCategoryList.reloadRows(at: [indexPath1, indexPath2], with: .automatic)
            
        case .town:
            print("town : "+inputValue)
            
            modelDoctorFilterProcess.town = inputValue
            modelDoctorFilterProcess.townID = UtilityClass.getTownID(inputValue)
            
            modelDoctorFilterProcess.town_pcode = UtilityClass.getTownPCode(inputValue)
            
            let strLatitude = UtilityClass.getLatitudeOfTown(inputValue)
            let strLongitude = UtilityClass.getLongitudeOfTown(inputValue)
            
            let locationArry:NSMutableArray = [strLongitude, strLatitude]
            
            modelDoctorFilterProcess.locationArry = locationArry.mutableCopy() as? NSMutableArray
            
            let indexPath2 = IndexPath(item: 2, section: 1)
            tableViewCategoryList.reloadRows(at: [indexPath2], with: .automatic)
            
        default:
            break
        }
    }
}
