//
//  InvioceViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

class InvioceViewController: UIViewController {

    ///UILabel Refrence - Balance Text
    @IBOutlet weak var balanceLabel: UILabel!
    ///UITableView Refrence - Shows Invoices Data
    @IBOutlet weak var invioceTableView: UITableView!
    ///UIButton Refrecne - Confirm Button
    @IBOutlet weak var confirmButton: UIButton!
    
    //MARK: - viewDidLoad
    ///UIViewController Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }

    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        setConfirmTitle()
    }
    
    //MARK: - Deinit
    deinit {
        print("InvioceViewController deinit")
    }
    
    ///Set Confirm Title Method
    func setConfirmTitle() {
        
        if modelAppointmentData.isReschedule {
            
            self.balanceLabel.text = "Sufficient Balance"
            self.confirmButton.setTitle("Confirm".localized(), for: .normal)
            
        } else {
            
            let wallet_balance = UtilityClass.getUserInfoData()["wallet_balance"] as! Double
            
            var appointmentPrice = modelAppointmentData.total_consultant_fees
            
            appointmentPrice = appointmentPrice.replacingOccurrences(of: " ", with: "")
            appointmentPrice = appointmentPrice.replacingOccurrences(of: "Kyat".localized(), with: "")
            
            let appointmentFees = Double(appointmentPrice)!
            
            if wallet_balance >= appointmentFees {
                self.balanceLabel.text = "Sufficient Balance"
                self.confirmButton.setTitle("Confirm".localized(), for: .normal)
            } else {
                self.balanceLabel.text = "In-sufficient Balance"
                self.confirmButton.setTitle("Add Balance".localized(), for: .normal)
            }
        }
    }
    
    //MARK: - setUpTableView
    ///Setup table View Method
    func setUpTableView() {
        
        invioceTableView.delegate = self
        invioceTableView.dataSource = self
    }
    
    // MARK: - setUpNavigationBar
    ///SetupNavigation Bar Method
    func setUpNavigationBar() {
        
        self.title = "Service Invoice".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    // MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }


    //MARK:- on confirm button action
    ///on confirm button action
    @IBAction func onConfirmButtonAction(_ sender: Any) {
        
        if modelAppointmentData.isReschedule {
            
            let ConfirmAppointmentVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateViewController(withIdentifier: "ConfirmAppointmentVC") as! ConfirmAppointmentViewController
            
            ConfirmAppointmentVC.backgroungImageScreen = UtilityClass.screenShotMethod()
            
            self.navigationController?.pushViewController(ConfirmAppointmentVC, animated: false)
            
        } else {
            
            if confirmButton.title(for: .normal) == "Confirm".localized() {
                
                if modelAppointmentData.appointmentType == "chat" {
                    self.callBookAppointmentAPi()
                } else {
                    
                    let ConfirmAppointmentVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateViewController(withIdentifier: "ConfirmAppointmentVC") as! ConfirmAppointmentViewController
                    
                    ConfirmAppointmentVC.backgroungImageScreen = UtilityClass.screenShotMethod()
                    
                    self.navigationController?.pushViewController(ConfirmAppointmentVC, animated: false)
                }
                
            } else {
                
                //UtilityClass.showAlertWithTitle(title: App_Name, message: "Your wallet balance is not sufficient. Please top up youw wallet first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                    let myWalletVC = UIStoryboard.getWalletStoryBoard().instantiateInitialViewController() as! WalletViewController
                    myWalletVC.isFromConfirmBooking = true
                    self.navigationController?.pushViewController(myWalletVC, animated: true)
                //})
            }
        }
    }
    
    //MARK: - Call Book Appointment API
    ///Call Book Appointment API
    func callBookAppointmentAPi()
    {
        self.view.endEditing(true)
        
        let params = modelAppointmentData.getAppointmentChatDictionary()
        
        print("parameters = ", params)
        
        let urlToHit = EndPoints.chat_confirm.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let bookingMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: bookingMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        var wallet_balance = UtilityClass.getUserInfoData()["wallet_balance"] as! Double
                        
                        var appointmentPrice = modelAppointmentData.total_consultant_fees
                        
                        appointmentPrice = appointmentPrice.replacingOccurrences(of: " Kyat", with: "")
                        
                        let appointmentFees = Double(appointmentPrice)!
                        
                        wallet_balance = wallet_balance - appointmentFees
                        UtilityClass.setWalletBalanceInUserInfo(walletBalance: wallet_balance)
                        self.navigationController?.popToRootViewController(animated: false)
                    })
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension InvioceViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: InvioceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InvioceTableViewCell") as! InvioceTableViewCell
        
        if indexPath.row == 0
        {
            cell.nameLabel.text = "Date of issue:".localized()
            cell.descriptionLabel.text = modelAppointmentData.issueDate
        }
        else if indexPath.row == 1
        {
            cell.nameLabel.text = "Scheduled Date/Time:".localized()
            cell.descriptionLabel.text = modelAppointmentData.scheduleDate
        }
        else if indexPath.row == 2
        {
            cell.nameLabel.text = "Doctor Name:".localized()
            cell.descriptionLabel.text = modelAppointmentData.doctorName
        }
        else if indexPath.row == 3
        {
            cell.nameLabel.text = "Service:".localized()
            cell.descriptionLabel.text = modelAppointmentData.appointmentType.capitalized
        }
        else if indexPath.row == 4
        {
            cell.nameLabel.text = "Service Unit:".localized()
            if modelAppointmentData.appointmentType == "chat"
            {
                cell.descriptionLabel.text = "7 Days (maximum)".localized()
            }
            else
            {
                cell.descriptionLabel.text = "15 min (maximum)".localized()
            }
        }
        else if indexPath.row == 5
        {
            cell.nameLabel.text = "Consultation Fees:".localized()
            cell.descriptionLabel.text = modelAppointmentData.serviceFees
        }
        else if indexPath.row == 6
        {
            cell.nameLabel.text = "Service Fees:".localized()
            cell.descriptionLabel.text = modelAppointmentData.service_tax + " " + "Kyat".localized()
        }
        else
        {
            cell.nameLabel.text = "Total Fees:".localized()
            cell.descriptionLabel.text = modelAppointmentData.total_consultant_fees + " " + "Kyat".localized()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}

