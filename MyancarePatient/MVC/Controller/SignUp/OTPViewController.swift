//
//  OTPViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 04/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import PKHUD

class OTPViewController: UIViewController {
    
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var screenHeaderLabel1: UILabel!
    
    @IBOutlet weak var fourthCodeTextField: UITextField!
    @IBOutlet weak var thirdCodeTextField: UITextField!
    @IBOutlet weak var secondCodeTextField: UITextField!
    @IBOutlet weak var firstCodeTextField: UITextField!
    
    @IBOutlet weak var resendCodeBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var changeNumberBtn: UIButton!
    
    var isFromForgotVC = false
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        setUpView()
    }
    
    //MARK: - Deinit
    deinit {
        print("OTPViewController deinit")
    }
    
    //MARK: - setUpView
    func setUpView() {
        
        var personName = ""
        
        if isFromForgotVC {
            personName = ""
            changeNumberBtn.isHidden = true
            backBtn.isHidden = false
        } else {
            let nickName = UtilityClass.getPersonNickName()
            
            if nickName == "" || nickName == "None".localized() {
                personName = UtilityClass.getPersonName()
            } else {
                personName = nickName
            }
            
            changeNumberBtn.isHidden = false
            backBtn.isHidden = true
        }
        
        let headerString = "Almost done!".localized() + " " + personName
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        attributedString.addAttribute(.font, value: UIFont.createPoppinsSemiBoldFont(withSize: 18.0), range: NSRange(location: "Almost done!".localized().count + 1, length: personName.count))
        screenHeaderLabel.attributedText = attributedString
        
        let headerString1 = "You should receive the \n verification code in your \n mobile/mail and please type \n the verification code here.".localized()
        let attributedString1 = NSMutableAttributedString(string: headerString1, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0 * scaleFactorX),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8 * scaleFactorX
            ])
        screenHeaderLabel1.attributedText = attributedString1
        
        resendCodeBtn.setTitle("Resend Code".localized(), for: .normal)
        changeNumberBtn.setTitle("Change Number".localized(), for: .normal)
        confirmBtn.setTitle("Confirm".localized(), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Resend Button Button Action
    @IBAction func onResendBtnAction(_ sender: UIButton)
    {
        callReSentOtpAPI(modelSignUpProcess.tempToken!)
    }
    
    //MARK: - Change Number Button Action
    @IBAction func onChangeNumberBtnAction(_ sender: UIButton)
    {
        //Redirect To Change Phone Number VC
        let changeNoVC = UIStoryboard.getChangePhoneNumberStoryBoard().instantiateInitialViewController() as! ChangePhoneNumberViewController
        self.present(changeNoVC, animated: true, completion: nil)
    }
    
    //MARK: - Confirm Button Action
    @IBAction func onConfirmButtonAction(_ sender: UIButton)
    {
        if validateUserInput()
        {
            let firstValue = firstCodeTextField.text
            let secondValue = secondCodeTextField.text
            let thirdValue = thirdCodeTextField.text
            let fourthValue = fourthCodeTextField.text
            
            modelSignUpProcess.enterdOTPValue = firstValue! + secondValue! + thirdValue! + fourthValue!
            print("value = \(String(describing: modelSignUpProcess.enterdOTPValue))")
            
            if isFromForgotVC {
                callConfirmOTPForgotAPi()
            } else {
                callConfirmOTPAPi()
            }
        }
    }
    
    // MARK: Validation
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(firstCodeTextField?.text?.isEmpty)!
        {
            isError = false
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter OTP Code.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.firstCodeTextField.becomeFirstResponder()
            })
        }
        else if(secondCodeTextField?.text?.isEmpty)!
        {
            isError = false
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter OTP Code.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.secondCodeTextField.becomeFirstResponder()
            })
        }
        else if(thirdCodeTextField?.text?.isEmpty)!
        {
            isError = false
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter OTP Code.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.thirdCodeTextField.becomeFirstResponder()
            })
        }
        else if(fourthCodeTextField?.text?.isEmpty)!
        {
            isError = false
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter OTP Code.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.fourthCodeTextField.becomeFirstResponder()
            })
        }
        
        return isError
    }
    
    //MARK: - Call Resent OTP API
    func callReSentOtpAPI(_ tempToken:String)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ["tmp_token":tempToken]
        
        let urlToHit =  EndPoints.resendOTP.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                let replyMsg = responseDictionary["msg"] as? String
                guard replyMsg != nil else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
        }
    }
    
    //MARK: - Call Confirm OTP API
    func callConfirmOTPAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getVerifyOTPDataDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.verifyOTP.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    let notificationStatus = dataDict!["notification_setting"] as! Bool
                    UtilityClass.saveNotificationStatus(userDict: notificationStatus)
                    
                    UtilityClass.saveUserInfoData(userDict: dataDict!)
                    
                    NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": dataDict!["id"]] as? [AnyHashable : Any])
                    
                    let congratesVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "congratesVC")
                    self.navigationController?.pushViewController(congratesVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Call Confirm OTP Forgot Password API
    func callConfirmOTPForgotAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getVerifyOTPForgotDataDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.verifyForgotOtp.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    modelSignUpProcess.tempToken = dataDict!["tmp_token"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message:"Otp verify successfully. Please reset password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                        let passwordVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "passwordVC") as! PasswordViewController
                        passwordVC.isFromForgot = self.isFromForgotVC
                        self.navigationController?.pushViewController(passwordVC, animated: true)
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

// MARK: - Extension -> UITextField -> UITextFieldDelegate
extension OTPViewController : UITextFieldDelegate
{
    @IBAction func textFieldDidChange(_ theTextField: UITextField)
    {
        if !(theTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!
        {
            if theTextField == firstCodeTextField && secondCodeTextField?.text?.count == 0
            {
                secondCodeTextField?.becomeFirstResponder()
            }
            else if theTextField == secondCodeTextField && thirdCodeTextField?.text?.count == 0
            {
                thirdCodeTextField?.becomeFirstResponder()
            }
            else if theTextField == thirdCodeTextField && fourthCodeTextField?.text?.count == 0
            {
                fourthCodeTextField?.becomeFirstResponder()
            }
            else if theTextField == fourthCodeTextField
            {
                fourthCodeTextField?.resignFirstResponder()
            }
            else
            {
                theTextField.resignFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
        {
            return false
        }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if (textField == fourthCodeTextField)
            {
                fourthCodeTextField?.text = ""
                thirdCodeTextField?.becomeFirstResponder()
            }
            else if (textField == thirdCodeTextField)
            {
                thirdCodeTextField?.text = ""
                secondCodeTextField?.becomeFirstResponder()
            }
            else if (textField == secondCodeTextField)
            {
                secondCodeTextField?.text = ""
                firstCodeTextField?.becomeFirstResponder()
            }
            else
            {
                firstCodeTextField?.text = ""
            }
            
            return false
        }
        
        return true
    }
}
