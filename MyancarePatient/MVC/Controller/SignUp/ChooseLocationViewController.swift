//
//  ChooseLocationViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 02/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import PKHUD

class ChooseLocationViewController: UIViewController {

    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var locationTableView: UITableView!
    var arrStateValue : NSMutableArray = []
    
    @IBOutlet var locationButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    //MARK: - View Lyf Cycle Methods
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
        setupTableView()
    }
    
    // MARK: - deinit
    deinit
    {
        print("ChooseLocationViewController Deinit")
        
        modelSignUpProcess.stateName = ""
        modelSignUpProcess.districtName = ""
        modelSignUpProcess.townName = ""
        modelSignUpProcess.districttownID = ""
    }
    
    // MARK: - setUpView
    func setUpView() {
        
        let headerString = "Please let me know where are \n you living now?".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        nextButton.setTitle("Next".localized(), for: .normal)
        locationButton.titleLabel?.font = UIFont.init(name: (locationButton.titleLabel?.font.fontName)!, size: (((locationButton.titleLabel?.font.pointSize)! * scaleFactorX)))
        locationButton.setTitle("Detect location automatically".localized(), for: .normal)
    }
    
    //MARK: - setupTableView
    func setupTableView()  {
        
        locationTableView.dataSource = self
        locationTableView.delegate = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }

    //MARK: - Validate Input TextField
    func validateTheInputField() -> Bool
    {
        if modelSignUpProcess.stateName == nil || modelSignUpProcess.stateName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select state first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.districtName == nil || modelSignUpProcess.districtName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select district first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.townName == nil || modelSignUpProcess.townName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select township first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - On Dectect Location Button Action
    @IBAction func onDetectLocationButtonAction(_ sender: UIButton)
    {
        _ = LocationManager.sharedInstance()
        
        if LocationManager.sharedInstance().isLocationAccessAllowed()
        {
            callNearestDistrictAPI()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "", message: "For Autodetect Location Please Allow Location On Settings.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK: - Next Button Click Action
    @IBAction func onNextButtonAction(_ sender: UIButton)
    {
        if validateTheInputField()
        {
            let healthVC =  UIStoryboard.getHealthStoryBoard().instantiateInitialViewController() as! HealthViewController
            navigationController?.pushViewController(healthVC, animated: true)
        }
    }
    
    //MARK: - Nearest District API
    func callNearestDistrictAPI()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit =  EndPoints.getNearest(String(LocationManager.sharedInstance().newLongitude), String(LocationManager.sharedInstance().newLatitude)).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 404
            {
                let responseDictionary = dictionary!
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                
                if replyType == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String:Any]
                    guard dataDict != nil  else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    modelSignUpProcess.stateName = dataDict!["state_region"] as? String
                    modelSignUpProcess.districtName = dataDict!["district"] as? String
                    modelSignUpProcess.townName = dataDict!["town"] as? String
                    modelSignUpProcess.districttownID = dataDict!["id"] as? String

                    let strLatitude = dataDict!["latitude"] as! Double
                    let strLongitude = dataDict!["longitude"] as! Double
                    
                    let locationArry:NSMutableArray = [strLongitude,strLatitude]
                    
                    modelSignUpProcess.locationArry = locationArry.mutableCopy() as? NSMutableArray
                    
                    print(modelSignUpProcess.stateName!)
                    print(modelSignUpProcess.districtName!)
                    print(modelSignUpProcess.townName!)
                    
                    self.locationTableView.reloadData()
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                UtilityClass.showAlertWithTitle(title: "", message:App_Global_Error_Msg  , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
        }
    }
}

//MARK: -
//MARK: - Extension -> UITableView -> UITableViewDataSource, UITableViewDelegate
extension ChooseLocationViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : HealthDataCell!
        
        let cellIdentifier = kCellIdentifier_HealthData_BloodType
        
        var cellType = HealthDataCellType.height
        
        switch indexPath.row {
            
        case 0:
            cellType = .state
            
        case 1:
            cellType = .district
            
        case 2:
            cellType = .town
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HealthDataCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        cell.updateCell()
        
        return cell
    }
}

//MARK: - Extension -> HealthDataCellDelegate
extension ChooseLocationViewController: HealthDataCellDelegate
{
    func healthDataCell(_ cell: HealthDataCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
        case .state:
            print("state : "+inputValue)
            
            modelSignUpProcess.stateName = inputValue
            modelSignUpProcess.districtName = ""
            modelSignUpProcess.townName = ""
            
            let indexPath0 = IndexPath(item: 0, section: 0)
            let indexPath1 = IndexPath(item: 1, section: 0)
            let indexPath2 = IndexPath(item: 2, section: 0)
            
            locationTableView.reloadRows(at: [indexPath0, indexPath1, indexPath2], with: .automatic)
            
        case .district:
            print("district : "+inputValue)
            
            modelSignUpProcess.districtName = inputValue
            modelSignUpProcess.townName = ""
            
            let indexPath1 = IndexPath(item: 1, section: 0)
            let indexPath2 = IndexPath(item: 2, section: 0)
            
            locationTableView.reloadRows(at: [indexPath1, indexPath2], with: .automatic)
            
        case .town:
            print("town : "+inputValue)
            
            modelSignUpProcess.townName = inputValue
            modelSignUpProcess.districttownID = UtilityClass.getTownID(inputValue)
            
            let strLatitude = UtilityClass.getLatitudeOfTown(inputValue)
            let strLongitude = UtilityClass.getLongitudeOfTown(inputValue)
            
            let locationArry:NSMutableArray = [strLongitude,strLatitude]
            
            modelSignUpProcess.locationArry = locationArry.mutableCopy() as? NSMutableArray
            
            print("townID : "+modelSignUpProcess.districttownID!)
            
            let indexPath2 = IndexPath(item: 2, section: 0)
            
            locationTableView.reloadRows(at: [indexPath2], with: .automatic)
            
        default:
            break
        }
    }
}
