//
//  PasswordViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 04/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import PKHUD

class PasswordViewController: UIViewController {

    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var agreBtn: UIButton!
    @IBOutlet weak var agreeLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var passwordTableView: UITableView!
    @IBOutlet var labelPhoneNumber: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var termLabel: UILabel!
    
    var termsCheckedOrNot = false
    var isFromForgot = false
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupTableView()
        setupView()
    }
    
    // MARK: - deinit
    deinit
    {
        print("PasswordViewController Deinit")
    }
    
    //MARK: - setupTableView
    func setupTableView()  {
        
        passwordTableView.dataSource = self
        passwordTableView.delegate = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }
    
    //MARK: - setupView
    func setupView()
    {
        if isFromForgot {
            backBtn.isHidden = true
        } else {
            backBtn.isHidden = false
        }
        
        let headerString = "Ok, your phone number will use \n as an user name. Please type a \n password.".localized()
        
        let attributedString1 = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString1
        
        labelPhoneNumber.text = "+95-"+modelSignUpProcess.mobile!
        
        //let attributedString = NSMutableAttributedString(string: "I agree to the terms of service", attributes: [.font: UIFont.createPoppinsRegularFont(withSize: 15.0),.foregroundColor: UIColor(red: 30.0 / 255.0, green: 197.0 / 255.0, blue: 155.0 / 255.0, alpha: 1.0),.kern: 0.3])
        //attributedString.addAttribute(.foregroundColor, value: UIColor(white: 153.0 / 255.0, alpha: 1.0), range: NSRange(location: 0, length: 14))
        //
        //agreeLabel.attributedText = attributedString
        
        agreeLabel.isUserInteractionEnabled = true
        let gestureAgree = UITapGestureRecognizer.init(target: self, action: #selector(termsClicked))
        agreeLabel.addGestureRecognizer(gestureAgree)
        
        termLabel.isUserInteractionEnabled = true
        termLabel.addGestureRecognizer(gestureAgree)
        
        agreeLabel.text = "I agrre to the".localized()
        termLabel.text = " term of services".localized()
        
        confirmBtn.setTitle("Confirm".localized(), for: .normal)
    }
    
    //MARK: - termsClicked
    @objc func termsClicked ()
    {
        let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
        staticPagesVC.selectedIndex = 3
        self.navigationController?.pushViewController(staticPagesVC, animated: true)
        
        //UtilityClass.openSafariController(usingLink: .termsAndCond, onViewController: self)
    }
    
    //MARK: - Validate InputField
    func validateTheInputField() -> Bool {
        
        if modelSignUpProcess.password == nil || modelSignUpProcess.password!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter password first.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.password!.count < 6 || modelSignUpProcess.password!.count > 20
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Password should be in range of 6 to 20 characters.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.password_confirmation == nil || modelSignUpProcess.password_confirmation!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter confirm password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.password_confirmation != modelSignUpProcess.password
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Password doesn't match.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if termsCheckedOrNot == false
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please check terms & services.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - Agree Button Action
    @IBAction func agreeButtonAction(_ sender: Any) {
        if termsCheckedOrNot == false {
            termsCheckedOrNot = true
            agreBtn.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        } else {
            termsCheckedOrNot = false
            agreBtn.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
    }
    
    //MARK: - Confirm Button Action
    @IBAction func onConfirmButtonAction(_ sender: UIButton)
    {
        if validateTheInputField()
        {
            if isFromForgot {
                callResetPasswordAPi()
            } else {
                callRegisterAPi()
            }
        }
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Call Register API
    func callRegisterAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getRegisterDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.register.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
        
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    UtilityClass.saveUserInfoData(userDict: dataDict!)
                    modelSignUpProcess.tempToken = dataDict!["tmp_token"] as? String
                    
                    NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": dataDict!["id"]] as? [AnyHashable : Any])
                    
                    let otpVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "otpVC") as! OTPViewController
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Call Reset Password API
    func callResetPasswordAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getResetPasswordDataDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.resetPassword.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    let nav = UINavigationController.init(rootViewController: welcomeVC)
                    UtilityClass.changeRootViewController(with: nav)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: -
//MARK: - Extension -> UITableView -> UITableViewDataSource, UITableViewDelegate
extension PasswordViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : PhoneInfoCell!
        
        let cellIdentifier = kCellIdentifier_PhoneInfo_Email
        
        var cellType = PhoneInfoCellType.none
        
        switch indexPath.row {
        case 0:
            cellType = .password
            
        case 1:
            cellType = .confirmPassword
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PhoneInfoCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: - Extension -> PhoneDataCellDelegate
extension PasswordViewController : PhoneDataCellDelegate
{
    func phoneDataCell(_ cell: PhoneInfoCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
        case .password:
            modelSignUpProcess.password = inputValue
            
        case .confirmPassword:
            modelSignUpProcess.password_confirmation = inputValue
            
        default:
            break
        }
    }
}
