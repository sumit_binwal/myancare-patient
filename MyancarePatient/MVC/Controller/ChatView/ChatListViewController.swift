//
//  ChatViewController.swift
//  SampleScreens
//
//  Created by Bharat Kumar Pathak on 04/07/17.
//  Copyright © 2017 Konstant. All rights reserved.
//

import UIKit
import PKHUD

/// Chat List Model Data
class ModelChatList
{
    ///    Chat Message Conversation ID
    var conversationID = ""
    ///    Chat Message UserName
    var userName = ""
    ///    Chat Message UserID
    var userID = ""
    ///    Chat Message User Picture
    var userPicture = ""
    ///    User Last Message Text
    var lastMessage = ""
    ///    Unread Message Count
    var unreadCount = 0
    ///    Doctor Related Data
    var doctorDict : [String : Any] = [:]
    ///    Flag That Show User Is Blocked Or Not
    var isBlocked = false
    ///    Flag That Show Account Is Deleted or Not
    var isAccountDeleted = false
    
    
    /// Update Chat Model Data With Data
    ///
    /// - Parameter dictionary: Conversation Dictionray
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        //print(dictionary)
        
        if let id = dictionary["id"] as? String
        {
            self.conversationID = id
        }
        
        if let unread = dictionary["patient_unread"] as? Int
        {
            //print("unread = ", unread)
            self.unreadCount = unread
        }
    
        if let userDict = dictionary["doctor"] as? [String:Any]
        {
            doctorDict = userDict
            
            if let name = userDict["name"] as? String
            {
                self.userName = name
            }
            
            if let userID = userDict["id"] as? String
            {
                self.userID = userID
            }
            
            if let picture = userDict["avatar_url"] as? String
            {
                self.userPicture = picture
            }
        }
        
        if let lastMsgDict = dictionary["messages"] as? [[String:Any]]
        {
            if lastMsgDict.count > 0
            {
                let messageDict = lastMsgDict[0]
                self.lastMessage = messageDict["body"] as! String
            }
        }
    }
}

class ChatListViewController: UIViewController
{
    
    /// Table View Chat List Refrence
    @IBOutlet weak var tableChatList: UITableView!
    
    /// Chat Title View Refrence
    @IBOutlet var titleView: UIView!
    
    /// Group Image Button Refrence
    @IBOutlet weak var groupImageButton: UIBarButtonItem!
    
    /// Back Button Refrence
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    /// More Button Refrence
    @IBOutlet weak var moreButton: UIBarButtonItem!
    
    /// Group Name Label Refrence
    @IBOutlet weak var groupName: UILabel!
    
    /// Group Status Label Refrence
    @IBOutlet weak var groupStatus: UILabel!
    
    /// Model Chat List Array Refrence
    var arrayChatList = [ModelChatList]()

    
    /// View Controller Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupTableView()
        setUpNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "No Active Internet Connection Found", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        getConversationListFromSocket()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    ///MARK:- Set Navigation Bar
    func setUpNavigationBar()
    {
        self.title = "Chats".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    ///MARK:- navigation bar back Button action
    /// Back Button Pressed Clicked Action
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    /// Setup Table View Data
    func setupTableView()
    {
        tableChatList.delegate = self
        tableChatList.dataSource = self
    }

    /// Did Receive Memory Warning
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    /// Get All Chat Conversation List From WebServer - (Socket)
    func getConversationListFromSocket()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        SocketManagerHandler.sharedInstance().getConversationList { (chatListArray, statusCode) in
            
            HUD.hide()
            
            if statusCode == 200
            {
                self.updateModel(usingArray: chatListArray)
            }
        }
    }
    
    //MARK:- Update Model
    /// Update Chat List Model Values To Server Response Data
    ///
    /// - Parameter array: Getting Chat List Conversation Data Array From Server
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayChatList.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelChatList () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayChatList.append(model) // Adding model to array
        }
        
        tableChatList.reloadData()
    }
    
    //MARK:- Action Events
    
    /// On Back Button Click Event
    ///
    /// - Parameter sender: UiBarButton (Back Button) Refrence
    @IBAction func onBackAction(_ sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /// On Back Button Click Event
    ///
    /// - Parameter sender: UiBarButton (Back Button) Refrence

    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func onMoreButtonAction() -> Void
    {
        
    }
    
    func onProfileImageButtonAction() -> Void
    {
        
    }
}


// MARK: - UiTableView Data Source And Delegate Method Extension
extension ChatListViewController:UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayChatList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: ConnectDirectCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_connect_direct_cell) as! ConnectDirectCell
        
        cell.updateCell(usingModel: arrayChatList[indexPath.row])
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let chatVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
    
        chatVC.chatRoomID = arrayChatList[indexPath.row].conversationID
        chatVC.senderID = arrayChatList[indexPath.row].userID
        chatVC.chatUsername = arrayChatList[indexPath.row].userName

        navigationController?.pushViewController(chatVC, animated: true)
    }
}
