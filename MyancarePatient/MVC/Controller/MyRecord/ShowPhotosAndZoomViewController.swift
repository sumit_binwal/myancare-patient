//
//  ShowPhotosAndZoomViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

///ShowPhotosAndZoom View Controller - VIewConroller Class
class ShowPhotosAndZoomViewController: UIViewController {
    
    ///UICollection View OutLet - photosCollectionView
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    ///NSMutableArray Variable - myRecordAddFilesArr
    var myRecordAddFilesArr : NSMutableArray = []
    
    ///Variable Int Type - SelectedIndex
    var selectedIndex = 0
    
    ///Variable - arrayChatMessageSendToMediaView Array - ModelChatMessage
    var arrayChatMessagesSendToMedaiView = [ModelChatMessage]()
    
    ///Bool Variable - isFromChatController : To Identify that comes from ChatViewController or not
    var isFromChatController = false
    
    ///UIPageControl Variable
    @IBOutlet var pageControlArticles: UIPageControl!
    
    
    //MARK :- View Controller Life Cycle Methods
    /// ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        setUpView()
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("ShowPhotosAndZoomViewController deinit")
    }
    
    
    /// SetUpView Method
    func setUpView() {
        
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        
        if isFromChatController {
            pageControlArticles.numberOfPages = arrayChatMessagesSendToMedaiView.count
            pageControlArticles.isHidden = true
        } else {
            pageControlArticles.numberOfPages = myRecordAddFilesArr.count
        }
        
        pageControlArticles.currentPage = selectedIndex
        
        photosCollectionView.reloadData()
        photosCollectionView.layoutIfNeeded()
        photosCollectionView.scrollToItem(at: IndexPath.init(row: selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    //MARK:- setUpNavigationBar
    /// SetUp Navigation Bar Method
    func setUpNavigationBar() {
        
        if isFromChatController {
            self.title = "Media".localized()
        } else {
            self.title = "Medical Records".localized()
        }
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    /// Back Button Clicked Action Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
/// Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ShowPhotosAndZoomViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isFromChatController {
            return self.arrayChatMessagesSendToMedaiView.count
        } else {
            return self.myRecordAddFilesArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ZoomCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ZoomCollectionViewCell", for: indexPath) as! ZoomCollectionViewCell
        
        cell.zoomImageView.setShowActivityIndicator(true)
        cell.zoomImageView.setIndicatorStyle(.gray)
        
        if isFromChatController {
            
            cell.zoomImageView.sd_setImage(with: URL.init(string: arrayChatMessagesSendToMedaiView[indexPath.row].fullImageString), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
            
        } else {
            
            let myrecordfile = myRecordAddFilesArr[indexPath.row] as! MyRecordFilesAddModel
            
            cell.zoomImageView.sd_setImage(with: URL.init(string: myrecordfile.recordImageUrl), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
        }
        
        cell.imageViewZommScrol.minimumZoomScale = 1.0
        cell.imageViewZommScrol.maximumZoomScale = 5.0
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let indexPath = photosCollectionView.indexPathsForVisibleItems.first
        pageControlArticles.currentPage = (indexPath?.row)!
        photosCollectionView.reloadData()
    }
}

