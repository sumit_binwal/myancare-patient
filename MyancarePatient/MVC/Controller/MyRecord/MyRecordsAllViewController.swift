//
//  MyRecordsAllViewController.swift
//  MyanCareDoctor
//
//  Created by iOS on 28/02/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD


/// MyRecordsAll ViewController
class MyRecordsAllViewController: UIViewController {

    
    /// MyRecord TableView Refrence
    @IBOutlet weak var myRecordTableView: UITableView!
    
    /// MedicalRecord Data Array
    var myRecordDataArr = [MedicalRecord]()
    
    /// Variable - IsPaging Boolian
    var isPaging = false
    
    /// Variable - Reresh Controller
    var refreshControl1 = UIRefreshControl()
    
    //MARK:- viewDidLoad
    
    /// ViewController LIfe Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
        
        myRecordTableView.isHidden = true
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            myRecordTableView?.refreshControl = refreshControl1
        } else {
            myRecordTableView?.addSubview(refreshControl1)
        }
        
        // Configure Refresh Control
        refreshControl1.addTarget(self, action: #selector(refreshAppointmentData(_:)), for: .valueChanged)
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector (someActionAtMyRecordAll), name: NSNotification.Name(rawValue: "someActionAtMyRecordAll"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("MyRecordsAllViewController deinit")
    }
    
    
    /// Pull TO Refresh On MyRecord TableView Method
    ///
    /// - Parameter sender: refreshControll Button Refrence
    @objc func refreshAppointmentData(_ sender: Any) {
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    

    
    //MARK: - setTableViewDelegate
    
    /// Set TableView Delegate Methdos
    func setTableViewDelegate()  {
        
        myRecordTableView.delegate = self
        myRecordTableView.dataSource = self
        
        myRecordTableView.emptyDataSetSource = self
        myRecordTableView.emptyDataSetDelegate = self
    }
    
    //MARK: - setUpNavigationBar
    /// Setup Navigation Bar
    func setUpNavigationBar() {
        
        self.title = "Record Books".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    /// Back Button Click Event Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /// Fetch All Medicle Record From Server
    func callAllRecordsApi()
    {
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    
    /// Fetch All Medicle Record From Server
    @objc func someActionAtMyRecordAll() {
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    
    //Mark :- get medical record listing from api
    
    /// Call APi For Fetching All Record From Server
    func getMedicalRecordListFromAPI()
    {
        let urlToHit = EndPoints.medicalRecordsAll(myRecordDataArr.count).path
        
        if myRecordDataArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                self.myRecordTableView.isHidden = false
                
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                self.myRecordTableView.isHidden = false
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    HUD.hide()
                    self.refreshControl1.endRefreshing()
                    
                    self.myRecordTableView.isHidden = false
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                self.myRecordTableView.isHidden = false
                
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    
    /// Update Medicle Record Data After Fetching from Server
    ///
    /// - Parameter array: Fetched server Data Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = MedicalRecord () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            myRecordDataArr.append(model) // Adding model to array
        }
        
        myRecordTableView.reloadData()
        
        myRecordTableView.isHidden = false
        
        HUD.hide()
        self.refreshControl1.endRefreshing()
    }
    
    //MARK: - Call Delete record API
    
    /// Delete Record Api Call
    func callDeleteRecordAPi()
    {
        self.view.endEditing(true)
        
        let params = [
            "medicalrecord_id" : modelAddNewRecordProcess.recordID ?? ""
            ] as [String : Any]
        
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.createMedicalRecords.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        // post a notification
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "someActionAtMyRecord"), object: nil, userInfo: nil)
                        
                        self.myRecordDataArr.removeAll()
                        self.isPaging = true
                        
                        self.getMedicalRecordListFromAPI()
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// MARK: - UITabelView DataSource And Delegate Method Extension
extension MyRecordsAllViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myRecordDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (myRecordDataArr[indexPath.row].user!["id"] as? String ?? "" == UtilityClass.getUserIdData()) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRecordTableViewCell") as! MyRecordTableViewCell
            
            cell.delegate = self
            
            cell.doctorNameLabel.text = myRecordDataArr[indexPath.row].doctorName
            cell.diseaseNameLabel.text = myRecordDataArr[indexPath.row].diseaseName
            cell.hospitalNameLabel.text = myRecordDataArr[indexPath.row].hospitalName
            cell.dateLabel.text = myRecordDataArr[indexPath.row].date
            
            cell.viewButton.tag = indexPath.row
            cell.editButton.tag = indexPath.row
            cell.deleteButton.tag = indexPath.row
            cell.shareButton.tag = indexPath.row
            
            if indexPath.row == myRecordDataArr.count - 1 && isPaging {
                getMedicalRecordListFromAPI()
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRecordAllTableViewCell") as! MyRecordAllTableViewCell
            
            var spec = "Shared By".localized() as NSString
            spec = spec.appending(" : \(myRecordDataArr[indexPath.row].user!["name"] as? String ?? "")") as NSString
            
            cell.patientNameLabel.text = spec as String
            
            cell.doctorNameLabel.text = myRecordDataArr[indexPath.row].user!["name"] as? String ?? ""
            cell.diseaseNameLabel.text = myRecordDataArr[indexPath.row].diseaseName
            cell.hospitalNameLabel.text = myRecordDataArr[indexPath.row].hospitalName
            cell.dateLabel.text = myRecordDataArr[indexPath.row].date
            
            cell.viewButton.tag = indexPath.row
            
            cell.delegate = self
            
            if indexPath.row == myRecordDataArr.count - 1 && isPaging {
                getMedicalRecordListFromAPI()
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (myRecordDataArr[indexPath.row].user!["id"] as? String ?? "" == UtilityClass.getUserIdData()) {
            return (176 * scaleFactorX)
        } else {
            return (220 * scaleFactorX)
        }
    }
}


// MARK: - MyRecordAllTableview Cell Delgate Method Extension
extension MyRecordsAllViewController : MyRecordAllTableViewCellDelegate
{
    
    /// View My Full Record Button Clicked Event
    ///
    /// - Parameter cell: MyRecordAllView Cell Refrence
    func myRecordCellViewButtonAction(_ cell: MyRecordAllTableViewCell) {
        
        let tag = cell.viewButton.tag
        
        if (myRecordDataArr[tag].recordFiles?.count)! > 0 {
            
            let myRecordPhotosVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "PhotosShowVC") as! PhotosShowViewController
            
            myRecordPhotosVC.myRecordID = myRecordDataArr[tag].recordID!
            
            self.navigationController?.pushViewController(myRecordPhotosVC, animated: true)
        } else {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "No record files found".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
}

//MARK:- extension -> MyRecordTableViewCellDelegate
///MyRecord TableViewCell Delegate Extension
extension MyRecordsAllViewController : MyRecordTableViewCellDelegate
{
    /// View My Full Record Button Clicked Event
    ///
    /// - Parameter cell: MyRecordTableViewCell Cell Refrence

    func myRecordCellViewButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.viewButton.tag
        
        if (myRecordDataArr[tag].recordFiles?.count)! > 0 {
            
            let myRecordPhotosVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "PhotosShowVC") as! PhotosShowViewController
            
            myRecordPhotosVC.myRecordID = myRecordDataArr[tag].recordID!
            
            self.navigationController?.pushViewController(myRecordPhotosVC, animated: true)
        } else {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "No record files found".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    
    /// Edit My Medical Record Clicked Event
    ///
    /// - Parameter cell: MyRecordTableViewCell Refrence
    func myRecordCellEditButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.editButton.tag
        
        let editNewRecordVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "EditRecordVC") as! EditRecordViewController
        
        editNewRecordVC.medicalRecordData = myRecordDataArr[tag]
        
        self.navigationController?.pushViewController(editNewRecordVC, animated: true)
    }
    
    
    /// Delete My Medicle Record Clicked Event
    ///
    /// - Parameter cell: MyRecordTableView Cell Refrence
    func myRecordCellDeleteButtonAction(_ cell: MyRecordTableViewCell) {
        
        UtilityClass.showAlertWithTitle(title: "", message: "Do you really want to delete this record?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()], dismissHandler: { (buttonIndex) in
            
            if buttonIndex == 0 {
                
                let tag = cell.deleteButton.tag
                
                modelAddNewRecordProcess.recordID = self.myRecordDataArr[tag].recordID
                self.callDeleteRecordAPi()
            }
        })
    }
    
    
    /// Share My Record Button Clicked Event
    ///
    /// - Parameter cell: MyRecordTableViewCell Refrence
    func myRecordCellShareButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.shareButton.tag
        
        let recordShareVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "RecordShareVC") as! RecordShareViewController
        recordShareVC.myRecordDataShare = myRecordDataArr[tag]
        self.navigationController?.pushViewController(recordShareVC, animated: true)
    }
}



//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///DZNEmptyDataSet Delegate Method Extension 
extension MyRecordsAllViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
