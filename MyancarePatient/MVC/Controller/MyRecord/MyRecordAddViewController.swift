//
//  MyRecordAddViewController.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

//MARK: -

/// Model Add New Record Process Model
var modelAddNewRecordProcess = AddNewRecordModel ()

class MyRecordAddViewController: UIViewController
{
    
    ///Myrecord TableView Refrence
    @IBOutlet weak var addRecordTableView: UITableView!
    ///Confirm Button Refrence
    @IBOutlet weak var confirmButton: UIButton!
    ///Cancel Button Refrence
    @IBOutlet weak var cancelButton: UIButton!
    
    ///Variable :Bool - IsRocord Is Edited or Not
    var isEditRecord = false
    
    ///Variable :Medical Record Model Data
    var medicalRecordData = MedicalRecord()
    
    //MARK:- viewDidLoad
    
    /// View Controller Life Cycle Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }

    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        setUpData()
        
        confirmButton.setTitle("Confirm".localized(), for: .normal)
        cancelButton.setTitle("Cancel".localized(), for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit
    {
        print("MyRecordAddViewController deinit")
    }
    
    //MARK: - setUpTableView
    
    /// Setup TableView Method
    func setUpTableView ()
    {
        addRecordTableView.delegate = self
        addRecordTableView.dataSource = self
    }
    
    //MARK: - setUpData
    /// Setup ViewController Method
    func setUpData ()
    {
        if isEditRecord
        {
            modelAddNewRecordProcess.recordID = medicalRecordData.recordID
            modelAddNewRecordProcess.doctorName = medicalRecordData.doctorName
            modelAddNewRecordProcess.diseaseName = medicalRecordData.diseaseName
            modelAddNewRecordProcess.hospitalName = medicalRecordData.hospitalName
            modelAddNewRecordProcess.date = medicalRecordData.date
        }
        else
        {
            modelAddNewRecordProcess.date = ""
            modelAddNewRecordProcess.diseaseName = ""
            modelAddNewRecordProcess.doctorName = ""
            modelAddNewRecordProcess.hospitalName = ""
            modelAddNewRecordProcess.recordID = ""
        }
    }
    
    //MARK: - setUpNavigationBar
    /// Setup Navigation Bar Method
    func setUpNavigationBar()
    {
        self.title = "New Medical Record".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    
    /// Back Button Clicked Event
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- on cancel button action
    /// Cancel Button Clicked Event
    @IBAction func cancelButtonAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- on confirm button action
    /// Confirm Button Clicked Event
    @IBAction func confirmButtonAction(_ sender: Any)
    {
        if validateTheInputField()
        {
            if isEditRecord
            {
                callEditNewRecordAPi()
            }
            else
            {
                callCreateNewRecordAPi()
            }
        }
    }
    
    //MARK:- get params for add new record
    
    /// Add New Record On Existing Data Array
    ///
    /// - Returns: New Record Data Dictionary
    func getAddNewRecordParametersdata () -> [String : String]
    {
        let dictAddNewRecord = [
            "doctor_name" : modelAddNewRecordProcess.doctorName,
            "disease_name" : modelAddNewRecordProcess.diseaseName,
            "hospital_name" : modelAddNewRecordProcess.hospitalName,
            "recorded_date" : modelAddNewRecordProcess.date
        ]
        
        return dictAddNewRecord as! [String : String]
    }
    
    //MARK: - Call add new record API
    
    /// Create New Record API Call
    func callCreateNewRecordAPi()
    {
        self.view.endEditing(true)
        
        let params = getAddNewRecordParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.createMedicalRecords.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let replyData = responseDictionary["data"] as! [String : Any]
                    let recordID = replyData["id"] as! String
                    
                    let photosVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "MyRecordPhotosVC") as! MyRecordPhotosViewController
                    
                    photosVC.myRecordID = recordID
                    photosVC.isAddFiles = true
                    
                    self.navigationController?.pushViewController(photosVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- get edit record params

    /// GetEdit Record Parameters Data
    ///
    /// - Returns: Edited Records Data
    func getEditRecordParametersdata () -> [String : String]
    {
        let dictAddNewRecord = [
            "medicalrecord_id" : modelAddNewRecordProcess.recordID,
            "doctor_name" : modelAddNewRecordProcess.doctorName,
            "disease_name" : modelAddNewRecordProcess.diseaseName,
            "hospital_name" : modelAddNewRecordProcess.hospitalName,
            "recorded_date" : modelAddNewRecordProcess.date
        ]
        
        return dictAddNewRecord as! [String : String]
    }
    
    //MARK: - Call Edit record API
    
    /// Edit New Record Api Call
    func callEditNewRecordAPi()
    {
        self.view.endEditing(true)
        
        let params = getEditRecordParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.createMedicalRecords.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .put, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK : - Validation method
    
    /// Validate Inpute TextField
    ///
    /// - Returns: Bool True/False
    func validateTheInputField() -> Bool
    {
        if modelAddNewRecordProcess.doctorName == nil || modelAddNewRecordProcess.doctorName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter doctor name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddNewRecordProcess.diseaseName == nil || modelAddNewRecordProcess.diseaseName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter disease name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddNewRecordProcess.hospitalName == nil || modelAddNewRecordProcess.hospitalName!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter hospital name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelAddNewRecordProcess.date == nil || modelAddNewRecordProcess.date!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// UITableView DataSource Delegate Method Extension
extension MyRecordAddViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : MyRecordAddTableViewCell!
        
        let cellIdentifier = "MyRecordAddTableViewCell"
        
        var cellType = AddRecordType.none
        
        switch indexPath.row {
        
        case 0:
            cellType = .name
            
        case 1:
            cellType = .disease
            
        case 2:
            cellType = .hospital
            
        case 3:
            cellType = .date
        
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MyRecordAddTableViewCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.updateCell()
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (80 * scaleFactorX)
    }
}

//MARK: - Extention -> PersonalDataCellDelegate
///MyRecordAddTableViewCellDelegate Method Extension
extension MyRecordAddViewController : MyRecordAddTableViewCellDelegate
{

    /// Add Record And Fill TableView Cell Values
    ///
    /// - Parameters:
    ///   - cell: MyRecordAddTableViewCell Refrence
    ///   - inputValue: Inpute Value Filled By user
    func recordAddDataCell(_ cell: MyRecordAddTableViewCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
            
        case .name:
            modelAddNewRecordProcess.doctorName = inputValue
            
        case .disease:
            modelAddNewRecordProcess.diseaseName = inputValue
            
        case .hospital:
            modelAddNewRecordProcess.hospitalName = inputValue
            
        case .date:
            modelAddNewRecordProcess.date = inputValue
            
        default:
            break
        }
    }
}
