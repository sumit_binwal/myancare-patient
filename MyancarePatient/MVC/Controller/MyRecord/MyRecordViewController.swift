//
//  MyRecordViewController.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD

/// MyRecordViewController
class MyRecordViewController: UIViewController {

    ///MyRecord tableview Refrence
    @IBOutlet weak var myRecordTableView: UITableView!
    
    ///Medicle Record Data Array
    var myRecordDataArr = [MedicalRecord]()
    
    
    /// Bool Variable - isPaging
    var isPaging = false
   
    /// UIRefreshControl Variable
    var refreshControl1 = UIRefreshControl()
    
    //MARK:- viewDidLoad
    
    /// ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
        
        myRecordTableView.isHidden = true
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            myRecordTableView?.refreshControl = refreshControl1
        } else {
            myRecordTableView?.addSubview(refreshControl1)
        }
        
        // Configure Refresh Control
        refreshControl1.addTarget(self, action: #selector(refreshAppointmentData(_:)), for: .valueChanged)
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
        
        NotificationCenter.default.addObserver(self, selector: #selector (someActionAtMyRecord), name: NSNotification.Name(rawValue: "someActionAtMyRecord"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("MyRecordViewController deinit")
    }

    /// Pull to Refresh
    ///
    /// - Parameter sender:
    @objc func refreshAppointmentData(_ sender: Any) {
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    
    
    //MARK: - setTableViewDelegate
    
    /// Set TableView Delegate
    func setTableViewDelegate()  {
        
        myRecordTableView.delegate = self
        myRecordTableView.dataSource = self
        
        myRecordTableView.emptyDataSetSource = self
        myRecordTableView.emptyDataSetDelegate = self
    }

    //MARK: - setUpNavigationBar
    /// setUpNavigationBar

    func setUpNavigationBar() {
        
        self.title = "Record Books".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add-reminder"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(addButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    
    /// BackButton Clicked Event
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- navigation bar add button action
    /// Add Button Clicked Event
    @objc func addButtonPressed() {
        
        let addNewRecordVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "MyRecordAddVC") as! MyRecordAddViewController
        self.navigationController?.pushViewController(addNewRecordVC, animated: true)
    }
    
    /// Call My Records API
    func callMyRecordsApi()
    {
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    
    /// Some Actions At My Records API
    @objc func someActionAtMyRecord() {
        
        myRecordDataArr.removeAll()
        isPaging = true
        
        getMedicalRecordListFromAPI()
    }
    
    //Mark :- get medical record listing from api
    
    /// get medical record listing from api
    func getMedicalRecordListFromAPI()
    {
        let urlToHit = EndPoints.medicalRecords(myRecordDataArr.count).path
        
        if myRecordDataArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                self.myRecordTableView.isHidden = false
                
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                self.myRecordTableView.isHidden = false
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    HUD.hide()
                    self.refreshControl1.endRefreshing()
                    
                    self.myRecordTableView.isHidden = false
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
               
                HUD.hide()
                self.refreshControl1.endRefreshing()
                
                self.myRecordTableView.isHidden = false
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    
    /// Updating Model MedicalRecord Array
    ///
    /// - Parameter array:
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = MedicalRecord () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            myRecordDataArr.append(model) // Adding model to array
        }
        
        myRecordTableView.reloadData()
        
        myRecordTableView.isHidden = false
        
        HUD.hide()
        self.refreshControl1.endRefreshing()
    }
    
    //MARK: - Call Delete record API
    
    /// Call Delete record API
    func callDeleteRecordAPi()
    {
        self.view.endEditing(true)
        
        let params = [
            "medicalrecord_id" : modelAddNewRecordProcess.recordID ?? ""
            ] as [String : Any]
        
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.createMedicalRecords.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "someActionAtMyRecordAll"), object: nil)
                        
                        self.myRecordDataArr.removeAll()
                        self.isPaging = true
                        
                        self.getMedicalRecordListFromAPI()
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// UITableViewDataSoruce Delegate Methods Extension
extension MyRecordViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myRecordDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyRecordTableViewCell") as! MyRecordTableViewCell
        
        cell.delegate = self
        
        cell.doctorNameLabel.text = myRecordDataArr[indexPath.row].doctorName
        cell.diseaseNameLabel.text = myRecordDataArr[indexPath.row].diseaseName
        cell.hospitalNameLabel.text = myRecordDataArr[indexPath.row].hospitalName
        cell.dateLabel.text = myRecordDataArr[indexPath.row].date
        
        cell.viewButton.tag = indexPath.row
        cell.editButton.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row
        cell.shareButton.tag = indexPath.row
        
        if isPaging && indexPath.row == myRecordDataArr.count - 1 {
            getMedicalRecordListFromAPI()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (176 * scaleFactorX)
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension MyRecordViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK:- extension -> MyRecordTableViewCellDelegate
///MyRecordTableView Cell Delegate Methods
extension MyRecordViewController : MyRecordTableViewCellDelegate
{
    
    /// View Full Medicle Record Clicked Action
    ///
    /// - Parameter cell: MyRecordTableViewCell
    func myRecordCellViewButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.viewButton.tag
        
        if (myRecordDataArr[tag].recordFiles?.count)! > 0 {
            
            let myRecordPhotosVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "PhotosShowVC") as! PhotosShowViewController
            
            myRecordPhotosVC.myRecordID = myRecordDataArr[tag].recordID!
            
            self.navigationController?.pushViewController(myRecordPhotosVC, animated: true)
        } else {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "No record files found".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    /// Edit Full Medicle Record Clicked Action
    ///
    /// - Parameter cell: MyRecordTableViewCell
    func myRecordCellEditButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.editButton.tag
        
        let editNewRecordVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "EditRecordVC") as! EditRecordViewController
        
        editNewRecordVC.medicalRecordData = myRecordDataArr[tag]
        
        self.navigationController?.pushViewController(editNewRecordVC, animated: true)
    }
    
    /// Delete Full Medicle Record Clicked Action
    ///
    /// - Parameter cell: MyRecordTableViewCell

    func myRecordCellDeleteButtonAction(_ cell: MyRecordTableViewCell) {
        
        UtilityClass.showAlertWithTitle(title: "", message: "Do you really want to delete this record?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()], dismissHandler: { (buttonIndex) in
            
            if buttonIndex == 0 {
                
                let tag = cell.deleteButton.tag
                
                modelAddNewRecordProcess.recordID = self.myRecordDataArr[tag].recordID
                self.callDeleteRecordAPi()
            }
        })
    }
    
    /// Share Full Medicle Record Clicked Action
    ///
    /// - Parameter cell: MyRecordTableViewCell

    func myRecordCellShareButtonAction(_ cell: MyRecordTableViewCell) {
        
        let tag = cell.shareButton.tag
        
        let recordShareVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "RecordShareVC") as! RecordShareViewController
        recordShareVC.myRecordDataShare = myRecordDataArr[tag]
        self.navigationController?.pushViewController(recordShareVC, animated: true)
    }
}
