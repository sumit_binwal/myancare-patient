//
//  ForgotPassViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 06/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import PKHUD

class ForgotPassViewController: UIViewController {
    
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var mobileTextField: UITextField!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
    }
    
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UtilityClass.enableIQKeyboard()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    //MARK: - Deinit
    deinit {
        print("ForgotPassViewController deinit")
    }
    
    // MARK: - setupView
    func setupView()  {
        
        UtilityClass.disableIQKeyboard()
        modelSignUpProcess.mobile = ""
        
        let headerString = "Please provide your \n mobile number to reset \n password.".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        //Set Border To Container View
        containerView.setBorderColor(UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 5, borderWidth: 0.5)
        
        resetButton.setTitle("Reset Password".localized(), for: .normal)
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - on Reset Password Button Action
    @IBAction func onResetPasswordAction(_ sender: Any) {
        
        if validateTheInputField() {
            
            modelSignUpProcess.mobile = mobileTextField.text
            callForgotPasswordAPi()
        }
    }
    
    //MARK: - Validate InputField
    func validateTheInputField() -> Bool {
        
        if mobileTextField.text == nil || (mobileTextField.text?.isEmptyString())! {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
            
        } else if (mobileTextField.text?.count)! < 6 || (mobileTextField.text?.count)! > 11 {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter valid mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Call Forgot Password API
    func callForgotPasswordAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getForgotPasswordDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.forgotPassword.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    modelSignUpProcess.tempToken = dataDict!["tmp_token"] as? String
                    
                    let otpVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "otpVC") as! OTPViewController
                    otpVC.isFromForgotVC = true
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}
