//
//  ArticleDetailViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

class ArticleDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //Mark:- Properties
    var articleModelData = ArticleModel()
    
    @IBOutlet weak var btnLikeHeader: UIButton!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var btnCateType: UIButton!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnViw: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var imageViewHeader: UIImageView!
    @IBOutlet var btnBookmark: UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        labelTitle.text = articleModelData.title!
        
        btnCateType.setTitle(" \(articleModelData.cateName ?? "") ", for: .normal)
        
        imageViewHeader.setShowActivityIndicator(true)
        imageViewHeader.setIndicatorStyle(.gray)
        
        imageViewHeader.sd_setImage(with: URL(string: articleModelData.imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
        
        if articleModelData.isLikeByMe!
        {
            btnLike.setImage(#imageLiteral(resourceName: "likes-1"), for: .normal)
        }
        else
        {
            btnLike.setImage(#imageLiteral(resourceName: "unlike"), for: .normal)
        }
        
        if articleModelData.isLikeByMe!
        {
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-like-icon"), for: .normal)
        }
        else
        {
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-unlike"), for: .normal)
        }
        
        if articleModelData.isBookmark!
        {
            btnBookmark.setImage(#imageLiteral(resourceName: "bookmarks"), for: .normal)
        }
        else
        {
            btnBookmark.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
        }
        
        btnLike.setTitle(articleModelData.likesCount! + " " + "Likes".localized(), for: .normal)
        btnViw.setTitle(articleModelData.viewCount! + " " + "Views".localized(), for: .normal)
        btnComment.setTitle(articleModelData.commentsCount! + " " + "Comments".localized(), for: .normal)
        
        btnCateType.setTitle(" \(articleModelData.cateName ?? "") ", for: .normal)
        
        imageViewHeader.setShowActivityIndicator(true)
        imageViewHeader.setIndicatorStyle(.gray)
        
        imageViewHeader.sd_setImage(with: URL(string: articleModelData.imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
        
        let strPostedate = UtilityClass.getDateStringFromTimeStamp(timeStamp: articleModelData.postedTime!, dateFormat: "MMMM d, yy") as String
        
        var strPostedDateAndByObj = UtilityClass.getDateStringFromTimeStamp(timeStamp: articleModelData.postedTime!, dateFormat: "MMMM d, yy") as NSString
        
        strPostedDateAndByObj = strPostedDateAndByObj.appending(" ") as NSString
        strPostedDateAndByObj = strPostedDateAndByObj.appending("by".localized()) as NSString
        strPostedDateAndByObj = strPostedDateAndByObj.appending(" ") as NSString
        
        strPostedDateAndByObj = strPostedDateAndByObj.appending("\(articleModelData.postedBy!)") as NSString
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            labelTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: articleModelData.title!, location: 0, length: (articleModelData.title?.count)!, color: UIColor.white, font: UIFont.createPoppinsMediumFont(withSize: (18.0 + CGFloat(customFontSize))))
            
            labelTime.attributedText = UILabel.createAttributedStringForText(strObj: strPostedDateAndByObj as String, size:(14.0 + CGFloat(customFontSize)) , size2:(14.0 + CGFloat(customFontSize)), location: 0, location2: strPostedate.count + 3, length: strPostedate.count, length2:  strPostedDateAndByObj.length - strPostedate.count - 3, color: UIColor.MyanCarePatient.colorForPostedDate, color2: UIColor.MyanCarePatient.colorForPostedDate)
        }
        else
        {
            labelTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: articleModelData.title!, location: 0, length: (articleModelData.title?.count)!, color: UIColor.white, font: UIFont.createPoppinsMediumFont(withSize: 18))
            
            labelTime.attributedText = UILabel.createAttributedStringForText(strObj: strPostedDateAndByObj as String, size:14 , size2:14, location: 0, location2: strPostedate.count + 3, length: strPostedate.count, length2:  strPostedDateAndByObj.length - strPostedate.count - 3, color: UIColor.MyanCarePatient.colorForPostedDate, color2: UIColor.MyanCarePatient.colorForPostedDate)
        }
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        if (UtilityClass.getUserSidData()) != nil
        {
            self.articleViewAPI(articleID: articleModelData.articleID!)
        }
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("ArticleDetailViewController deinit")
    }
    
    //Mark:- IBActions
    
    // MARK: - Like Button Action
    @IBAction func buttonLikeTapped(_ sender: UIButton)
    {
        if (UtilityClass.getUserSidData()) == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0
                {
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
            
            return
        }
        
        if btnLikeHeader.currentImage == #imageLiteral(resourceName: "article-detail-unlike")
        {
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-like-icon"), for: .normal)
            btnLike.setImage(#imageLiteral(resourceName: "likes-1"), for: .normal)
            
            let likeCount = NSInteger(articleModelData.likesCount!)
            
            articleModelData.likesCount = String(likeCount!+1)
            
            btnLike.setTitle(articleModelData.likesCount! + " " + "Likes".localized(), for: .normal)
        }
        else
        {
            let likeCount = NSInteger(articleModelData.likesCount!)
            
            articleModelData.likesCount = String(likeCount!-1)
            
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-unlike"), for: .normal)
            btnLike.setImage(#imageLiteral(resourceName: "unlike"), for: .normal)
            
            btnLike.setTitle(articleModelData.likesCount! + " " + "Likes".localized(), for: .normal)
        }
        
        self.articleLikeUnlikeAPI(articleID: articleModelData.articleID!, articleIndex: 0)
    }
    
    // MARK: - Bookmark Button Action
    @IBAction func buttonBookmarkTapped(_ sender: UIButton)
    {
        if (UtilityClass.getUserSidData()) == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0 {
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
            
            return
        }
        
        if btnBookmark.currentImage == #imageLiteral(resourceName: "bookmark")
        {
            btnBookmark.setImage(#imageLiteral(resourceName: "bookmarks"), for: .normal)
        }
        else
        {
            btnBookmark.setImage(#imageLiteral(resourceName: "bookmark"), for: .normal)
        }
        
        self.articleBookmarkAPI(articleID: articleModelData.articleID!)
    }
    
    // MARK: - Share Button Action
    @IBAction func buttonShareTapped(_ sender: UIButton)
    {
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [articleModelData.slug ?? ""], applicationActivities:nil)
        
        activityViewController.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
        
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil);
        }
    }
    
    func doSomething(action: UIAlertAction) {
        //Use action.title
    }
    
    // MARK: - Like Button Action
    @IBAction func buttonTotalLikesTapped(_ sender: UIButton) {
        
        if (UtilityClass.getUserSidData()) == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0 {
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
            
            return
        }
        
        if btnLike.currentImage == #imageLiteral(resourceName: "unlike") {
            btnLike.setImage(#imageLiteral(resourceName: "likes-1"), for: .normal)
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-like-icon"), for: .normal)
            
            let likeCount = NSInteger(articleModelData.likesCount!)
            
            articleModelData.likesCount = String(likeCount!+1)
            
            btnLike.setTitle(articleModelData.likesCount! + " " + "Likes".localized(), for: .normal)
        }
        else
        {
            let likeCount = NSInteger(articleModelData.likesCount!)
            
            articleModelData.likesCount = String(likeCount!-1)
            
            btnLike.setTitle(articleModelData.likesCount! + " " + "Likes".localized(), for: .normal)
            btnLike.setImage(#imageLiteral(resourceName: "unlike"), for: .normal)
            
            btnLikeHeader.setImage(#imageLiteral(resourceName: "article-detail-unlike"), for: .normal)
        }
        
        self.articleLikeUnlikeAPI(articleID: articleModelData.articleID!, articleIndex: 0)
    }
    
    // MARK: - Views Button Action
    @IBAction func buttonViewsTapped(_ sender: UIButton)
    {
        
    }
    
    // MARK: - Comments Button Action
    @IBAction func buttonCommentsTapped(_ sender: UIButton) {
        
        if (UtilityClass.getUserSidData()) == nil
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex != 0 {
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
            })
            
            return
        }
        
        let commentVC = UIStoryboard.getCommentsStoryBoard().instantiateViewController(withIdentifier: "CommentsVC") as! CommentsViewController
        
        commentVC.articleID = articleModelData.articleID!
        //commentVC.hidesBottomBarWhenPushed = true
        commentVC.commentsDelegate = self
        
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
    // MARK: - Navigation Back Button Action
    @IBAction func buttonBackTapped(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //Mark:-  UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellObj : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellArticleDetail", for: indexPath)
        
        let labelDetail : UILabel = (cellObj.viewWithTag(101) as? UILabel)!
        let strDetails = articleModelData.description_long?.html2String
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            labelDetail.attributedText = UILabel.createAttributedStringForTextNormal(strObj: strDetails!, location: 0, length: (strDetails?.count)!, color: UIColor.MyanCarePatient.signupTextFocus, font: UIFont.createPoppinsRegularFont(withSize: (14.0 + CGFloat(customFontSize))))
        }
        else
        {
             labelDetail.attributedText = UILabel.createAttributedStringForTextNormal(strObj: strDetails!, location: 0, length: (strDetails?.count)!, color: UIColor.MyanCarePatient.signupTextFocus, font: UIFont.createPoppinsRegularFont(withSize: 14.0))
        }
        
        return cellObj
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //Mark :- Article Views API
    func articleViewAPI(articleID : String)
    {
        let urlToHit = EndPoints.articleView.path
        
        let params:[String:Any] = ["article_id":articleID]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.post, parameters: params) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                let replyMsg = responseDictionary["msg"] as! String
                guard replyMsg != nil else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
        }
    }
    
    //Mark: - Article Like Unlike API
    func articleLikeUnlikeAPI(articleID : String, articleIndex : Int)
    {
        let urlToHit = EndPoints.articleLike.path
        
        let params:[String:Any] = ["article_id":articleID]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.post, parameters: params) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    if self.articleModelData != nil
                    {
                        let isLiked = dataDict!["liked"] as! Bool
                        
                        if isLiked
                        {
                            self.articleModelData.isLikeByMe = true
                        }
                        else
                        {
                            self.articleModelData.isLikeByMe = false
                        }
                    }
                }
                else
                {
                    self.articleModelData.isLikeByMe = false
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //Mark: - Article BookMark API
    func articleBookmarkAPI(articleID : String)
    {
        let urlToHit = EndPoints.articleBookMark.path
        
        let params:[String:Any] = ["article_id":articleID]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.post, parameters: params) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                }
                else
                {
                    self.articleModelData.isLikeByMe = false
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: - Extension -> Data
extension Data {
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

//MARK: - Extension -> String
extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

//MARK: - Extension -> CommentsViewDelegate
extension ArticleDetailViewController : CommentsViewDelegate {
    
    func updateCommentsCount(commentsCount: Int) {
        
        articleModelData.commentsCount = String(commentsCount)
        btnComment.setTitle(articleModelData.commentsCount! + " " + "Comments".localized(), for: .normal)
    }
}

