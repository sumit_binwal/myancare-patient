//
//  AudioContollerDelegate.swift
//  MyancarePatient
//
//  Created by Jitendra Singh on 29/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class AudioContollerDelegate: NSObject, SINAudioControllerDelegate
{
    private(set) var muted = false
    var isMuted = false

    // MARK: - SINAudioControllerDelegate
    func audioControllerMuted(_ audioController: SINAudioController?)
    {
        muted = true
    }
    
    func audioControllerUnmuted(_ audioController: SINAudioController?)
    {
        muted = false
    }
}
