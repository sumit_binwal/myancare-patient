//
//  VideoCallHandlingVC.swift
//  MyancarePatient
//
//  Created by Jitendra Singh on 02/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

enum EButtonsBar
{
    case kButtonsAnswerDecline
    case kButtonsHangup
    case kButtonsWakenByCallKit
}

class VideoCallHandlingVC : SINUIViewController, SINCallDelegate
{
    @IBOutlet weak var remoteUsername: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var callStateLabel: UILabel!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var remoteVideoView: UIView!
    @IBOutlet weak var localVideoView: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var connectingLabel: UILabel!
    
    @IBOutlet weak var remoteVideoFullscreenGestureRecognizer: UIGestureRecognizer!
    @IBOutlet weak var localVideoFullscreenGestureRecognizer: UIGestureRecognizer!
    @IBOutlet weak var switchCameraGestureRecognizer: UIGestureRecognizer!
    
    var isMute    : Bool = false
    //var isSpeaker : Bool = false
    
    var appointmentID : String = ""
    var doctorID : String = ""
    
    var durationTimer: Timer?
    weak var call: SINCall?

    func audioController() -> SINAudioController
    {
        return (((UIApplication.shared.delegate as? AppDelegate)?.client)?.audioController())!
    }
    
    func videoController() -> SINVideoController
    {
        return (((UIApplication.shared.delegate as? AppDelegate)?.client)?.videoController())!
    }
    
    func setCall(_ call: SINCall)
    {
        self.call = call
        self.call?.delegate = self
    }
    
    // MARK: - UIViewController Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        if (((UIApplication.shared.delegate as? AppDelegate)?.callKitProvider)?.callExists(self.call?.callId))!
        {
            if self.call?.state == SINCallState.established
            {
                self.startCallDurationTimer(with: #selector(onDurationTimer(_:)))
                self.showButtons(EButtonsBar.kButtonsHangup)
            }
            else
            {
                self.setCallStatusText("")
                self.showButtons(EButtonsBar.kButtonsWakenByCallKit)
            }
        }
        else
        {
            if self.call?.direction == SINCallDirection.incoming
            {
                self.cameraButton.isHidden = true
                self.muteButton.isHidden = true
                
                self.setCallStatusText("incoming...")
                self.showButtons(EButtonsBar.kButtonsAnswerDecline)
            
                self.audioController().startPlayingSoundFile(path(forSound: "incoming"), loop: true)
            }
            else
            {
                self.cameraButton.isHidden = false
                self.muteButton.isHidden = false
                self.setCallStatusText("calling...")
            
                self.showButtons(EButtonsBar.kButtonsHangup)
            }
        }
        
        
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if (currentRoute.outputs != nil)
        {
            for description in currentRoute.outputs
            {
                if (description.portType == AVAudioSessionPortHeadphones)
                {
                    self.audioController().disableSpeaker()
                    print("headphone plugged in")
                }
                else
                {
                    self.audioController().enableSpeaker()
                    print("headphone pulled out")
                }
            }
        }
        else
        {
            print("requires connection to device")
        }
        
        self.localVideoFullscreenGestureRecognizer.require(toFail: switchCameraGestureRecognizer)
  
        self.videoController().localView().addGestureRecognizer (localVideoFullscreenGestureRecognizer)
    
        self.videoController().remoteView().addGestureRecognizer (remoteVideoFullscreenGestureRecognizer)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(audioRouteChangeListener(notification:)),
            name: NSNotification.Name.AVAudioSessionRouteChange,
            object: nil)
    }
    
    @objc dynamic private func audioRouteChangeListener (notification:NSNotification)
    {
        let audioRouteChangeReason = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as! UInt
        
        switch audioRouteChangeReason
        {
        
        case AVAudioSessionRouteChangeReason.newDeviceAvailable.rawValue:
            self.audioController().disableSpeaker()
            print("headphone plugged in")
        
        case AVAudioSessionRouteChangeReason.oldDeviceUnavailable.rawValue:
            self.audioController().enableSpeaker()
            print("headphone pulled out")
        
        default:
            break
        }
    }
    
    func callDismissed()
    {
        self.call?.hangup()
        
        self.dismiss(animated: true)
        {
            self.audioController().stopPlayingSoundFile()
            self.stopCallDurationTimer()
            self.videoController().remoteView().removeFromSuperview()
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
            
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        videoController().localView().contentMode = .scaleAspectFill
        videoController().remoteView().contentMode = .scaleAspectFill
        
        self.localVideoView.addSubview(videoController().localView())
        
        self.remoteVideoView.isHidden = true
        
        self.userImgView.layer.cornerRadius = 55
        self.userImgView.layer.borderWidth = 1
        self.userImgView.layer.borderColor = UIColor.white.cgColor
        self.userImgView.layer.masksToBounds = true
        
        self.audioController().unmute()
        
        if self.call?.direction == SINCallDirection.incoming
        {
            let dicData = self.call?.headers
            print(" calling user headers====== \(dicData ?? [:])")
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                appointmentID = dicData!["APPOINTMENT_ID"] as! String
            }
           
            var pushData = NSDictionary()
            var pushDict = NSDictionary()
            
            if((userDefaults.object(forKey: "pushUserInfo")) != nil)
            {
                pushData = userDefaults.object(forKey: "pushUserInfo") as! NSDictionary
                pushDict = pushData["public_headers"] as! NSDictionary
                appointmentID = pushDict["APPOINTMENT_ID"] as! String
            }
            
            print(" calling user push data====== \(pushDict)")
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                self.remoteUsername.text = dicData!["CALLER_NAME"] as? String
                
                self.doctorID = (dicData!["CALL_ID"] as? String)!
                
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(dicData!["CALLER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "welcome-doctor-logo"))
            }
            else if(pushDict != nil)
            {
                self.remoteUsername.text = pushDict["CALLER_NAME"] as? String
                
                self.doctorID = (pushDict["CALL_ID"] as? String)!
                
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(pushDict["CALLER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "welcome-doctor-logo"))
            }
            else
            {
                self.remoteUsername.text = self.call?.remoteUserId
                self.userImgView.image = #imageLiteral(resourceName: "signup-male-focus")
            }
        }
        else
        {
            let dicData = self.call?.headers
            print(" calling user headers====== \(dicData ?? [:])")
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                appointmentID = dicData!["APPOINTMENT_ID"] as! String
            }
            
            if (dicData != nil)
            {
                self.remoteUsername.text = dicData!["RECEIVER_NAME"] as? String
            
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(dicData!["RECEIVER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "signup-male-focus"))
            }
            else
            {
                self.remoteUsername.text = self.call?.remoteUserId
                self.userImgView.image = #imageLiteral(resourceName: "signup-male-focus")
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Sinch Call Fire Socket Event
    func sinchSocketCallEvent(_ eventType:String, appointmentID: String)
    {
        if !SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            return
        }
        
        SocketManagerHandler.sharedInstance().manageCallSocketEvent (callDuration, eventType: eventType, appointmentID: appointmentID) { (dataDict, statusCode) in
            
        }
    }
    
    // MARK: - Call Actions
    @IBAction func accept(_ sender: Any)
    {
        self.audioController().stopPlayingSoundFile()
        //self.audioController().enableSpeaker()
        
        sinchSocketCallEvent (SocketManageCallEventKeyword.callEventPatientPicked.rawValue, appointmentID: appointmentID)

        self.call?.answer()
    }
    
    @IBAction func decline(_ sender: Any)
    {
        self.call?.hangup()
        self.audioController().disableSpeaker()
        
        sinchSocketCallEvent (SocketManageCallEventKeyword.callEventPatientReject.rawValue, appointmentID: appointmentID)

        self.dismiss()
    }
    
    @IBAction func hangup(_ sender: Any)
    {
        self.call?.hangup()
        
        self.dismiss(animated: true)
        {
            self.audioController().stopPlayingSoundFile()
            self.stopCallDurationTimer()
            self.videoController().remoteView().removeFromSuperview()
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
            
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func muteUnmute(_ sender: Any)
    {
        if(isMute)
        {
            isMute = false
            self.audioController().unmute()
            //self.muteButton.setTitle("Mute", for: UIControlState.normal)
            self.muteButton.setImage(#imageLiteral(resourceName: "callMute"), for: UIControlState.normal)
        }
        else
        {
            isMute = true
            self.audioController().mute()
            //self.muteButton.setTitle("Unmute", for: UIControlState.normal)
            self.muteButton.setImage(#imageLiteral(resourceName: "callUnmute"), for: UIControlState.normal)
        }
    }
    
    @IBAction func onSwitchCameraTapped(_ sender: Any)
    {
        let current: AVCaptureDevice.Position = self.videoController().captureDevicePosition
        self.videoController().captureDevicePosition = SINToggleCaptureDevicePosition(current)
    }
    
    @IBAction func onFullScreenTapped(_ sender: Any)
    {
        let _: UIView? = (sender as AnyObject).view
    }

    @objc func onDurationTimer(_ unused: Timer)
    {
        let duration = Int(Date().timeIntervalSince ((self.call?.details.establishedTime)!))
        self.setDuration(duration)
    }
    
    // MARK: - SINCallDelegate
    func callDidProgress(_ call: SINCall)
    {
        print("did Progress call detail ===== \(String(describing: self.call?.details))")
        
        self.setCallStatusText("ringing...")
        self.audioController().startPlayingSoundFile(path(forSound: "ringback"), loop: true)
    }
    
    func callDidEstablish(_ call: SINCall)
    {
        self.remoteVideoView.isHidden = false
        self.logoImgView.isHidden = true
        self.titleLabel.isHidden = true
        self.remoteUsername.isHidden = true
        self.userImgView.isHidden = true
        self.callStateLabel.isHidden = true
        
        print("did establish call detail ===== \(String(describing: self.call?.details))")
        
        self.startCallDurationTimer(with: #selector(self.onDurationTimer(_:)))
        self.showButtons(EButtonsBar.kButtonsHangup)
        self.audioController().stopPlayingSoundFile()
        
        //self.audioController().enableSpeaker()
        self.cameraButton.isHidden = false
        self.muteButton.isHidden = false
    }
    
    func callDidEnd(_ call: SINCall)
    {
        print("did end call detail ===== \(String(describing: self.call?.details))")
        
        //if isDisappearing {
        //    return
        //}
        //else
        if isAppearing
        {
            setShouldDeferredDismiss(true)
            return
        }
        
        dismiss(animated: true) {
            
            self.audioController().stopPlayingSoundFile()
            self.stopCallDurationTimer()
            self.videoController().remoteView().removeFromSuperview()
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
            
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    func callDidAddVideoTrack(_ call: SINCall)
    {
        //self.localVideoView.addSubview(videoController().localView())
        self.remoteVideoView.addSubview(videoController().remoteView())
    }
    
    // MARK: - Sounds
    func path(forSound soundName: String) -> String
    {
        let audioFileName = soundName
        
        let audioFilePath = Bundle.main.path(forResource: audioFileName, ofType: "wav")
        print(audioFilePath ?? "")
        
        return audioFilePath!
    }
}
