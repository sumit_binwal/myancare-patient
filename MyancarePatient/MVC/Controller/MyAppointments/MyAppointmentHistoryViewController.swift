//
//  MyAppointmentHistoryViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import PKHUD

class MyAppointmentHistoryViewController: UIViewController {

    
    ///UITableView Refrence Outlet - Appointment Data Shows
    @IBOutlet var tableViewAppointment: UITableView!
    
    ///Array of Appointment Model Data
    var appointmentDataArr = [AppointmentModel]()
    
    ///Bool Variable - to check comes rom doctor's profile screen or not
    var isFromDoctorProfile = false
    
    ///String Variable - Store a Doctor ID
    var doctorID = ""
    
    ///Bool Variable - to check paging is applied or not
    var isPaging = false
    
    ///UIRefreshControl Refrence - Pull To Refresh Control
    var refreshControl2 = UIRefreshControl()
    
    
    /// UIViewController Life Cycle Methods
    //MARK: - viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableViewAppointment?.refreshControl = refreshControl2
        } else {
            tableViewAppointment?.addSubview(refreshControl2)
        }
        
        // Configure Refresh Control
        refreshControl2.addTarget(self, action: #selector(refreshAppointmentData(_:)), for: .valueChanged)
        
        self.tableViewAppointment.isHidden = true
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        isPaging = true
        appointmentDataArr.removeAll()
        
        getHistoryAppointmentListFromAPI()
    }
    
    //MARK: - Deinit
    deinit {
        print("MyAppointmentHistoryViewController deinit")
    }
    
    
    /// Calling AppointmentHistory API Method
    func callAppointmentHistoryApi()
    {
        isPaging = true
        appointmentDataArr.removeAll()
        
        getHistoryAppointmentListFromAPI()
    }
    
    //MARK: - setTableViewDelegate
    
    /// SetupTable View Delegate Method
    func setTableViewDelegate()  {
        
        tableViewAppointment.delegate = self
        tableViewAppointment.dataSource = self
        
        tableViewAppointment.emptyDataSetSource = self
        tableViewAppointment.emptyDataSetDelegate = self
    }
    
    
    /// Refresh Appointment Data Method
    
    @objc func refreshAppointmentData(_ sender: Any) {
        
        isPaging = true
        appointmentDataArr.removeAll()
        
        AppWebHandler.sharedInstance().cancelTask(forEndpoint: "appointments?")
        getHistoryAppointmentListFromAPI()
    }
    
    //Mark :- get appointment history list from api
    ///get appointment history list from api
    func getHistoryAppointmentListFromAPI()
    {
        var urlToHit = EndPoints.appointmentList("1", "0", appointmentDataArr.count).path
        if isFromDoctorProfile
        {
            urlToHit = EndPoints.appointmentListProfileDoctor("1", "0", doctorID, appointmentDataArr.count).path
        }
        
        print(urlToHit.absoluteString)
        
        if appointmentDataArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            self.refreshControl2.endRefreshing()
            
            if error != nil
            {
                HUD.hide()
                self.refreshControl2.endRefreshing()
                
                self.tableViewAppointment.isHidden = false
                
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                self.refreshControl2.endRefreshing()
                
                self.tableViewAppointment.isHidden = false
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let appointmentDtaArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: appointmentDtaArr1!)
                }
                else
                {
                    HUD.hide()
                    self.refreshControl2.endRefreshing()
                    
                    self.tableViewAppointment.isHidden = false
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                HUD.hide()
                self.refreshControl2.endRefreshing()
                
                self.tableViewAppointment.isHidden = false
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
/// Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = AppointmentModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            appointmentDataArr.append(model) // Adding model to array
        }
        
        tableViewAppointment.reloadData()
        
        self.tableViewAppointment.isHidden = false
        
        HUD.hide()
        self.refreshControl2.endRefreshing()
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension MyAppointmentHistoryViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "appointmentListTableViewCell") as! AppointmentListTableViewCell
        
        if appointmentDataArr.count > 0
        {
            let doctorDict = appointmentDataArr[indexPath.row].doctor_dict
            if (doctorDict["name"] as! String).contains("Dr")
            {
                cell.NameLabel.text = "\(doctorDict["name"] as? String ?? "")"
            }
            else
            {
                cell.NameLabel.text = "Dr.\(doctorDict["name"] as? String ?? "")"
            }
            
            let arrSpec = doctorDict["specializations"] as! [[String : Any]]
            
            let specilaization = UtilityClass.getCommaSepratedStringFromArrayDict(completeArray: arrSpec, withKeyName: "name")
            
            cell.typeLabel.text = specilaization
            
            cell.patientImageView.setShowActivityIndicator(true)
            cell.patientImageView.setIndicatorStyle(.gray)
            
            cell.patientImageView.sd_setImage(with: URL.init(string: (doctorDict["avatar_url"] as? String)!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
            
            if appointmentDataArr[indexPath.row].type == "chat" {
                cell.startDateLabel.text = appointmentDataArr[indexPath.row].formatted_date
            } else {
                
                let startDate = UtilityClass.getDateStringFromTimeStamp(timeStamp: appointmentDataArr[indexPath.row].slotStartTime, dateFormat: "dd MMM")
                print("startDate = \(startDate)")
                
                cell.startDateLabel.text = startDate as String
            }
            
            var booking_status = ""
            
            if appointmentDataArr[indexPath.row].status == "0"
            {
                booking_status = "waiting"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForWaitingStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForWaitingStatus
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForWaitingStatus.cgColor
            }
            else if appointmentDataArr[indexPath.row].status == "1" || appointmentDataArr[indexPath.row].status == "6"
            {
                booking_status = "accepted"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForAcceptedStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForAcceptedView
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForAcceptedView.cgColor
            }
            else if appointmentDataArr[indexPath.row].status == "2"
            {
                booking_status = "rejected"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForRejectedStatus.cgColor
            }
            else if appointmentDataArr[indexPath.row].status == "4"
            {
                booking_status = "canceled"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForRejectedStatus.cgColor
            }
            else if appointmentDataArr[indexPath.row].status == "5"
            {
                booking_status = "expired"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForRejectedStatus
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForRejectedStatus.cgColor
            }
            else
            {
                booking_status = "completed"
                
                cell.statusLabel.textColor = UIColor.MyanCarePatient.colorForAcceptedStatus
                cell.startDateLabel.textColor = UIColor.MyanCarePatient.colorForAcceptedView
                
                cell.startDateView.layer.borderColor = UIColor.MyanCarePatient.colorForAcceptedView.cgColor
            }
            
            cell.statusLabel.text =  booking_status.capitalized.localized()
            
            cell.delegate = self
            cell.tag = indexPath.row
        }
        
        return cell
    }
    
    private func tableView(_ tableView: UITableView, willDisplay cell: AppointmentListTableViewCell, forRowAt indexPath: IndexPath)
    {
        if isPaging && indexPath.row == appointmentDataArr.count - 1
        {
            getHistoryAppointmentListFromAPI()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (106 * scaleFactorX)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if appointmentDataArr.count > 0
        {
            let appointmentDetail = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentDetailVC") as! MyAppointmentDetailViewController
            
            appointmentDetail.appointmentModelData = appointmentDataArr[indexPath.row]
            
            navigationController?.pushViewController(appointmentDetail, animated: true)
        }
    }
}

///AppointmentListTableViewCell Delegate Methods Extension
extension MyAppointmentHistoryViewController : AppointmentListTableViewCellDelegate
{
    
    /// Image View Clicked Event Method
    ///
    /// - Parameter cell: Cell Refrence
    func AppointmentListTableViewCellImageViewClick(_ cell: AppointmentListTableViewCell) {
        
        let tag = cell.tag
        
        if appointmentDataArr.count > 0
        {
            let doctorDetailVC = UIStoryboard.getDoctorDetailStoryBoard().instantiateInitialViewController() as! DoctorDetailViewController
            
            let doctorDict = appointmentDataArr[tag].doctor_dict
            doctorDetailVC.doctor_id = doctorDict["id"] as! String
            
            self.navigationController?.pushViewController(doctorDetailVC, animated: true)
        }
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
///Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension MyAppointmentHistoryViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Appointments History For You".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
