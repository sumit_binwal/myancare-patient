//
//  MyAppointmentDetailViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class MyAppointmentDetailViewController: UIViewController {

    //Mark: - Properties:-
    ///UIButton Refrence - Cancel Button
    @IBOutlet weak var cancelChatButton: UIButton!
    ///UIButton Refrence - Action Button
    @IBOutlet weak var actionButton: UIButton!
    ///UIButton Refrence - Cancel Button
    @IBOutlet weak var cancelButton: UIButton!
    ///UIButton Refrence - Accept Button
    @IBOutlet weak var acceptButon: UIButton!
    ///UIImageView Refrence - ImageView Profile
    @IBOutlet var imageViewProfile: UIImageView!
    ///UILabel Refrence - shows Doctor Name
    @IBOutlet var labelDoctorName: UILabel!
    ///UILabel Refrence - shows Doctor Speciality
    @IBOutlet var labelDoctorSpclty: UILabel!
    ///UILabel Refrence - Label Reschedule
    @IBOutlet var labelReschedule: UILabel!
    ///UILabel Refrence - Cancel
    @IBOutlet var labelCancel: UILabel!
    ///UILabel Refrence - shows Main Date
    @IBOutlet var labelMainDate: UILabel!
    ///UIView Refrence - shows Main Date View
    @IBOutlet var mainDateView: UIView!
    ///UIButton Refrence - Status Button
    @IBOutlet weak var btnStatus: UIButton!
    ///NSLayoutConstraint Refrence - to mange tableview frames
    @IBOutlet weak var tableviewBottomConstraint: NSLayoutConstraint!
    
    
    ///UITableView Refrence - to show data on listview
    @IBOutlet var tableViewList: UITableView!
    
    ///Array of AppointmentModel Data
    var appointmentModelData = AppointmentModel()
    
    ///Variable Number of Rows
    var numberOfRows = 0
    
    ///UIViewController LIfe Cycle Methods
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        setViewData()
        getEnableChatButtonStatus()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("MyAppointmentDetailViewController deinit")
    }
    
    //MARK: - setUpNavigationBar
    ///Setup Navigation Bar Method
    func setUpNavigationBar() {
        
        self.title = "My Appointments".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK: - navigation bar back button action
    ///Navigation Bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - setViewData
    ///Set View Data Method
    func setViewData() {
        
        let doctorDict = appointmentModelData.doctor_dict
        
        let arrSpec = doctorDict["specializations"] as! [[String : Any]]
        
        let specilaization = UtilityClass.getCommaSepratedStringFromArrayDict(completeArray: arrSpec, withKeyName: "name")
        
        labelDoctorName.text = specilaization
        
        if (doctorDict["name"] as! String).contains("Dr")
        {
            labelDoctorName.text = "\(doctorDict["name"] as? String ?? "")"
        }
        else
        {
            labelDoctorName.text = "Dr.\(doctorDict["name"] as? String ?? "")"
        }
        
        labelDoctorSpclty.text = appointmentModelData.type.capitalized
        
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.size.width/2 * scaleFactorX
        imageViewProfile.layer.borderWidth = 1.0
        imageViewProfile.layer.borderColor = UIColor.clear.cgColor
        imageViewProfile.layer.masksToBounds = true
        
        imageViewProfile.setShowActivityIndicator(true)
        imageViewProfile.setIndicatorStyle(.gray)
        
        imageViewProfile.sd_setImage(with: URL.init(string: (doctorDict["avatar_url"] as? String)!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        
        imageViewProfile.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(doctorImageViewClicked))
        imageViewProfile.addGestureRecognizer(gesture)
        
        //labelDate.text = appointmentModelData.date_of_issue
        
        var booking_status = ""
        
        if appointmentModelData.status == "0"
        {
            booking_status = "waiting"
        }
        else if appointmentModelData.status == "1" || appointmentModelData.status == "6"
        {
            booking_status = "accepted"
        }
        else if appointmentModelData.status == "2"
        {
            booking_status = "rejected"
        }
        else if appointmentModelData.status == "4"
        {
            booking_status = "canceled"
        }
        else if appointmentModelData.status == "5"
        {
            booking_status = "expired"
        }
        else
        {
            booking_status = "completed"
        }
        
        btnStatus.setTitle(booking_status.capitalized.localized(), for: .normal)
        
        if appointmentModelData.type == "chat"
        {
            labelMainDate.text = appointmentModelData.formatted_date
        }
        else
        {
            let startDate = UtilityClass.getDateStringFromTimeStamp(timeStamp: appointmentModelData.slotStartTime, dateFormat: "dd MMM")
            //print("startDate = \(startDate)")
            
            labelMainDate.text = startDate as String
        }
        
        mainDateView.layer.cornerRadius = 1.0
        mainDateView.layer.borderWidth = 1.0
        mainDateView.layer.masksToBounds = true
        
        labelMainDate.textColor = UIColor.white
        mainDateView.layer.borderColor = UIColor.clear.cgColor
        
        numberOfRows = 9
        
        cancelChatButton.isHidden = true
        
        if appointmentModelData.status == "0"
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForWaitingStatus
            
            if appointmentModelData.type == "chat"
            {
                cancelButton.isHidden = true
                acceptButon.isHidden = true
                labelReschedule.isHidden = true
                labelCancel.isHidden = true
                
                actionButton.isHidden = true
                
                cancelChatButton.isHidden = false
            }
            else
            {
                cancelButton.isHidden = false
                acceptButon.isHidden = false
                labelReschedule.isHidden = false
                labelCancel.isHidden = false
                
                actionButton.isHidden = true
                
                cancelChatButton.isHidden = true
            }
            
            tableviewBottomConstraint.constant = 55
        }
        else if appointmentModelData.status == "1" || appointmentModelData.status == "6"
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForAcceptedStatus
            
            cancelButton.isHidden = true
            acceptButon.isHidden = true
            labelReschedule.isHidden = true
            labelCancel.isHidden = true
            
            if appointmentModelData.type == "chat"
            {
                actionButton.isHidden = false
                tableviewBottomConstraint.constant = 55
            }
            else
            {
                actionButton.isHidden = true
                tableviewBottomConstraint.constant = 0
            }
            
            actionButton.setTitle(("Start \(appointmentModelData.type.capitalized)").localized(), for: .normal)
        }
        else if appointmentModelData.status == "2"
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForRejectedStatus
            
            cancelButton.isHidden = true
            acceptButon.isHidden = true
            labelReschedule.isHidden = true
            labelCancel.isHidden = true
            
            actionButton.isHidden = true
            
            numberOfRows = 10
            
            tableviewBottomConstraint.constant = 0
        }
        else if appointmentModelData.status == "5"
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForRejectedStatus
            
            cancelButton.isHidden = true
            acceptButon.isHidden = true
            labelReschedule.isHidden = true
            labelCancel.isHidden = true
            
            actionButton.isHidden = true
            
            numberOfRows = 10
            
            tableviewBottomConstraint.constant = 0
        }
        else if appointmentModelData.status == "4"
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForRejectedStatus
            
            cancelButton.isHidden = true
            acceptButon.isHidden = true
            labelReschedule.isHidden = true
            labelCancel.isHidden = true
            
            actionButton.isHidden = true
            
            numberOfRows = 10
            
            tableviewBottomConstraint.constant = 0
        }
        else
        {
            btnStatus.backgroundColor = UIColor.MyanCarePatient.colorForAcceptedStatus
            
            cancelButton.isHidden = true
            acceptButon.isHidden = true
            labelReschedule.isHidden = true
            labelCancel.isHidden = true
            
            actionButton.isHidden = false
            
            actionButton.setTitle("Review".localized(), for: .normal)
            
            tableviewBottomConstraint.constant = 55
        }
        
        acceptButon.backgroundColor = UIColor.MyanCarePatient.colorForWaitingStatus
        cancelButton.backgroundColor = UIColor.MyanCarePatient.colorForRejectedStatus
        actionButton.backgroundColor = UIColor.MyanCarePatient.colorForAcceptedView
        
        labelCancel.text = "Cancel Appointment".localized()
        labelReschedule.text = "Reschedule Appointment".localized()
        
        cancelChatButton.setTitle("Cancel Appointment".localized(), for: .normal)
    }
    
    
    ///Get Enable Chat Button Status - Socket Event
    func getEnableChatButtonStatus()
    {
        SocketManagerHandler.sharedInstance().getButtonEnableStatus (_appointmentID: appointmentModelData.appointmentID) { (dataDict, statusCode) in
            
            let buttonEnableStatus = dataDict["enable"] as! String
            
            if self.actionButton.title(for: .normal) != "Review".localized()
            {
                if (buttonEnableStatus == "1")
                {
                    self.actionButton.isUserInteractionEnabled = true
                    self.actionButton.alpha = 1
                }
                else
                {
                    self.actionButton.isUserInteractionEnabled = false
                    self.actionButton.alpha = 0.5
                }
            }
        }
    }
    
    
    /// Doctor Image View Clicked Event Method
    @objc func doctorImageViewClicked()
    {
        let doctorDetailVC = UIStoryboard.getDoctorDetailStoryBoard().instantiateInitialViewController() as! DoctorDetailViewController
        
        let doctorDict = appointmentModelData.doctor_dict
        doctorDetailVC.doctor_id = doctorDict["id"] as! String
        
        self.navigationController?.pushViewController(doctorDetailVC, animated: true)
    }
    
    //MARK: - on cancel button action
    ///on cancel Appointment button action
    @IBAction func cancelAppointmentButtonAction(_ sender: Any)
    {
        let appointRejectVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentCancelVC") as! MyAppointmentCancelViewController
        
        appointRejectVC.backgroungImageScreen = UtilityClass.screenShotMethod()
        appointRejectVC.appointmentModelData = appointmentModelData
        
        self.navigationController?.pushViewController(appointRejectVC, animated: false)
    }
    
    //MARK: - on cancel button action
    /// On Cancel Chat Appointment Button Action Event
    @IBAction func cancelChatAppointmentButtonAction(_ sender: Any)
    {
        let appointRejectVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentCancelVC") as! MyAppointmentCancelViewController
        
        appointRejectVC.backgroungImageScreen = UtilityClass.screenShotMethod()
        appointRejectVC.appointmentModelData = appointmentModelData
        
        self.navigationController?.pushViewController(appointRejectVC, animated: false)
    }
    
    //MARK: - on reschedule button action
    ///On Reschedule Button Clicked Event
    @IBAction func resceduleAppointmentButtonAction(_ sender: Any)
    {
        modelAppointmentData.serviceFees = appointmentModelData.amount
        modelAppointmentData.appointmentType = appointmentModelData.type
        
        var service_fees = appointmentModelData.service_fees
        
        service_fees = service_fees.replacingOccurrences(of: " ", with: "")
        service_fees = service_fees.replacingOccurrences(of: "Kyat".localized(), with: "")
        modelAppointmentData.service_tax = service_fees
        
        var total_appointment_fees = appointmentModelData.total_appointment_fees
        
        total_appointment_fees = total_appointment_fees.replacingOccurrences(of: " ", with: "")
        total_appointment_fees = total_appointment_fees.replacingOccurrences(of: "Kyat".localized(), with: "")
        modelAppointmentData.total_consultant_fees = total_appointment_fees
        
        modelAppointmentData.doctorID = self.appointmentModelData.doctor_dict["id"] as! String
        
        modelAppointmentData.appointmentID = self.appointmentModelData.appointmentID
        
        modelAppointmentData.doctorName = self.appointmentModelData.doctor_dict["name"] as! String
            
        modelAppointmentData.issueDate = UtilityClass.getStringFromDateFormat(date: Date(), formate: "dd MMM yyyy | hh:mm a")
            
        modelAppointmentData.isReschedule = true
            
        let schdeuleVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateInitialViewController()
        self.navigationController?.pushViewController(schdeuleVC!, animated: true)
    }
    
    // Sinch Work
    //MARK:- Sinch
    ///Sinch Task
    func client()->SINClient {
        return ((UIApplication.shared.delegate as? AppDelegate)?.client)!
    }
    
    //MARK: - on review button action
    ///on review button Clicked action Method
    @IBAction func actionReviewButtonAction(_ sender: Any) {
        
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "No Active Internet Connection Found".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        
        let button = sender as! UIButton
        
        if button.title(for: .normal) == "Start Chat".localized()
        {
            let chatView = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
                
            chatView.senderID = appointmentModelData.doctor_dict["id"]! as! String
            chatView.chatUsername = appointmentModelData.doctor_dict["name"]! as! String
            
            navigationController?.pushViewController(chatView, animated: true)
        }
        else
        {
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController

            MyAppointmentsReviewVC.appointmentModelData = appointmentModelData

            self.navigationController?.pushViewController(MyAppointmentsReviewVC, animated: true)
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension MyAppointmentDetailViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: AppointmentsTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_AppointmentsDetailTableCustomCell) as! AppointmentsTableCustomCell
        
        let patientDict = appointmentModelData.patient_dict
        
        if indexPath.row == 0
        {
            cell.titleLabel.text = "Date of issue:".localized()
            cell.descripionLabel.text = appointmentModelData.date_of_issue
        }
        else if indexPath.row == 1
        {
            cell.titleLabel.text = "Scheduled Date/Time:".localized()
            
            if appointmentModelData.type == "chat"
            {
                cell.descripionLabel.text = appointmentModelData.formatted_str
            }
            else
            {
                let startDate = UtilityClass.getDateStringFromTimeStampDetail(timeStamp: appointmentModelData.slotStartTime) as String
                cell.descripionLabel.text = startDate
            }
        }
        else if indexPath.row == 2
        {
            cell.titleLabel.text = "Patient Name:".localized()
            cell.descripionLabel.text = patientDict["name"] as? String
        }
        else if indexPath.row == 3
        {
            cell.titleLabel.text = "Service:".localized()
            cell.descripionLabel.text = appointmentModelData.type.capitalized
        }
        else if indexPath.row == 4
        {
            cell.titleLabel.text = "Service Unit:".localized()
            if appointmentModelData.type == "chat"
            {
                cell.descripionLabel.text = "7 Days (maximum)".localized()
            }
            else
            {
                cell.descripionLabel.text = "15 min (maximum)".localized()
            }
        }
        else if indexPath.row == 5
        {
            cell.titleLabel.text = "Consultation Fees:".localized()
            cell.descripionLabel.text = appointmentModelData.amount
        }
        else if indexPath.row == 6
        {
            cell.titleLabel.text = "Service Fees:".localized()
            if appointmentModelData.service_fees == ""
            {
                cell.descripionLabel.text = "0.0" + "Kyat".localized()
            }
            else
            {
                cell.descripionLabel.text = appointmentModelData.service_fees
            }
        }
        else if indexPath.row == 7
        {
            cell.titleLabel.text = "Total Fees:".localized()
            if appointmentModelData.total_appointment_fees == ""
            {
                cell.descripionLabel.text = appointmentModelData.amount
            }
            else
            {
                cell.descripionLabel.text = appointmentModelData.total_appointment_fees
            }
        }
        else if indexPath.row == 8
        {
            cell.titleLabel.text = "Reason for visit:".localized()
            if appointmentModelData.type == "chat"
            {
                cell.descripionLabel.text = "NA".localized()
            }
            else
            {
                cell.descripionLabel.text = appointmentModelData.reason
            }
            //appointmentModelData.getAppoinrmentReasonString()
        }
        else
        {
            cell.titleLabel.text = "Reason for cancel:".localized()
            cell.descripionLabel.text = appointmentModelData.remarks //appointmentModelData.getAppoinrmentRemarkString()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return (39 * scaleFactorX)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
