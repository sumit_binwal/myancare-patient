//
//  ChangePasswordViewController.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
///Change Password Model Data Variable
var modelChangePasswordProcess = ChangePasswordModel()

///Change Password View Controller Class - UIViewController Type
class ChangePasswordViewController: UIViewController {

    ///UIBUtton Refrence - COnfirm Button
    @IBOutlet weak var confirmButton: UIButton!
    ///UITableView Variable - Change Password Content TableView
    @IBOutlet weak var changePasswordTableView: UITableView!
    
    // MARK: - viewDidLoad
    ///UIViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }

    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("ChangePasswordViewController deinit")
    }
    
    //MARK: - setUpTableView
    ///Setup TableView Method
    func setUpTableView() {
        
        changePasswordTableView.delegate = self
        changePasswordTableView.dataSource = self
    }
    
    // MARK: - setUpNavigationBar
    ///setUpNavigationBar  method
    func setUpNavigationBar() {
        
        self.title = "Change Password".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        confirmButton.setTitle("Confirm".localized(), for: .normal)
    }
    
    // MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///Confirm Button Clicked Action
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if validateTheInputField() {
            callChangePasswordAPi()
        }
    }
    
    //MARK: - Validate InputField
    
    /// Validate All Input TextField Values
    ///
    /// - Returns: Returns if any one is not filled
    func validateTheInputField() -> Bool {
        
        if modelChangePasswordProcess.password_old == nil || modelChangePasswordProcess.password_old!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter old password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelChangePasswordProcess.password!.count < 6 || modelChangePasswordProcess.password!.count > 20
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Password should be in range of 6 to 20 characters.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelChangePasswordProcess.password == nil || modelChangePasswordProcess.password!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter new password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelChangePasswordProcess.password_confirmation == nil || modelChangePasswordProcess.password_confirmation!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter confirm password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelChangePasswordProcess.password_confirmation != modelChangePasswordProcess.password
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Password doesn't match.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    ///Get Update Password Dictionary Value
    ///
    /// - Returns: Retrun A Dictinoary for Values
    func getUpdatePasswordDictionary() -> [String : Any]
    {
        return [
            "old_password" : modelChangePasswordProcess.password_old ?? "",
            "password" : modelChangePasswordProcess.password ?? "",
            "password_confirmation" : modelChangePasswordProcess.password_confirmation ?? ""
        ]
    }
    
    //MARK: - Call change Password API
    ///Call change Password API
    func callChangePasswordAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = getUpdatePasswordDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.change_password.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    UtilityClass.deleteUserData()
                    
                    let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                    
                    let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                    
                    UtilityClass.changeRootViewController(with: navigationController)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: -
//MARK: - Extension -> UITableView -> UITableViewDataSource, UITableViewDelegate
///Extension -> UITableView -> UITableViewDataSource, UITableViewDelegate
extension ChangePasswordViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : ChangePasswordTableViewCell!
        
        let cellIdentifier = "ChangePasswordTableViewCell"
        
        var cellType = ChangePasswordCellType.none
        
        switch indexPath.row {
            
        case 0:
            cellType = .oldPassword
        
        case 1:
            cellType = .newPassword
            
        case 2:
            cellType = .confirmPassword
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChangePasswordTableViewCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: - Extension -> PhoneDataCellDelegate
///Extension -> PhoneDataCellDelegate
extension ChangePasswordViewController : ChangePasswordTableViewCellDelegate
{
    
    /// Update Change Password Data TableView Cell UPdate Values
    ///
    /// - Parameters:
    ///   - cell: ChangePasswordTableView Cell Refrence
    ///   - inputValue: InputValue String
    func changePasswordDataCell(_ cell: ChangePasswordTableViewCell, updatedInputFieldText inputValue: String) {
        
        switch cell.cellType {
            
        case .oldPassword:
            modelChangePasswordProcess.password_old = inputValue
            
        case .newPassword:
            modelChangePasswordProcess.password = inputValue
            
        case .confirmPassword:
            modelChangePasswordProcess.password_confirmation = inputValue
            
        default:
            break
        }
    }
}

