//
//  PasscodeSettingViewController.swift
//  MyancarePatient
//
//  Created by iOS on 31/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class PasscodeSettingViewController: UIViewController {

    @IBOutlet weak var passcodeOnLabel: UILabel!
    @IBOutlet weak var setNewLabel: UILabel!
    
    @IBOutlet weak var passcodeOnOffSwitch: UISwitch!
    var isPasscodeOnOrOff = false
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        self.tabBarController?.tabBar.isHidden = true
        
        isPasscodeOnOrOff = UtilityClass.getPasscodeOnOff()
        if isPasscodeOnOrOff {
            passcodeOnOffSwitch.isOn = true
        } else {
            passcodeOnOffSwitch.isOn = false
        }
        
        passcodeOnLabel.text = "Passcode On".localized()
        
        if UtilityClass.getPasscode() == "" {
            setNewLabel.text = "Setup New Passcode".localized()
        } else {
            setNewLabel.text = "Change Passcode".localized()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("PasscodeSettingViewController deinit")
    }
    
    // MARK: - setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Passcode Settings".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    // MARK: - Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - on passcode On Off Button Action
    @IBAction func passcodeOnOffAction(_ sender: Any) {
        
        if passcodeOnOffSwitch.isOn {
            passcodeOnOffSwitch.isOn = false
            isPasscodeOnOrOff = false
            
            UtilityClass.setPasscodeOnOff(passcodeOnOff: false)
        } else {
            
            passcodeOnOffSwitch.isOn = true
            isPasscodeOnOrOff = true
            
            UtilityClass.setPasscodeOnOff(passcodeOnOff: true)
        }
    }
    
    //MARK: - on set a New Passcode Button Action
    @IBAction func setNewPasscodeAction(_ sender: Any) {
        
        let passcodeVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "PasscodeViewController") as! PasscodeViewController
        
        passcodeVC.hidesBottomBarWhenPushed = true
        passcodeVC.isFromPasscodeSetting = true
        
        self.navigationController?.pushViewController(passcodeVC, animated: true)
    }
}
