//
//  UIColorExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    struct MyanCarePatient {
        
        static let lightGreyColor = UIColor (RED: 204, GREEN: 204, BLUE: 204, ALPHA: 1)
        static let signUpButtonHighlightColor = UIColor (RED: 15, GREEN: 176, BLUE: 152, ALPHA: 1)
        static let signUpButtonColor = UIColor (RED: 119, GREEN: 119, BLUE: 119, ALPHA: 1)
        static let welcomeScreenIntroTextColor = UIColor (RED: 85, GREEN: 85, BLUE: 85, ALPHA: 1)
        static let welcomeScreenGuestTextColor = UIColor (RED: 1, GREEN: 123, BLUE: 206, ALPHA: 1)
        static let signupTextFocus = UIColor (RED: 102, GREEN: 102, BLUE: 102, ALPHA: 1)
        static let signupTextUnFocus = UIColor (RED: 187, GREEN: 187, BLUE: 187, ALPHA: 1)
        static let textfieldPlaceholderColor = UIColor (RED: 187, GREEN: 187, BLUE: 187, ALPHA: 1)
        static let appDefaultGreenColor = UIColor (RED: 30, GREEN: 197, BLUE: 155, ALPHA: 1)
        static let darkGreyColorForTabBar = UIColor (RED: 15, GREEN: 176, BLUE: 152, ALPHA: 1)
        static let dateColor = UIColor (RED: 68, GREEN: 68, BLUE: 68, ALPHA: 1)

        //Articles
        static let darkGreyColorForText = UIColor (RED: 51, GREEN: 51, BLUE: 51, ALPHA: 1)
        static let LightGreyColorForText = UIColor (RED: 85, GREEN: 85, BLUE: 85, ALPHA: 1)
        static let colorForPostedDate = UIColor (RED: 228, GREEN: 228, BLUE: 228, ALPHA: 1)
        
        // doctor segment color
        static let colorForDoctorSegmentSelected = UIColor (RED: 1, GREEN: 123, BLUE: 206, ALPHA: 1)
        
        // appointment list color
        static let colorForAcceptedStatus = UIColor (RED: 0, GREEN: 222, BLUE: 78, ALPHA: 1)
        
        static let colorForAcceptedView = UIColor (RED: 30, GREEN: 197, BLUE: 155, ALPHA: 1)
        
        static let colorForRejectedStatus = UIColor (RED: 255, GREEN: 0, BLUE: 0, ALPHA: 1)
        
        static let colorForWaitingStatus = UIColor (RED: 1, GREEN: 123, BLUE: 206, ALPHA: 1)
        
        static let colorForActiveReviewStatus = UIColor (RED: 233, GREEN: 167, BLUE: 33, ALPHA: 1)
        static let colorForDeactiveReviewStatus = UIColor (RED: 153, GREEN: 153, BLUE: 153, ALPHA: 1)
    }
    
    convenience init(RED : Float, GREEN: Float, BLUE: Float, ALPHA: Float)
    {
        self.init(red: CGFloat(RED/255), green: CGFloat(GREEN/255), blue: CGFloat(BLUE/255), alpha: CGFloat(ALPHA))
    }
}
