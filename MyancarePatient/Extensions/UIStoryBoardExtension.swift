//
//  UIStoryBoardExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    static func getLanguageStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Language", bundle : nil)
        return storyBoard
    }
    
    static func getWelcomeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Welcome", bundle : nil)
        return storyBoard
    }
    
    static func getLoginStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Login", bundle : nil)
        return storyBoard
    }
    
    static func getSignUpStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Signup", bundle : nil)
        return storyBoard
    }
    
    static func getHomeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Home", bundle : nil)
        return storyBoard
    }
    
    static func getArticlesStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Articles", bundle : nil)
        return storyBoard
    }
    
    static func getArticlDetailStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ArticleDetail", bundle : nil)
        return storyBoard
    }
    
    static func getProfileStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Profile", bundle : nil)
        return storyBoard
    }
    
    static func getAboutUsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "AboutUs", bundle : nil)
        return storyBoard
    }
    
    static func getChangeTextSizeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ChangeTextSize", bundle : nil)
        return storyBoard
    }
    
    static func getPasscodeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Passcode", bundle : nil)
        return storyBoard
    }
    
    static func getLocationStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Location", bundle: nil)
        return storyBoard
    }
    
    static func getHealthStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Health", bundle: nil)
        return storyBoard
    }
    
    static func getPhoneInfoStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "PhoneInfo", bundle: nil)
        return storyBoard
    }
    
    static func getChangePhoneNumberStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ChangePhoneNumber", bundle: nil)
        return storyBoard
    }
    
    static func getCommentsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Comments", bundle: nil)
        return storyBoard
    }
    
    static func getFilterStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Category", bundle: nil)
        return storyBoard
    }
    
    static func getDoctorDetailStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "DoctorDetail", bundle: nil)
        return storyBoard
    }
    
    static func getBookmarkLikeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "BookmarkLike", bundle: nil)
        return storyBoard
    }
    
    static func getScheduleAppointmentStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ScheduleAppointment", bundle: nil)
        return storyBoard
    }
    
    static func getWalletStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyWallet", bundle : nil)
        return storyBoard
    }
    
    static func getMyAppointmentsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyAppointments", bundle : nil)
        return storyBoard
    }
    
    static func getMedicineReminderStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MedicineReminder", bundle : nil)
        return storyBoard
    }
    
    static func getMyRecordStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyRecord", bundle : nil)
        return storyBoard
    }
    
    static func getDoctorFilterStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "DoctorFilter", bundle : nil)
        return storyBoard
    }
    
    static func getDoctorProfileStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "DoctorProfile", bundle : nil)
        return storyBoard
    }
    
    static func getChatViewStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ChatView", bundle : nil)
        return storyBoard
    }
    
    static func getCallHandlingStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "CallHandling", bundle: nil)
        return storyBoard
    }
}
