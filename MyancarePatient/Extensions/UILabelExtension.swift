//
//  LabelExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    
    static func createAttributedStringForText(strObj : String, size : CGFloat, size2 : CGFloat, location : Int,location2: Int,length : Int, length2: Int, color : UIColor, color2 : UIColor ) -> NSAttributedString {
        
        let attributedString = NSMutableAttributedString(string: strObj)
        attributedString.addAttributes([
            NSAttributedStringKey.font: UIFont.createPoppinsSemiBoldFont(withSize: size),
            NSAttributedStringKey.foregroundColor: color
            ], range: NSRange(location: location, length: length)
        )
        attributedString.addAttributes([
            NSAttributedStringKey.font: UIFont.createPoppinsRegularFont(withSize: size2),
            NSAttributedStringKey.foregroundColor: UIColor.MyanCarePatient.signUpButtonColor
            ], range: NSRange(location: location2, length: length2)
        )
        return attributedString
    }
    static func createAttributedStringForTextNormal(strObj : String, location : Int,length : Int, color : UIColor, font : UIFont  ) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: strObj)
        attributedString.addAttributes([
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: color
            ], range: NSRange(location: location, length: length)
        )
        
        return attributedString
    }
    
    static func setLineSpacing(labelText:NSAttributedString?, lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        
        attributedString = NSMutableAttributedString(attributedString: labelText!)
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
}
