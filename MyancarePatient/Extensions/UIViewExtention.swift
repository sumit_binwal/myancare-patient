//
//  UIViewExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
extension UIView
{
    func setBorderColor(_ color: UIColor, cornorRadius: CGFloat, borderWidth: CGFloat) {
        self.layer.cornerRadius = cornorRadius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
    }
}
