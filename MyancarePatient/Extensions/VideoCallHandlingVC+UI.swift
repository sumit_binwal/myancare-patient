//
//  CallHandlingVC+UI.swift
//  MyancarePatient
//
//  Created by Jitendra Singh on 02/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

// This extension for the CallHandlingVC serves only to separate code that
// is directly related to the usage of the Sinch SDK, from code that is used
// for improving the look and feel of the sample app.

// Nothing magic here, just regular code interacting with the iOS SDK.

// Includes helper methods for updating UI elements such as labels and buttons.
// Includes helper functionality around dealing with modal presentation of the
// call view.

import Foundation
import UIKit

extension VideoCallHandlingVC
{
    func setCallStatusText(_ text: String)
    {
        callStateLabel.text = text
    }
    
    // MARK: - Buttons
    func showButtons(_ buttons: EButtonsBar)
    {
        if buttons == EButtonsBar.kButtonsAnswerDecline
        {
            answerButton.isHidden = false
            declineButton.isHidden = false
            endCallButton.isHidden = true
            connectingLabel.isHidden = true
        }
        else if buttons == EButtonsBar.kButtonsHangup
        {
            endCallButton.isHidden = false
            answerButton.isHidden = true
            declineButton.isHidden = true
            connectingLabel.isHidden = true
        }
        else if buttons == EButtonsBar.kButtonsWakenByCallKit
        {
            endCallButton.isHidden = true
            answerButton.isHidden = true
            declineButton.isHidden = true
            connectingLabel.isHidden = false
        }
    }
    
    // MARK: - Duration
    func setDuration(_ seconds: Int)
    {
        callDuration = String(seconds)

        setCallStatusText(String(format: "%02d:%02d", Int(seconds / 60), Int(seconds % 60)))
        
        if (seconds == 900)
        {
            self.call?.hangup()
            
            self.dismiss(animated: true)
            {
                self.audioController().stopPlayingSoundFile()
                self.stopCallDurationTimer()
                self.videoController().remoteView().removeFromSuperview()
                self.audioController().disableSpeaker()
                
                let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
                
                MyAppointmentsReviewVC.isFromCall = true
                MyAppointmentsReviewVC.doctor_id = self.doctorID
                MyAppointmentsReviewVC.appointment_id = self.appointmentID
                
                MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                
                guard let appD = appDelegate as? AppDelegate else {
                    return
                }
                
                let topController = appD.window?.currentViewController()
                
                topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func internal_updateDuration(_ timer: Timer)
    {
        let selector: Selector = NSSelectorFromString(timer.userInfo as! String)
        
        if responds(to: selector)
        {
            //clang diagnostic push
            //clang diagnostic ignored "-Warc-performSelector-leaks"
            perform(selector, with: timer)
            //clang diagnostic pop
        }
    }
    
    func startCallDurationTimer(with sel: Selector)
    {
        let selectorAsString: String = NSStringFromSelector(sel)
        
        durationTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.internal_updateDuration(_:)), userInfo: selectorAsString, repeats: true)
    }
    
    func stopCallDurationTimer()
    {
        durationTimer?.invalidate()
        durationTimer = nil
    }
}
